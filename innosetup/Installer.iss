; -- Installer.iss --
; Installer for game
#define ApplicationVersion="0.99.1"

[Setup]
AppName=UFO Attack
AppVersion={#ApplicationVersion}
OutputBaseFilename=UFOAttackSetup-{#ApplicationVersion}
WizardStyle=modern
DefaultDirName={autopf}\UFO Attack
DefaultGroupName=UFOAttack
UninstallDisplayIcon=../assets/icons/uninstall.ico
SetupIconFile=../assets/icons/gameicon.ico
Compression=lzma2
SolidCompression=yes
OutputDir=.
; "ArchitecturesAllowed=x64" specifies that Setup cannot run on
; anything but x64.
ArchitecturesAllowed=x64
; "ArchitecturesInstallIn64BitMode=x64" requests that the install be
; done in "64-bit mode" on x64, meaning it should use the native
; 64-bit Program Files directory and the 64-bit view of the registry.
ArchitecturesInstallIn64BitMode=x64
DisableWelcomePage=no
DisableDirPage=no

[Files]
Source: "../msvc2015/x64/Release/msvc2015.exe"; DestDir: "{app}"; DestName: "UFOAttack.exe"
Source: "../msvc2015/msvc2015/assets/*"; DestDir: "{app}/assets/"; Flags: recursesubdirs     
Source: "../msvc2015/msvc2015/config.json"; DestDir: "{app}"
Source: "../SDL2/lib/x64/SDL2.dll"; DestDir: "{app}"
Source: "../SDL2_image/lib/x64/SDL2_image.dll"; DestDir: "{app}"
Source: "../SDL2_image/lib/x64/libjpeg-9.dll"; DestDir: "{app}"
Source: "../SDL2_image/lib/x64/libpng16-16.dll"; DestDir: "{app}"
Source: "../SDL2_image/lib/x64/zlib1.dll"; DestDir: "{app}"      
Source: "../SDL2_image/lib/x64/libtiff-5.dll"; DestDir: "{app}"
Source: "../SDL2_image/lib/x64/libwebp-7.dll"; DestDir: "{app}"
Source: "../SDL2_mixer/lib/x64/SDL2_mixer.dll"; DestDir: "{app}"
Source: "../SDL2_mixer/lib/x64/libFLAC-8.dll"; DestDir: "{app}"
Source: "../SDL2_mixer/lib/x64/libmodplug-1.dll"; DestDir: "{app}"
Source: "../SDL2_mixer/lib/x64/libmpg123-0.dll"; DestDir: "{app}"
Source: "../SDL2_mixer/lib/x64/libogg-0.dll"; DestDir: "{app}"
Source: "../SDL2_mixer/lib/x64/libopus-0.dll"; DestDir: "{app}"
Source: "../SDL2_mixer/lib/x64/libopusfile-0.dll"; DestDir: "{app}"
Source: "../SDL2_mixer/lib/x64/libvorbis-0.dll"; DestDir: "{app}"
Source: "../SDL2_mixer/lib/x64/libvorbisfile-3.dll"; DestDir: "{app}"
Source: "../GameReadme.txt"; DestDir: "{app}"; Flags: isreadme; DestName: "README.txt"
Source: "../assets/icons/gameicon.ico"; DestDir: "{app}"
Source: "../assets/icons/uninstall.ico"; DestDir: "{app}"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "Do you want to create desktop icon?"; Flags: checkablealone

[Icons]
Name: "{group}\UFOAttack"; Filename: "{app}\UFOAttack.exe"; IconFilename: "../assets/icons/gameicon.ico"
Name: "{group}\{cm:UninstallProgram,MyApp}"; Filename: "{uninstallexe}"; IconFilename: "../assets/icons/uninstall.ico"
Name: "{commondesktop}\UFOAttack"; Filename: "{app}\UFOAttack.exe"; Tasks: desktopicon; IconFilename: "{app}/gameicon.ico"

