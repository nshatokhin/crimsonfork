#pragma once

#include "components/AnimationState.h"

#define GENERAL_ANIMATION_STATES 1
#define GENERAL_ANIMATION_FRAMES 3

#define PLAYER_ANIMATION_STATES 9
#define PLAYER_ANIMATION_FRAMES 6

#define BUTTON_ANIMATION_STATES 3
#define BUTTON_ANIMATION_FRAMES 1

#define HITMAP_MAXIMUM_POINTS_COUNT 1

typedef State<GENERAL_ANIMATION_FRAMES> EnemyAnimation;
typedef State<GENERAL_ANIMATION_FRAMES> BonusAnimation;
typedef State<GENERAL_ANIMATION_FRAMES> BulletAnimation;
typedef State<GENERAL_ANIMATION_FRAMES> WeaponAnimation;
typedef State<PLAYER_ANIMATION_FRAMES> PlayerAnimation;
typedef State<BUTTON_ANIMATION_FRAMES> ButtonAnimation;

struct SpriteSize
{
	int32_t width;
	int32_t height;
};

