#pragma once

#include "common/Common.h"
#include "components/Modifier.h"

class BonusParams
{
public:
	BonusParams() {}

public:
	uint32_t id;
	uint32_t hitmapRadius;
	uint32_t animationStatesCount;
	BonusAnimation animationStates[GENERAL_ANIMATION_STATES];
	uint32_t collectSound;
	Modifier modifier;
	bool instant;
	uint32_t dropChance;
};
