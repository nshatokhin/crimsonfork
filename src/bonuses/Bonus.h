#pragma once

#include "BonusParams.h"

#include <vector>

class Bonus
{
public:
	static std::vector<BonusParams> params;
};

std::vector<BonusParams> Bonus::params;
