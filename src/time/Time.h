#pragma once

#include <chrono>
#include <thread>

typedef std::chrono::time_point<std::chrono::steady_clock> timestamp_t;

class Time
{
public:
	static timestamp_t getTimestamp()
	{
		return std::chrono::high_resolution_clock::now();
	}

	static double timeDifference(timestamp_t later, timestamp_t earlier)
	{
		return std::chrono::duration_cast<std::chrono::nanoseconds>(later - earlier).count() / nanosecondsInSecond;
	}

	static void sleep(double seconds)
	{
		if (seconds <= 0) return;

		std::chrono::nanoseconds sleepTime(static_cast<long long>(seconds * nanosecondsInSecond));

		std::this_thread::sleep_for(sleepTime);
	}

private:
	static constexpr double nanosecondsInSecond = 1e+9;
};
