#pragma once

#include "helpers/Utils.h"

class BinaryTimer
{
public:
	BinaryTimer() : fired(false), m_interval(0), m_value(0) {}

	void update(double dt)
	{
		if (isEqual(m_interval, 0) || m_interval < 0 || fired)
		{
			return;
		}

		m_value += dt;

		if (m_value >= m_interval)
		{
			m_value = 0;
			fired = true;
		}
	}

	void setInterval(double interval)
	{
		m_interval = interval;
		m_value = 0;
		fired = false;
	}

	double interval() const
	{
		return m_interval;
	}

	double value() const
	{
		return m_value;
	}

	bool isRunning() const
	{
		return m_interval > 0;
	}

public:
	bool fired;

private:
	double m_interval;
	double m_value;
	
};