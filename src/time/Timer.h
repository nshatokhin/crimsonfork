#pragma once

#include "common/Function.h"
#include "helpers/Utils.h"

template<typename TimeoutCallbackOwner, typename TimeoutCallback>
class Timer
{
public:
	Timer() : m_interval(0), m_value(0), m_callbackOwner(nullptr), m_callbackFunction(nullptr) {}

	void setCallback(TimeoutCallbackOwner *cbOwner, TimeoutCallback cb)
	{
		assert(cbOwner && cb);
		m_callbackOwner = cbOwner;
		m_callbackFunction = cb;
	}

	void update(double dt)
	{
		if (isEqual(m_interval, 0) || m_interval < 0)
		{
			return;
		}

		m_value += dt;

		if (m_value >= m_interval)
		{
			m_value = 0;

			if (m_callbackOwner && m_callbackFunction)
				CALL_MEMBER_FN(m_callbackOwner, m_callbackFunction);
		}
	}

	void setInterval(double interval)
	{
		m_interval = interval;
		m_value = 0;
	}

	double interval() const
	{
		return m_interval;
	}

	double value() const
	{
		return m_value;
	}

	bool isRunning() const
	{
		return m_interval > 0;
	}

private:
	double m_interval;
	double m_value;

	TimeoutCallbackOwner * m_callbackOwner;
	TimeoutCallback m_callbackFunction;
};