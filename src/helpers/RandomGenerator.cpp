#include "RandomGenerator.h"

#include <ctime>
#include <random>

void RandomGenerator::init()
{
	srand(static_cast<unsigned int>(time(NULL)));
}

int32_t RandomGenerator::generate(int32_t min, uint32_t count)
{
	return rand() % count + min;
}
