#pragma once

#include <cstdint>

class RandomGenerator
{
public:
	static void init();
	static int32_t generate(int32_t min = 0, uint32_t count = UINT32_MAX);
};