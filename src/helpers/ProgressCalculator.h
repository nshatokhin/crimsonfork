#pragma once

#include <cassert>

class ProgressCalculator
{
public:
	static double progress(double value, double min, double max)
	{
		assert(value >= min && value <= max);

		return (value - min) / (max - min);
	}
};