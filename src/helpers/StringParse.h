#pragma once

#include "primitives/Coordinate2D.h"

#include <cstdint>
#include <string>

void getIntFromStr(int &width, int &height, std::string str);
bool parseCoordinateNotation(Coordinate2D &coordinate, std::string notation);
bool parseDiceNotation(uint32_t &count, uint32_t &size, std::string notation);
