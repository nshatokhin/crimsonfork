#include "StringParse.h"

#include "services/LoggerLocator.h"

#include <string>

void getIntFromStr(int &width, int &height, std::string str)
{
	size_t pos = str.find("x");
	if (pos != std::string::npos) {
		width = stoi(str.substr(0, pos));
		height = stoi(str.substr(pos + 1));
	}
}

bool parseCoordinateNotation(Coordinate2D &coordinate, std::string notation)
{
	if (notation.size() == 0)
	{
		LoggerLocator::locate()->logFatal(std::string("Coordinate string is empty. Not coordinate notation (x; y)"));
		return false;
	}
	else if (notation[0] != '(' || notation[notation.size()-1] != ')')
	{
		LoggerLocator::locate()->logFatal(std::string("Coordinate string ").append(notation).append(" is not coordinate notation (x; y)"));
		return false;
	}

	size_t pos = notation.find(";");
	if (pos != std::string::npos) {
		coordinate.x = stod(notation.substr(1, pos));
		coordinate.y = stod(notation.substr(pos + 1, notation.size() - 1));

		return true;
	}
	else
	{
		LoggerLocator::locate()->logFatal(std::string("Coordinate string ").append(notation).append(" is not coordinate notation (x; y)"));
		return false;
	}

	return false;
}

bool parseDiceNotation(uint32_t &count, uint32_t &size, std::string notation)
{
	count = 0;
	size = 0;

	size_t pos = notation.find("d");
	if (pos != std::string::npos) {
		if (pos == 0)
		{
			count = 1;
		}
		else
		{
			count = stoi(notation.substr(0, pos));
		}

		size = stoi(notation.substr(pos + 1));

		return true;
	}

	return false;
}
