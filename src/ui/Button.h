#pragma once

#include <cmath>
#include <cstdint>

#include "common/Common.h"
#include "common/Function.h"
#include "pools/ButtonsAnimationPool.h"
#include "primitives/Rectangle.h"

template<typename ActionsCallbacksOwner, typename ActionCallback>
class Button
{
public:
	enum class ButtonState : uint8_t
	{
		IDLE = 0,
		HOVER,
		PRESSED
	};

public:
	Button() : m_mouseButtonPressed(false) {}

	void init(uint32_t animationComp, const Rectangle &rectangle, ButtonsAnimationPool *animationsPool, ActionsCallbacksOwner * cbOwner, ActionCallback callback)
	{
		assert(animationsPool && cbOwner);

		animationComponent = animationComp;
		rect = rectangle;
		m_pool = animationsPool;
		m_callbackOwner = cbOwner;
		m_callbackFunction = callback;

		setState(ButtonState::IDLE);
	}

	void onMouseMove(int32_t x, int32_t y)
	{
		m_mousePosition = Coordinate2D(x, y);

		Rectangle shape = rect;
		shape.ltCorner.x = std::abs(rect.ltCorner.x);
		shape.ltCorner.y = std::abs(rect.ltCorner.y);

		if (shape.intersect(m_mousePosition))
		{
			if (m_mouseButtonPressed)
			{
				setState(ButtonState::PRESSED);
			}
			else
			{
				setState(ButtonState::HOVER);
			}
		}
		else
		{
			setState(ButtonState::IDLE);
		}
	}

	void onMouseButtonClick(int32_t x, int32_t y, bool isReleased)
	{
		m_mousePosition = Coordinate2D(x, y);
		m_mouseButtonPressed = !isReleased;

		Rectangle shape = rect;
		shape.ltCorner.x = std::abs(rect.ltCorner.x);
		shape.ltCorner.y = std::abs(rect.ltCorner.y);

		if (shape.intersect(m_mousePosition))
		{
			if (m_mouseButtonPressed)
			{
				setState(ButtonState::PRESSED);
			}
			else
			{
				setState(ButtonState::IDLE);

				CALL_MEMBER_FN(m_callbackOwner, m_callbackFunction);
			}
		}
	}

public:
	uint32_t animationComponent;
	Rectangle rect;

protected:
	inline void setState(ButtonState state)
	{
		m_pool->setState(animationComponent, static_cast<uint32_t>(state));
	}

	bool contains(int32_t x, int32_t y) {}

protected:
	
	ButtonsAnimationPool * m_pool;

	Coordinate2D m_position, m_leftTopCorner;
	uint32_t m_width, m_height;

	bool m_mouseButtonPressed;
	Coordinate2D m_mousePosition;

	ActionsCallbacksOwner * m_callbackOwner;
	ActionCallback m_callbackFunction;
	
};
