#pragma once

#include "Button.h"
#include "Engine/framework/Framework.h"
#include "common/Common.h"
#include "entities/CameraEntity.h"

#include <memory>

template<typename ActionsCallbacksOwner, typename ActionCallback>
class Popup
{
public:
	Popup() : visible(false), ltcX(0), ltcY(0), width(0), height(0) {
		animations = std::unique_ptr<ButtonsAnimationPool>(new ButtonsAnimationPool(BUTTONS_COUNT));
	}

	void init(uint32_t backgroundSprite, uint32_t action1StatesCount, ButtonAnimation * action1States,
		Rectangle action1Rect, uint32_t action2StatesCount, ButtonAnimation * action2States, Rectangle action2Rect,
		ActionsCallbacksOwner *cbOwner, ActionCallback cbAction1, ActionCallback cbAction2)
	{
		backgroundSpriteId = backgroundSprite;
		action1Button.init(animations->createAnimation(action1StatesCount, action1States), action1Rect, animations.get(), cbOwner, cbAction1);
		action2Button.init(animations->createAnimation(action2StatesCount, action2States), action2Rect, animations.get(), cbOwner, cbAction2);
	}

	void onMouseMove(int32_t x, int32_t y)
	{
		action1Button.onMouseMove(action1Button.rect.ltCorner.x > 0 ? x - ltcX : width - x + ltcX,
			action1Button.rect.ltCorner.y > 0 ? y - ltcY : height - y + ltcY);
		action2Button.onMouseMove(action2Button.rect.ltCorner.x > 0 ? x - ltcX : width - x + ltcX,
			action2Button.rect.ltCorner.y > 0 ? y - ltcY : height - y + ltcY);
	}

	void onMouseButtonClick(int32_t x, int32_t y, bool isReleased)
	{
		action1Button.onMouseButtonClick(action1Button.rect.ltCorner.x > 0 ? x - ltcX : width - x + ltcX,
			action1Button.rect.ltCorner.y > 0 ? y - ltcY : height - y + ltcY, isReleased);
		action2Button.onMouseButtonClick(action2Button.rect.ltCorner.x > 0 ? x - ltcX : width - x + ltcX,
			action2Button.rect.ltCorner.y > 0 ? y - ltcY : height - y + ltcY, isReleased);
	}

	void show() {
		visible = true;
	}

	void hide() {
		visible = false;
	}

	void update(double dt)
	{
		animations->update(dt);
	}

	void render(const CameraEntity &camera, const std::vector<Sprite *> &sprites, const std::vector<SpriteSize> &spriteSizes)
	{
		SpriteSize size = spriteSizes.at(backgroundSpriteId);
		ltcX = camera.screenWidth() / 2 - size.width / 2;
		ltcY = camera.screenHeight() / 2 - size.height / 2;
		width = size.width;
		height = size.height;

		drawSprite(sprites.at(backgroundSpriteId), ltcX, ltcY, width, height);

		uint32_t spriteX = action1Button.rect.ltCorner.x > 0 ? 
			static_cast<uint32_t>(ltcX + action1Button.rect.ltCorner.x) :
			static_cast<uint32_t>(ltcX + size.width + action1Button.rect.ltCorner.x - action1Button.rect.width);

		uint32_t spriteY = action1Button.rect.ltCorner.y > 0 ? 
			static_cast<uint32_t>(ltcY + action1Button.rect.ltCorner.y) :
			static_cast<uint32_t>(ltcY + size.height + action1Button.rect.ltCorner.y - action1Button.rect.height);

		uint32_t button1Sprite = animations->getObject(action1Button.animationComponent)->m_currentSprite;

		drawSprite(sprites.at(button1Sprite), spriteX, spriteY, action1Button.rect.width, action1Button.rect.height);

		spriteX = action2Button.rect.ltCorner.x > 0 ?
			static_cast<uint32_t>(ltcX + action2Button.rect.ltCorner.x) :
			static_cast<uint32_t>(ltcX + size.width + action2Button.rect.ltCorner.x - action2Button.rect.width);

		spriteY = action2Button.rect.ltCorner.y > 0 ?
			static_cast<uint32_t>(ltcY + action2Button.rect.ltCorner.y) :
			static_cast<uint32_t>(ltcY + size.height + action2Button.rect.ltCorner.y - action2Button.rect.height);

		uint32_t button2Sprite = animations->getObject(action2Button.animationComponent)->m_currentSprite;

		drawSprite(sprites.at(button2Sprite), spriteX, spriteY, action2Button.rect.width, action2Button.rect.height);
	}


public:
	static constexpr uint8_t BUTTONS_COUNT = 2;

	uint32_t backgroundSpriteId;
	
	Button<ActionsCallbacksOwner, ActionCallback> action1Button;
	Button<ActionsCallbacksOwner, ActionCallback> action2Button;

	std::unique_ptr<ButtonsAnimationPool> animations;

	bool visible;

protected:
	uint32_t ltcX, ltcY;
	uint32_t width, height;
};
