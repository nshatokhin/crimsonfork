#pragma once

#include "Engine/framework/Framework.h"
#include "Engine/logic/drop/ChanceChecker.h"
#include "Engine/logic/drop/DropGenerator.h"

#include <cstdint>
#include <map>
#include <memory>

#include "commands/CommandReceiver.h"
#include "entities/CameraEntity.h"
#include "entities/PlayerEntity.h"
#include "pools/BulletsPool.h"
#include "pools/BonusesPool.h"
#include "pools/EnemiesPool.h"
#include "pools/GeneralAnimationPool.h"
#include "pools/GroundTilesPool.h"
#include "pools/PhysicsPool.h"
#include "pools/PlayersAnimationPool.h"
#include "primitives/Coordinate2D.h"
#include "services/config/ConfigService.h"
#include "services/sound/SoundService.h"
#include "states/IGameState.h"
#include "structures/GameMap/GameMap.h"
#include "time/Timer.h"
#include "ui/Popup.h"
#include "weapons/Weapon.h"

class GameStateCommands;

class GameInstance : public CommandReceiver, public IGameState
{
public:
	static constexpr int PLAYERS_COUNT = 1;
	static constexpr int PLAYER_ID = 0;

public:
	GameInstance(int windowWidth, int windowHeight, int mapWidth, int mapHeight,
		int enemiesCount, int enemiesSpawn, int ammoCount, int bonusesCount, bool autoTest,
		CommandReceiver *stateChangeListener, GameStateCommands *stateChangeCommands);
	virtual ~GameInstance();

	// return : true - ok, false - failed, application will exit
	virtual bool Init() override;

	virtual void Close() override;

	// return value: if true will exit the application
	virtual bool update(double dt) override;
	virtual void render() override;

	virtual void onMouseMove(int32_t x, int32_t y, int32_t xrelative, int32_t yrelative) override;
	virtual void onMouseButtonClick(uint8_t button, bool isReleased, uint8_t clicks, int32_t x, int32_t y) override;
	virtual void onKeyPressed(SDL_Keycode k) override;
	virtual void onKeyReleased(SDL_Keycode k) override;

	void collisionHandler(uint32_t physicsComponent1, uint32_t physicsComponent2);
	void outOfWorldHandler(uint32_t physicsComponent);

	void enemiesSpawnTimeoutHandler();

	void popupRestartButtonHandler();
	void popupExitButtonHandler();

private:
	bool spawnEnemy();
	bool spawnEnemy(const Coordinate2D &position, const Coordinate2D &target);
	void destroyEnemy(uint32_t enemyId);
	void respawnEnemies();

	void destroyBullet(uint32_t bulletId);

	void spawnBonus(uint32_t id, const Coordinate2D &position);
	void destroyBonus(uint32_t bonusId);

	void spawnPlayer(const Coordinate2D &position);
	void destroyPlayer();

	void collisionPlayerEnemy(uint32_t playerId, uint32_t enemyId);
	void collisionEnemyBullet(uint32_t enemyId, uint32_t bulletId);
	void collisionPlayerBonus(uint32_t playerId, uint32_t bonusId);
	void collisionEnemyBonus(uint32_t enemyId, uint32_t bonusId);

	void victory();
	void gameOver();

	inline void drawUI();
	inline void drawScore();
	inline void drawTime();

	inline void drawEntity(uint32_t spriteId, const CoordinateScreen &pos);
	inline void drawEntity(uint32_t spriteId, const CoordinateScreen & pos, double angle, int32_t rcX = 0, int32_t rcY = 0);

private:
	CoordinateScreen m_mouseLastPos;
	bool m_autoTest;

	uint32_t m_screenWidth, m_screenHeight;
	uint32_t m_worldWidth, m_worldHeight;
	uint32_t m_maxEnemies, m_maxEnemiesSpawn, m_enemiesSpawnSimultaneously,
		m_enemiesSpawned, m_enemiesKilled;
	double m_enemiesSpawnCooldownTime;

	CameraEntity camera;
	PlayerEntity players[PLAYERS_COUNT];

	ConfigService * config;

	std::unique_ptr<SoundService> sound;

	std::unique_ptr<GeneralAnimationPool> animation;
	std::unique_ptr<PlayersAnimationPool> playersAnimation;
	std::unique_ptr<BulletsPool> bullets;
	std::unique_ptr<BonusesPool> bonuses;
	std::unique_ptr<EnemiesPool> enemies;
	std::unique_ptr<PhysicsPool> physics;

	std::unique_ptr<GameMap> gameMap;
	std::unique_ptr<GroundTilesPool> tiles;

	std::vector<Sprite *> sprites;
	std::vector<struct SpriteSize> spriteSizes;
	std::vector<Sprite *> terrains;

	uint32_t m_playerSpeedValue;
	uint32_t m_playerHitmapRadius;
	uint32_t m_playerDefaultWeapon;
	uint32_t m_playerStatesCount;
	PlayerAnimation * m_playerAnimationStates;

	EnemyParams m_enemyParams;

	uint32_t m_gameOverMusic, m_victoryMusic;

	uint32_t m_minEnemySpawnDistance, m_maxEnemySpawnDistance;
	uint32_t m_tileWidth, m_tileHeight;
	uint32_t m_gridCellWidth, m_gridCellHeight;

	std::map<uint32_t, bool> enemiesToDelete;
	std::map<uint32_t, bool> bulletsToDelete;

	bool m_gamePaused;

	typedef void(GameInstance::*EnemiesSpawnTimeoutHandler)();
	Timer<GameInstance, EnemiesSpawnTimeoutHandler> m_enemiesSpawnTimer;

	uint32_t m_uiBulletsIndicatorX, m_uiBulletsIndicatorY;
	uint32_t m_uiBulletsIndicatorSprite;
	uint32_t m_currentCursor, m_uiCursorArrowSprite, m_uiCursorAimSprite;

	uint32_t m_popupVictoryBackgroundSprite;
	uint32_t m_popupGameOverBackgroundSprite;

	uint32_t m_popupExitButtonStatesCount;
	ButtonAnimation * m_popupExitButtonAnimation;
	int32_t m_popupExitButtonX, m_popupExitButtonY;
	uint32_t m_popupExitButtonWidth, m_popupExitButtonHeight;

	uint32_t m_popupRestartButtonStatesCount;
	ButtonAnimation * m_popupRestartButtonAnimation;
	int32_t m_popupRestartButtonX, m_popupRestartButtonY;
	uint32_t m_popupRestartButtonWidth, m_popupRestartButtonHeight;

	typedef void(GameInstance::*PopupActionHandler)();
	Popup<GameInstance, PopupActionHandler> m_gameOverPopup;

	uint32_t m_fontTimeDelimiter;
	FontVector m_scoreFont, m_timeFont;
	uint32_t m_scoreIndicatorY, m_timeIndicatorX, m_timeIndicatorY;
	bool m_showScore, m_showTime;
	double m_elapsedTime;

	uint32_t m_wallSpriteId;
	uint32_t m_occupationCellWidth, m_occupationCellHeight;

	bool m_showDebugInfo;

	ChanceChecker m_chanceChecker;
	DropGenerator m_dropGenerator;
};
