#include "GameInstance.h"

#include "bonuses/Bonus.h"
#include "commands/GameStateCommands.h"
#include "helpers/ProgressCalculator.h"
#include "helpers/RandomGenerator.h"
#include "services/AnimationPoolLocator.h"
#include "services/BulletsPoolLocator.h"
#include "services/ConfigLocator.h"
#include "services/PhysicsPoolLocator.h"
#include "services/PlayersAnimationPoolLocator.h"
#include "services/SoundLocator.h"

#include <string>
#include <vector>

GameInstance::GameInstance(int windowWidth, int windowHeight, int mapWidth, int mapHeight,
	int enemiesCount, int enemiesSpawn, int ammoCount, int bonusesCount, bool autoTest,
	CommandReceiver *stateChangeListener, GameStateCommands *stateChangeCommands) : CommandReceiver(), IGameState(stateChangeListener, stateChangeCommands),
	m_autoTest(autoTest),
	m_screenWidth(windowWidth),
	m_screenHeight(windowHeight),
	m_worldWidth(mapWidth),
	m_worldHeight(mapHeight),
	m_maxEnemies(enemiesCount),
	m_maxEnemiesSpawn(enemiesSpawn),
	m_enemiesSpawnSimultaneously(0),
	m_enemiesSpawnCooldownTime(0),
	m_enemiesSpawned(0),
	m_enemiesKilled(0),
	m_gamePaused(false),
	m_showDebugInfo(false),
	m_chanceChecker(10000) // TODO: load from config
{
	config = ConfigLocator::locate();

	sound = std::unique_ptr<SoundService>(new SoundService(config->sounds(), config->music()));
	SoundLocator::provide(sound.get());

	sound->setMusicVolume(config->musicVolume());
	sound->setSoundsVolume(config->soundsVolume());

	camera.init(Coordinate2D(windowWidth / 2, windowHeight / 2), windowWidth, windowHeight, m_worldWidth, m_worldHeight);

	animation = std::unique_ptr<GeneralAnimationPool>(new GeneralAnimationPool(PLAYERS_COUNT + enemiesSpawn + ammoCount));
	AnimationPoolLocator::provide(animation.get());

	playersAnimation = std::unique_ptr<PlayersAnimationPool>(new PlayersAnimationPool(PLAYERS_COUNT));
	PlayersAnimationPoolLocator::provide(playersAnimation.get());

	bullets = std::unique_ptr<BulletsPool>(new BulletsPool(ammoCount));
	BulletsPoolLocator::provide(bullets.get());

	bonuses = std::unique_ptr<BonusesPool>(new BonusesPool(bonusesCount));

	uint32_t gridXCount = 25;
	uint32_t gridYCount = 14;
	m_occupationCellWidth = 50;
	m_occupationCellHeight = 50;

	gameMap = std::unique_ptr<GameMap>(new GameMap(m_worldWidth, m_worldHeight, gridXCount, gridYCount));

	enemies = std::unique_ptr<EnemiesPool>(new EnemiesPool(enemiesSpawn, enemiesSpawn + PLAYERS_COUNT + ammoCount, *gameMap.get()));

	// TODO: reserve somehow memory for bulletsToDelete and enemiesToDelete containers
	// Maybe I should to write my own Allocator that will take memory from object pool

	physics = std::unique_ptr<PhysicsPool>(new PhysicsPool(enemiesSpawn + PLAYERS_COUNT + ammoCount, Coordinate2D(-mapWidth/2, -mapHeight/2), mapWidth, mapHeight, config->gridCellWidth(), config->gridCellHeight(), m_occupationCellWidth, m_occupationCellHeight, this)); // TODO: load from JSON
	PhysicsPoolLocator::provide(physics.get());

	m_tileWidth = config->tileWidth();
	m_tileHeight = config->tileHeight();

	tiles = std::unique_ptr<GroundTilesPool>(new GroundTilesPool(config->terrainTilesCount(), mapWidth, mapHeight, m_tileWidth, m_tileHeight));
}

GameInstance::~GameInstance()
{
}

bool GameInstance::Init()
{
	showCursor(false);

	tiles->generateTiles();

	m_minEnemySpawnDistance = config->minEnemySpawnDistance();
	m_maxEnemySpawnDistance = config->maxEnemySpawnDistance();

	m_playerSpeedValue = config->playerSpeed();
	m_playerHitmapRadius = config->playerHitmapRadius();
	m_playerDefaultWeapon = config->playerDefaultWeapon();
	m_playerStatesCount = config->playerStatesCount();
	m_playerAnimationStates = config->playerAnimationStates();

	m_enemyParams = config->enemyParams();

	Weapon::weaponParams = config->weapons();

	Bonus::params = config->bonuses();
	for (size_t i = 0; i < Bonus::params.size(); i++)
	{
		m_dropGenerator.add(Bonus::params[i].id, Bonus::params[i].dropChance);
	}

	m_gameOverMusic = config->gameOverMusic();
	m_victoryMusic = config->victoryMusic();

	m_enemiesSpawnSimultaneously = config->enemiesSpawnSimultaneously();
	m_enemiesSpawnCooldownTime = config->enemiesSpawnCooldownTime();

	m_uiBulletsIndicatorX = config->gameBulletIndicatorX();
	m_uiBulletsIndicatorY = config->gameBulletIndicatorY();
	m_uiBulletsIndicatorSprite = config->gameBulletIndicatorSpriteId();
	m_uiCursorArrowSprite = config->cursorArrowSpriteId();
	m_uiCursorAimSprite = config->cursorAimSpriteId();
	m_currentCursor = m_uiCursorAimSprite;
	
	m_popupVictoryBackgroundSprite = config->gameOverPopupVictoryBackground();
	m_popupGameOverBackgroundSprite = config->gameOverPopupFailBackground();

	m_popupExitButtonStatesCount = config->gameOverPopupExitButtonStates();
	m_popupExitButtonAnimation = config->gameOverPopupExitButtonAnimation();
	m_popupExitButtonX = config->gameOverPopupExitButtonX();
	m_popupExitButtonY = config->gameOverPopupExitButtonY();
	m_popupExitButtonWidth = config->gameOverPopupExitButtonWidth();
	m_popupExitButtonHeight = config->gameOverPopupExitButtonHeight();

	m_popupRestartButtonStatesCount = config->gameOverPopupRestartButtonStates();
	m_popupRestartButtonAnimation = config->gameOverPopupRestartButtonAnimation();
	m_popupRestartButtonX = config->gameOverPopupRestartButtonX();
	m_popupRestartButtonY = config->gameOverPopupRestartButtonY();
	m_popupRestartButtonWidth = config->gameOverPopupRestartButtonWidth();
	m_popupRestartButtonHeight = config->gameOverPopupRestartButtonHeight();

	m_fontTimeDelimiter = config->fontTimeDelimiter();
	m_scoreFont = config->scoreFont();
	m_timeFont = config->timeFont();

	m_showScore = m_scoreFont.size() >= 10;
	m_showTime = m_timeFont.size() >= 10;

	m_scoreIndicatorY = config->scoreIndicatorY();
	m_timeIndicatorX = config->timeIndicatorX();
	m_timeIndicatorY = config->timeIndicatorY();

	m_wallSpriteId = config->wallSpriteId();

	sprites.clear();
	spriteSizes.clear();
	struct SpriteSize size;

	SpritesVector spritePathes = config->sprites();

	for (size_t i = 0; i < spritePathes.size(); i++)
	{
		sprites.push_back(loadSprite(spritePathes.at(i).data(), size.width, size.height));
		
		spriteSizes.push_back(size);
	}

	TerrainVector terrainPathes = config->terrains();
	terrains.clear();

	for (size_t i = 0; i < terrainPathes.size(); i++)
	{
		terrains.push_back(loadSprite(terrainPathes.at(i).data(), size.width, size.height));
	}

	m_enemiesSpawnTimer.setCallback(this, &GameInstance::enemiesSpawnTimeoutHandler);
	m_enemiesSpawnTimer.setInterval(0);

	m_gameOverPopup.init(m_popupGameOverBackgroundSprite, m_popupRestartButtonStatesCount, m_popupRestartButtonAnimation,
		Rectangle(Coordinate2D(m_popupRestartButtonX, m_popupRestartButtonY), m_popupRestartButtonWidth, m_popupRestartButtonHeight),
		m_popupExitButtonStatesCount, m_popupExitButtonAnimation, Rectangle(Coordinate2D(m_popupExitButtonX, m_popupExitButtonY), m_popupExitButtonWidth, m_popupExitButtonHeight),
		this, &GameInstance::popupRestartButtonHandler, &GameInstance::popupExitButtonHandler);

	spawnPlayer(Coordinate2D(-500, -300));
	spawnEnemy(Coordinate2D(-500, 40), Coordinate2D(-100, 250)); spawnEnemy(Coordinate2D(-100, 250), Coordinate2D(-500, 40));
	spawnEnemy(Coordinate2D(-500, 110), Coordinate2D(-100, 180)); spawnEnemy(Coordinate2D(-100, 180), Coordinate2D(-500, 110));
	spawnEnemy(Coordinate2D(-500, 180), Coordinate2D(-100, 110)); spawnEnemy(Coordinate2D(-100, 110), Coordinate2D(-500, 180));
	spawnEnemy(Coordinate2D(-500, 250), Coordinate2D(-100, 40)); spawnEnemy(Coordinate2D(-100, 40), Coordinate2D(-500, 250));

	spawnEnemy(Coordinate2D(-100, -250), Coordinate2D(500, -250)); spawnEnemy(Coordinate2D(500, -250), Coordinate2D(-100, -250));

	spawnEnemy(Coordinate2D(500, 180), Coordinate2D(100, 180)); spawnEnemy(Coordinate2D(100, 180), Coordinate2D(500, 180));

	spawnBonus(0, Coordinate2D(-400, -300));

	sound->playMusic(config->gameMusic(), true);

	m_elapsedTime = 0;
	
	return true;
}

void GameInstance::Close()
{
	destroyPlayer();

	m_enemiesSpawnTimer.setInterval(0);

	for (size_t i = 0; i < sprites.size(); i++)
	{
		destroySprite(sprites.at(i));
	}

	for (size_t i = 0; i < terrains.size(); i++)
	{
		destroySprite(terrains.at(i));
	}

	sprites.clear();
	spriteSizes.clear();
	terrains.clear();	
}

bool GameInstance::update(double dt)
{
	if (!m_gamePaused)
	{
		m_elapsedTime += dt;

		if (enemies->activeCount() < m_maxEnemiesSpawn &&
			m_enemiesSpawned < m_maxEnemies && !m_enemiesSpawnTimer.isRunning())
		{
			m_enemiesSpawnTimer.setInterval(m_enemiesSpawnCooldownTime);
		}

		enemies->targetPos = physics->getObject(players[PLAYER_ID].physicsComponent)->position();
		enemies->update(dt);

		physics->update(dt);
		animation->update(dt);
		playersAnimation->update(dt);

		players[PLAYER_ID].weapon.update(dt);

		//camera.setPosition(physics->getObject(players[PLAYER_ID].physicsComponent)->position());

		// Timers
		m_enemiesSpawnTimer.update(dt);

		// BLOODY HARVEST!!!
		for (auto i = bulletsToDelete.begin(); i != bulletsToDelete.end(); i++)
		{
			destroyBullet(i->first);
		}
		bulletsToDelete.clear();

		for (auto i = enemiesToDelete.begin(); i != enemiesToDelete.end(); i++)
		{
			destroyEnemy(i->first);
		}
		enemiesToDelete.clear();
	}

	if (m_gameOverPopup.visible)
	{
		m_gameOverPopup.update(dt);
	}

	return false;
}

void GameInstance::render()
{
	// Background
	TilesFieldSize fieldSize = tiles->tilesFieldSize();
	VisibleTiles vt = tiles->visibleTiles(camera.screenLeftTopCornerPosition(), camera.screenWidth(), camera.screenHeight());
	GroundTile tile;
	CoordinateScreen tilePos;

	for (uint32_t i = vt.left; i < vt.horizontalTilesCount; i++)
	{
		for (uint32_t j = vt.top; j < vt.verticalTilesCount; j++)
		{
			tile = tiles->tiles[j * fieldSize.horizontalCount + i];
			tilePos = camera.translateGlobalToScreen(Coordinate2D(
				static_cast<int32_t>(i*m_tileWidth) - static_cast<int32_t>(m_worldWidth / 2),
				static_cast<int32_t>(j*m_tileHeight) - static_cast<int32_t>(m_worldHeight / 2)
			));
			drawSprite(terrains[tile], tilePos.x, tilePos.y, m_tileWidth, m_tileHeight);
		}
	}

	Graph &graph = gameMap->graph();

	for (IdxType i = 0; i < graph.numNodes(); i++)
	{
		Graph::NodeType node = graph.GetNode(i);
		if (node.index == invalid_node_index)
		{
			drawEntity(m_wallSpriteId, camera.translateGlobalToScreen(node.position));
		}
	}

	CoordinateScreen screenPos;

	if (m_showDebugInfo)
	{
		OccupationPartition &occupationGrid = physics->occupationGrid();
		int32_t offsetX = -static_cast<int32_t>(m_worldWidth / 2);
		int32_t offsetY = -static_cast<int32_t>(m_worldHeight / 2);

		for (uint32_t i = 0; i < occupationGrid.cellsHorCount(); i++)
		{
			for (uint32_t j = 0; j < occupationGrid.cellsVertCount(); j++)
			{
				if (occupationGrid.isOccupied(i, j))
				{
					screenPos = camera.translateGlobalToScreen(Coordinate2D(offsetX + static_cast<int64_t>(i * m_occupationCellWidth), offsetY + static_cast<int64_t>(j * m_occupationCellHeight)));
					fillRectangle(screenPos.x, screenPos.y, m_occupationCellWidth, m_occupationCellHeight, 255, 0, 0, 200);
				}
			}
		}
	}

	PhysicsComponent * pc;

	BonusEntity * bonusEntities = bonuses->objects();

	for (uint32_t i = 0; i < bonuses->activeCount(); i++)
	{
		pc = physics->getObject(bonusEntities[i].physicsComponent);
		screenPos = camera.translateGlobalToScreen(pc->position());
		drawEntity(animation->getObject(bonusEntities[i].animationComponent)->m_currentSprite, screenPos);
	}

	EnemyEntity * enemyEntities = enemies->objects();
	Coordinate2D position, feelerPos;
	CoordinateScreen lineStart, lineEnd, temp;

	for (uint32_t i = 0; i < enemies->activeCount(); i++)
	{
		pc = physics->getObject(enemyEntities[i].physicsComponent);
		position = pc->position();
		screenPos = camera.translateGlobalToScreen(position);
		drawEntity(animation->getObject(enemyEntities[i].animationComponent)->m_currentSprite, screenPos);

		if (m_showDebugInfo)
		{
			for (uint32_t j = 0; j < enemyEntities[i].FEELERS_COUNT; j++)
			{
				feelerPos = position + enemyEntities[i].feelers[j].rotate(Coordinate2D(0, 0), pc->speedAngle());
				temp = camera.translateGlobalToScreen(feelerPos);
				drawLine(screenPos.x, screenPos.y, temp.x, temp.y, 255, 255, 0, 255);
			}

			temp = camera.translateGlobalToScreen(position + pc->orientation * pc->hitmap.points[0].x);
			drawLine(screenPos.x, screenPos.y, temp.x, temp.y, 255, 0, 0, 255);

			if (!enemyEntities[i].path.empty())
			{
				std::list<Coordinate2D>::const_iterator iter = enemyEntities[i].path.begin();
				lineStart = camera.translateGlobalToScreen(*iter);
				iter++;

				for (; iter != enemyEntities[i].path.end(); iter++)
				{
					lineEnd = camera.translateGlobalToScreen(*iter);

					drawLine(lineStart.x, lineStart.y, lineEnd.x, lineEnd.y, 0, 0, 255, 255);

					lineStart = lineEnd;
				}

				lineStart = screenPos;
				lineEnd = camera.translateGlobalToScreen(enemyEntities[i].currentTarget);
				drawLine(lineStart.x, lineStart.y, lineEnd.x, lineEnd.y, 0, 255, 255, 255);
			}
		}
	}

	for (uint32_t i = 0; i < PLAYERS_COUNT; i++)
	{
		pc = physics->getObject(players[PLAYER_ID].physicsComponent);
		Coordinate2D globalPos = pc->position();
		CoordinateScreen pos = camera.translateGlobalToScreen(globalPos);
		//CoordinateScreen bulletSpawnPoint = camera.translateGlobalToScreen(globalPos + players[PLAYER_ID].bulletSpawnPoint);
		//CoordinateScreen towerDoorPos = camera.translateGlobalToScreen(globalPos + players[PLAYER_ID].towerDoorPosition);

		drawEntity(playersAnimation->getObject(players[PLAYER_ID].animationComponent)->m_currentSprite, pos);

		// Draw cannon
		drawEntity(animation->getObject(players[PLAYER_ID].weapon.animationComponent)->m_currentSprite, pos, players[PLAYER_ID].weapon.weaponOrientation.rotationAngle(players[PLAYER_ID].weapon.baseWeaponOrientation));
		/*size = spriteSizes[m_playerCannonBaseSpriteId];
		uint32_t cannonSegmentLength = players[PLAYER_ID].cannonLength / (size.width / 2);

		for (uint32_t i = 0; i < players[PLAYER_ID].cannonLength; i += cannonSegmentLength)
		{
			drawEntity(m_playerCannonBaseSpriteId, pos + i * players[PLAYER_ID].weaponOrientation);
		}
		
		// Draw Tower
		drawEntity(m_playerTowerBaseSpriteId, pos);

		// Tower door sprite
		drawEntity(m_playerTowerDoorSpriteId, towerDoorPos);

		// Cannon end sprite
		//drawEntity(m_playerCannonSpriteId, bulletSpawnPoint);*/
	}

	BulletEntity * bulletEntities = bullets->objects();

	for (uint32_t i = 0; i < bullets->activeCount(); i++)
	{
		pc = physics->getObject(bulletEntities[i].physicsComponent);
		drawEntity(animation->getObject(bulletEntities[i].animationComponent)->m_currentSprite, camera.translateGlobalToScreen(pc->position()));
	}

	drawUI();
}

void GameInstance::onMouseMove(int32_t x, int32_t y, int32_t xrelative, int32_t yrelative)
{
	m_mouseLastPos = CoordinateScreen(x, y);

	if (m_gameOverPopup.visible)
	{
		m_gameOverPopup.onMouseMove(x, y);
	}

	if (m_gamePaused) return;

	players[PLAYER_ID].commands.aimTo.target = camera.translateScreenToGlobal(m_mouseLastPos);
	players[PLAYER_ID].processCommand(&players[PLAYER_ID].commands.aimTo);
}

void GameInstance::onMouseButtonClick(uint8_t button, bool isReleased, uint8_t clicks, int32_t x, int32_t y)
{
	if (m_gameOverPopup.visible && button == 1)
	{
		m_gameOverPopup.onMouseButtonClick(m_mouseLastPos.x, m_mouseLastPos.y, isReleased);
	}

	if (m_gamePaused) return;

	if (button == 1)
	{
		if (!isReleased)
		{
			players[PLAYER_ID].commands.aimTo.target = camera.translateScreenToGlobal(m_mouseLastPos);
			players[PLAYER_ID].processCommand(&players[PLAYER_ID].commands.aimTo);

			players[PLAYER_ID].weapon.pullTrigger();
		}
		else
		{
			players[PLAYER_ID].weapon.releaseTriger();
		}
	}
}

void GameInstance::onKeyPressed(SDL_Keycode k)
{
	if (k == SDLK_F1)
	{
		m_showDebugInfo = !m_showDebugInfo;
	}

	if (m_gamePaused) return;

	if (k == SDLK_UP) {
		players[PLAYER_ID].processCommand(&players[PLAYER_ID].commands.upPress);
	}

	if (k == SDLK_RIGHT) {
		players[PLAYER_ID].processCommand(&players[PLAYER_ID].commands.rightPress);
	}

	if (k == SDLK_DOWN) {
		players[PLAYER_ID].processCommand(&players[PLAYER_ID].commands.downPress);
	}

	if (k == SDLK_LEFT) {
		players[PLAYER_ID].processCommand(&players[PLAYER_ID].commands.leftPress);
	}
}

void GameInstance::onKeyReleased(SDL_Keycode k)
{
	if (m_gamePaused) return;

	if (k == SDLK_UP) {
		players[PLAYER_ID].processCommand(&players[PLAYER_ID].commands.upRelease);
	}

	if (k == SDLK_RIGHT) {
		players[PLAYER_ID].processCommand(&players[PLAYER_ID].commands.rightRelease);
	}

	if (k == SDLK_DOWN) {
		players[PLAYER_ID].processCommand(&players[PLAYER_ID].commands.downRelease);
	}

	if (k == SDLK_LEFT) {
		players[PLAYER_ID].processCommand(&players[PLAYER_ID].commands.leftRelease);
	}
}

void GameInstance::collisionHandler(uint32_t physicsComponent1, uint32_t physicsComponent2)
{
	PhysicsComponent * component1 = physics->getObject(physicsComponent1);
	PhysicsComponent * component2 = physics->getObject(physicsComponent2);

	switch (component1->type)
	{
		case EntityType::PLAYER:
			switch (component2->type)
			{
				case EntityType::ENEMY:
					collisionPlayerEnemy(component1->entityId, component2->entityId);
					break;

				case EntityType::BONUS:
					collisionPlayerBonus(component1->entityId, component2->entityId);
					break;

				default: break;
			}
			break;

		case EntityType::ENEMY:
			switch (component2->type)
			{
				case EntityType::PLAYER:
					collisionPlayerEnemy(component2->entityId, component1->entityId);
					break;

				case EntityType::BULLET:
					collisionEnemyBullet(component1->entityId, component2->entityId);
					break;

				case EntityType::BONUS:
					collisionEnemyBonus(component1->entityId, component2->entityId);
					break;

				default: break;
			}
			break;

		case EntityType::BULLET:
			switch (component2->type)
			{
				case EntityType::ENEMY:
					collisionEnemyBullet(component2->entityId, component1->entityId);
					break;
		
				default: break;
			}
			break;

		case EntityType::BONUS:
			switch (component2->type)
			{
			case EntityType::PLAYER:
				collisionPlayerBonus(component2->entityId, component1->entityId);
				break;
			case EntityType::ENEMY:
				collisionEnemyBonus(component2->entityId, component1->entityId);
				break;
			}
			break;
		}
}

void GameInstance::outOfWorldHandler(uint32_t physicsComponent)
{
	PhysicsComponent * component = physics->getObject(physicsComponent);
	if (component->type == EntityType::BULLET)
	{
		bulletsToDelete.insert({ component->entityId, true });
	}
}

bool GameInstance::spawnEnemy()
{
	if (enemies->activeCount() >= m_maxEnemiesSpawn || m_enemiesSpawned >= m_maxEnemies)
	{
		return false;
	}

	Coordinate2D position(RandomGenerator::generate(- static_cast<int32_t>(m_worldWidth / 2), m_worldWidth), RandomGenerator::generate(-static_cast<int32_t>(m_worldHeight / 2), m_worldHeight));
	Coordinate2D playerPosition = physics->getObject(players[PLAYER_ID].physicsComponent)->position();

	Vector2D dirVector(position, playerPosition), enemyOffsetVector = dirVector;
	double distanceToPlayer = dirVector.length();

	if (distanceToPlayer < m_minEnemySpawnDistance)
	{
		enemyOffsetVector = dirVector.unitVector() * m_minEnemySpawnDistance;
		position = enemyOffsetVector + playerPosition;
	}
	else if (distanceToPlayer > m_maxEnemySpawnDistance)
	{
		enemyOffsetVector = dirVector.unitVector() * m_maxEnemySpawnDistance;
		position = enemyOffsetVector + playerPosition;
	}

	if (position.x < - static_cast<int32_t>(m_worldWidth / 2) ||
		position.x > m_worldWidth / 2)
	{
		position.x = playerPosition.x - enemyOffsetVector.x;
	}

	if (position.y < - static_cast<int32_t>(m_worldHeight / 2) ||
		position.y > m_worldHeight / 2)
	{
		position.y = playerPosition.y - enemyOffsetVector.y;
	}

	uint32_t physicsId = physics->createPhysicsComponent(EntityType::ENEMY, position, Vector2D(0, 0), m_enemyParams.hitmapRadius, m_enemyParams.mass, true);
	uint32_t enemyId = enemies->createEnemy(playerPosition, m_enemyParams.maxSpeed, physicsId, animation->createAnimation(m_enemyParams.animationStatesCount, m_enemyParams.animationStates), m_enemyParams.maxHP);
	physics->getObject(physicsId)->entityId = enemyId;

	m_enemiesSpawned++;

	return true;
}

bool GameInstance::spawnEnemy(const Coordinate2D &position, const Coordinate2D &target)
{
	if (enemies->activeCount() >= m_maxEnemiesSpawn || m_enemiesSpawned >= m_maxEnemies)
	{
		return false;
	}

	
	uint32_t physicsId = physics->createPhysicsComponent(EntityType::ENEMY, position, Vector2D(0, 0), m_enemyParams.hitmapRadius, m_enemyParams.mass, true);
	uint32_t enemyId = enemies->createEnemy(target, m_enemyParams.maxSpeed, physicsId, animation->createAnimation(m_enemyParams.animationStatesCount, m_enemyParams.animationStates), m_enemyParams.maxHP);
	physics->getObject(physicsId)->entityId = enemyId;

	m_enemiesSpawned++;

	return true;
}

void GameInstance::destroyEnemy(uint32_t enemyId)
{
	EnemyEntity * enemy = enemies->getObject(enemyId);
	Coordinate2D position = physics->getObject(enemy->physicsComponent)->position();

	animation->destroyObject(enemy->animationComponent);
	physics->destroyPhysicsComponent(enemy->physicsComponent);

	enemies->destroyEnemy(enemyId);

	m_enemiesKilled++;

	sound->playSound(m_enemyParams.deathSound);

	if (m_chanceChecker.check(0.5)) // TODO: load from config
	{
		uint32_t bonusId = m_dropGenerator.generate();

		spawnBonus(bonusId, position);
	}

	if (m_enemiesKilled >= m_maxEnemies)
	{
		victory();
	}
}

void GameInstance::respawnEnemies()
{
	if (enemies->activeCount() >= m_maxEnemiesSpawn || m_enemiesSpawned >= m_maxEnemies)
	{
		return;
	}

	for (uint32_t i = 0; i < m_enemiesSpawnSimultaneously; i++)
	{
		if (!spawnEnemy())
		{
			return;
		}
	}

	m_enemiesSpawnTimer.setInterval(m_enemiesSpawnCooldownTime);
}

void GameInstance::destroyBullet(uint32_t bulletId)
{
	bullets->destroyBullet(bulletId);
}

void GameInstance::spawnBonus(uint32_t id, const Coordinate2D &position)
{
	uint32_t physicsId = physics->createPhysicsComponent(EntityType::BONUS, position, Vector2D(0, 0), Bonus::params[id].hitmapRadius, 1, true);
	uint32_t bonusId = bonuses->createBonus(0, Bonus::params[id].modifier, physicsId, animation->createAnimation(Bonus::params[id].animationStatesCount, Bonus::params[id].animationStates));
	physics->getObject(physicsId)->entityId = bonusId;
}

void GameInstance::destroyBonus(uint32_t bonusId)
{
	BonusEntity * bonus = bonuses->getObject(bonusId);

	animation->destroyObject(bonus->animationComponent);
	physics->destroyPhysicsComponent(bonus->physicsComponent);

	bonuses->destroyBonus(bonusId);
}

void GameInstance::spawnPlayer(const Coordinate2D &position)
{
	uint32_t physicsId = physics->createPhysicsComponent(EntityType::PLAYER, position, Vector2D(0, 0), m_playerHitmapRadius, 1, false);
	players[PLAYER_ID].init(m_playerSpeedValue, physicsId, playersAnimation->createAnimation(m_playerStatesCount, m_playerAnimationStates), 100); // TODO: load from config
	
	players[PLAYER_ID].weapon = Weapon::CreateWeapon(m_playerDefaultWeapon);
	players[PLAYER_ID].weapon.ownerPhysicsComponent = players[PLAYER_ID].physicsComponent;

	PhysicsComponent * pc = physics->getObject(physicsId);
	pc->entityId = PLAYER_ID;

	//camera.setPosition(pc->position());
	camera.setPosition(Coordinate2D(0, 0));
}

void GameInstance::destroyPlayer()
{
	physics->destroyPhysicsComponent(players[PLAYER_ID].physicsComponent);
	playersAnimation->destroyObject(players[PLAYER_ID].animationComponent);
}

void GameInstance::collisionPlayerEnemy(uint32_t playerId, uint32_t enemyId)
{
	//gameOver();
}

void GameInstance::collisionEnemyBullet(uint32_t enemyId, uint32_t bulletId)
{
	enemiesToDelete.insert({ enemyId, true });
	bulletsToDelete.insert({ bulletId, true });
}

void GameInstance::collisionPlayerBonus(uint32_t playerId, uint32_t bonusId)
{
	BonusEntity &bonusEntity = *bonuses->getObject(bonusId);

	Modifier::Apply<PlayerEntity>(players[playerId], bonusEntity.modifier);

	destroyBonus(bonusId);
}

void GameInstance::collisionEnemyBonus(uint32_t enemyId, uint32_t bonusId)
{
}

void GameInstance::victory()
{
	if (m_autoTest)
	{
		m_stateChangeListener->processCommand(&m_stateChangeCommands->restartStateCommand);
	}
	else
	{
		m_gamePaused = true;
		m_gameOverPopup.backgroundSpriteId = m_popupVictoryBackgroundSprite;
		m_gameOverPopup.visible = true;
		m_currentCursor = m_uiCursorArrowSprite;
		sound->playMusic(m_victoryMusic, false);
	}
}

void GameInstance::gameOver()
{
	if (m_autoTest)
	{
		m_stateChangeListener->processCommand(&m_stateChangeCommands->restartStateCommand);
	}
	else
	{
		m_gamePaused = true;
		m_gameOverPopup.backgroundSpriteId = m_popupGameOverBackgroundSprite;
		m_gameOverPopup.visible = true;
		m_currentCursor = m_uiCursorArrowSprite;
		sound->playMusic(m_gameOverMusic, false);
	}
}

void GameInstance::drawUI()
{
	Sprite * sprite = sprites[m_uiBulletsIndicatorSprite];
	SpriteSize size = spriteSizes[m_uiBulletsIndicatorSprite];

	for (uint32_t i = 0; i < players[PLAYER_ID].weapon.ammoLoadedCount; i++) {
		drawSprite(sprite, m_uiBulletsIndicatorX + i * size.width, m_uiBulletsIndicatorY, size.width, size.height);
	}

	drawScore();
	drawTime();

	if (m_gameOverPopup.visible)
	{
		m_gameOverPopup.render(camera, sprites, spriteSizes);
	}

	// Mouse pointer
	size = spriteSizes[m_currentCursor];
	drawSprite(sprites[m_currentCursor], m_mouseLastPos.x - size.width / 2, m_mouseLastPos.y - size.height / 2, size.width, size.height);
}

void GameInstance::drawScore()
{
	if (!m_showScore) return;

	uint32_t score = m_maxEnemies - m_enemiesKilled, temp = score;
	uint8_t digit;
	uint32_t decimalPlaces = 0;

	do {
		decimalPlaces++;
		temp /= 10;
	} while(temp > 0);

	SpriteSize size = spriteSizes[m_scoreFont[0]];
	uint32_t labelWidth = decimalPlaces * size.width;
	uint32_t xOffset = 0;

	for (uint32_t i = 0; i < decimalPlaces; i++)
	{
		digit = score % 10;
		score /= 10;

		size = spriteSizes[m_scoreFont[digit]];
		xOffset += size.width;
		drawSprite(sprites[m_scoreFont[digit]], m_screenWidth / 2 + labelWidth / 2 - xOffset, m_scoreIndicatorY - size.height / 2, size.width, size.height);
	}
}

void GameInstance::drawTime()
{
	if (!m_showTime) return;

	uint8_t seconds = static_cast<uint32_t>(m_elapsedTime) % 60;
	uint32_t minutes = static_cast<uint32_t>(m_elapsedTime) / 60;

	uint8_t digit;
	uint32_t xOffset = 0;
	SpriteSize size;

	// Seconds
	for (uint32_t i = 0; i < 2; i++)
	{
		digit = seconds % 10;
		seconds /= 10;

		size = spriteSizes[m_timeFont[digit]];
		xOffset += size.width;
		drawSprite(sprites[m_timeFont[digit]], m_screenWidth - m_timeIndicatorX - xOffset, m_timeIndicatorY, size.width, size.height);
	}

	// Delimiter
	size = spriteSizes[m_fontTimeDelimiter];
	xOffset += size.width;
	drawSprite(sprites[m_fontTimeDelimiter], m_screenWidth - m_timeIndicatorX - xOffset, m_timeIndicatorY, size.width, size.height);

	// Minutes
	uint32_t decimalPlaces = 0, temp = minutes;

	do {
		decimalPlaces++;
		temp /= 10;
	} while (temp > 0);

	for (uint32_t i = 0; i < decimalPlaces; i++)
	{
		digit = minutes % 10;
		minutes /= 10;

		size = spriteSizes[m_timeFont[digit]];
		xOffset += size.width;
		drawSprite(sprites[m_timeFont[digit]], m_screenWidth - m_timeIndicatorX - xOffset, m_timeIndicatorY, size.width, size.height);
	}
}

void GameInstance::drawEntity(uint32_t spriteId, const CoordinateScreen & pos)
{
	SpriteSize &size = spriteSizes[spriteId];

	if ((pos.x >= -size.width / 2 && pos.x <= static_cast<int64_t>(m_worldWidth) + size.width / 2) && (pos.y >= -size.height / 2 && pos.y <= static_cast<int64_t>(m_worldHeight) + size.height / 2))
	{
		drawSprite(sprites[spriteId], pos.x - size.width / 2, pos.y - size.height / 2, size.width, size.height);
	}
}

void GameInstance::drawEntity(uint32_t spriteId, const CoordinateScreen & pos, double angle, int32_t rcX, int32_t rcY)
{
	SpriteSize &size = spriteSizes[spriteId];

	if ((pos.x >= -size.width / 2 && pos.x <= static_cast<int64_t>(m_worldWidth) + size.width / 2) && (pos.y >= -size.height / 2 && pos.y <= static_cast<int64_t>(m_worldHeight) + size.height / 2))
	{
		drawSpriteRotated(sprites[spriteId], pos.x - size.width / 2, pos.y - size.height / 2, size.width, size.height, angle * 180 / M_PI, rcX + size.width / 2, rcY + size.height / 2);
	}
}

void GameInstance::enemiesSpawnTimeoutHandler()
{
	m_enemiesSpawnTimer.setInterval(0);
	//respawnEnemies();
}

void GameInstance::popupRestartButtonHandler()
{
	m_gameOverPopup.visible = false;
	m_currentCursor = m_uiCursorAimSprite;
	m_stateChangeListener->processCommand(&m_stateChangeCommands->restartStateCommand);
}

void GameInstance::popupExitButtonHandler()
{
	m_gameOverPopup.visible = false;
	m_currentCursor = m_uiCursorAimSprite;
	m_stateChangeListener->processCommand(&m_stateChangeCommands->menuStateCommand);
}
