#include "LoadingInstance.h"

#include "Engine/framework/Framework.h"

#include "services/ConfigLocator.h"
#include "services/SoundLocator.h"

#define BG_SPRITE "./assets/main_screen/logo.png"
#define TILE_SPRITE "./assets/main_screen/bg_tile.png"
#define LOADING_SPRITE_1 "./assets/loading/loading_sprite_0.png"
#define LOADING_SPRITE_2 "./assets/loading/loading_sprite_1.png"
#define LOADING_SPRITE_3 "./assets/loading/loading_sprite_2.png"

LoadingInstance::LoadingInstance(int windowWidth, int windowHeight, CommandReceiver * stateChangeListener, GameStateCommands * stateChangeCommands) : IGameState(stateChangeListener, stateChangeCommands),
	m_exit(false)
{
	camera.init(Coordinate2D(windowWidth / 2, windowHeight / 2), windowWidth, windowHeight, windowWidth, windowHeight);
	camera.setPosition(Coordinate2D());

	m_tileWidth = 25;
	m_tileHeight = 25;

	tiles = std::unique_ptr<GroundTilesPool>(new GroundTilesPool(1, windowWidth, windowHeight, m_tileWidth, m_tileHeight));

	animation = std::unique_ptr<GeneralAnimationPool>(new GeneralAnimationPool(1));

	State<3> states;
	states.framesCount = 3;
	states.frames[0].duration = 0.2;
	states.frames[0].spriteId = 1;
	states.frames[0].next = Transition::GO_NEXT;
	states.frames[1].duration = 0.2;
	states.frames[1].spriteId = 2;
	states.frames[1].next = Transition::GO_NEXT;
	states.frames[2].duration = 0.2;
	states.frames[2].spriteId = 3;
	states.frames[2].next = Transition::GO_NEXT;

	animation->createAnimation(1, &states);
}

LoadingInstance::~LoadingInstance()
{
}

bool LoadingInstance::Init()
{
	showCursor(false);

	tiles->generateTiles();

	sprites.clear();
	spriteSizes.clear();
	struct SpriteSize size;

	SpritesVector spritePathes = {
		BG_SPRITE,
		LOADING_SPRITE_1,
		LOADING_SPRITE_2,
		LOADING_SPRITE_3
	};

	for (size_t i = 0; i < spritePathes.size(); i++)
	{
		sprites.push_back(loadSprite(spritePathes.at(i).data(), size.width, size.height));

		spriteSizes.push_back(size);
	}

	TerrainVector terrainPathes = {
		TILE_SPRITE
	};

	terrains.clear();

	for (size_t i = 0; i < terrainPathes.size(); i++)
	{
		terrains.push_back(loadSprite(terrainPathes.at(i).data(), size.width, size.height));
	}

	return true;
}

void LoadingInstance::Close()
{
	for (size_t i = 0; i < sprites.size(); i++)
	{
		destroySprite(sprites.at(i));
	}

	for (size_t i = 0; i < terrains.size(); i++)
	{
		destroySprite(terrains.at(i));
	}

	sprites.clear();
	spriteSizes.clear();
	terrains.clear();
}

bool LoadingInstance::update(double dt)
{
	animation->update(dt);
	return m_exit;
}

void LoadingInstance::render()
{
	// Background
	TilesFieldSize fieldSize = tiles->tilesFieldSize();
	VisibleTiles vt = tiles->visibleTiles(camera.screenLeftTopCornerPosition(), camera.screenWidth(), camera.screenHeight());
	GroundTile tile;
	CoordinateScreen tilePos;
	Coordinate2D screenCenter = camera.screenCenter();

	for (uint32_t i = vt.left; i < vt.horizontalTilesCount; i++)
	{
		for (uint32_t j = vt.top; j < vt.verticalTilesCount; j++)
		{
			tile = tiles->tiles[j * fieldSize.horizontalCount + i];
			tilePos = camera.translateGlobalToScreen(Coordinate2D(
				static_cast<int32_t>(i*m_tileWidth) - static_cast<int32_t>(screenCenter.x),
				static_cast<int32_t>(j*m_tileHeight) - static_cast<int32_t>(screenCenter.y)
			));
			drawSprite(terrains[tile], tilePos.x, tilePos.y, m_tileWidth, m_tileHeight);
		}
	}

	drawSprite(sprites[0], camera.screenWidth() / 2 - spriteSizes[0].width / 2, camera.screenHeight() / 2 - spriteSizes[0].height / 2, spriteSizes[0].width, spriteSizes[0].height);

	for (uint32_t i = 0; i < animation->activeCount(); i++)
	{
		AnimationComponent<1, 3> &anim = animation->objects()[i];
		
		drawSprite(sprites[anim.m_currentSprite], camera.screenWidth() / 2 - spriteSizes[anim.m_currentSprite].width / 2, camera.screenHeight() / 2 - spriteSizes[anim.m_currentSprite].height / 2, spriteSizes[anim.m_currentSprite].width, spriteSizes[anim.m_currentSprite].height);
	}
}

void LoadingInstance::onMouseMove(int32_t x, int32_t y, int32_t xrelative, int32_t yrelative)
{
}

void LoadingInstance::onMouseButtonClick(uint8_t button, bool isReleased, uint8_t clicks, int32_t x, int32_t y)
{
}

void LoadingInstance::onKeyPressed(SDL_Keycode k)
{
}

void LoadingInstance::onKeyReleased(SDL_Keycode k)
{
}
