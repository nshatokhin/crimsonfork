#pragma once

#include "Engine/framework/Framework.h"

#include "commands/CommandReceiver.h"
#include "commands/GameStateCommands.h"

#include <cassert>

class IGameState
{
public:
	IGameState(CommandReceiver *stateChangeListener, GameStateCommands *stateChangeCommands) :
		m_stateChangeListener(stateChangeListener),
		m_stateChangeCommands(stateChangeCommands)
	{
		assert(m_stateChangeListener && m_stateChangeCommands);
	}

	virtual ~IGameState() {}

	// return : true - ok, false - failed, application will exit
	virtual bool Init() = 0;

	virtual void Close() = 0;

	// return value: if true will exit the application
	virtual bool update(double dt) = 0;
	virtual void render() = 0;

	virtual void onMouseMove(int32_t x, int32_t y, int32_t xrelative, int32_t yrelative) = 0;
	virtual void onMouseButtonClick(uint8_t button, bool isReleased, uint8_t clicks, int32_t x, int32_t y) = 0;
	virtual void onKeyPressed(SDL_Keycode k) = 0;
	virtual void onKeyReleased(SDL_Keycode k) = 0;

protected:
	CommandReceiver * m_stateChangeListener;
	GameStateCommands * m_stateChangeCommands;
};
