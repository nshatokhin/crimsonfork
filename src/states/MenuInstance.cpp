#include "MenuInstance.h"

#include "Engine/framework/Framework.h"

#include "services/ConfigLocator.h"
#include "services/SoundLocator.h"

MenuInstance::MenuInstance(int windowWidth, int windowHeight, CommandReceiver * stateChangeListener, GameStateCommands * stateChangeCommands) : IGameState(stateChangeListener, stateChangeCommands),
	m_exit(false)
{
	config = ConfigLocator::locate();

	sound = std::unique_ptr<SoundService>(new SoundService(config->menuSounds(), config->menuMusic()));
	SoundLocator::provide(sound.get());

	sound->setMusicVolume(config->musicVolume());
	sound->setSoundsVolume(config->soundsVolume());

	camera.init(Coordinate2D(windowWidth / 2, windowHeight / 2), windowWidth, windowHeight, windowWidth, windowHeight);
	camera.setPosition(Coordinate2D());

	m_tileWidth = config->tileWidth();
	m_tileHeight = config->tileHeight();

	tiles = std::unique_ptr<GroundTilesPool>(new GroundTilesPool(config->menuTilesCount(), windowWidth, windowHeight, m_tileWidth, m_tileHeight));
}

MenuInstance::~MenuInstance()
{
}

bool MenuInstance::Init()
{
	showCursor(false);

	tiles->generateTiles();

	m_menuBackgroundSprite = config->menuBackground();

	m_menuStartButtonStatesCount = config->menuStartButtonStates();
	m_menuStartButtonAnimation = config->menuStartButtonAnimation();
	m_menuStartButtonX = config->menuStartButtonX();
	m_menuStartButtonY = config->menuStartButtonY();
	m_menuStartButtonWidth = config->menuStartButtonWidth();
	m_menuStartButtonHeight = config->menuStartButtonHeight();

	m_menuExitButtonStatesCount = config->menuExitButtonStates();
	m_menuExitButtonAnimation = config->menuExitButtonAnimation();
	m_menuExitButtonX = config->menuExitButtonX();
	m_menuExitButtonY = config->menuExitButtonY();
	m_menuExitButtonWidth = config->menuExitButtonWidth();
	m_menuExitButtonHeight = config->menuExitButtonHeight();

	m_uiCursorArrowSprite = config->menuCursorArrowSpriteId();

	sprites.clear();
	spriteSizes.clear();
	struct SpriteSize size;

	SpritesVector spritePathes = config->menuSprites();

	for (size_t i = 0; i < spritePathes.size(); i++)
	{
		sprites.push_back(loadSprite(spritePathes.at(i).data(), size.width, size.height));

		spriteSizes.push_back(size);
	}

	TerrainVector terrainPathes = config->menuTerrains();
	terrains.clear();

	for (size_t i = 0; i < terrainPathes.size(); i++)
	{
		terrains.push_back(loadSprite(terrainPathes.at(i).data(), size.width, size.height));
	}

	m_menuPopup.init(m_menuBackgroundSprite, m_menuStartButtonStatesCount, m_menuStartButtonAnimation,
		Rectangle(Coordinate2D(m_menuStartButtonX, m_menuStartButtonY), m_menuStartButtonWidth, m_menuStartButtonHeight),
		m_menuExitButtonStatesCount, m_menuExitButtonAnimation, Rectangle(Coordinate2D(m_menuExitButtonX, m_menuExitButtonY), m_menuExitButtonWidth, m_menuExitButtonHeight),
		this, &MenuInstance::menuStartButtonHandler, &MenuInstance::menuExitButtonHandler);

	sound->playMusic(config->menuMusicTheme(), true);

	return true;
}

void MenuInstance::Close()
{
	for (size_t i = 0; i < sprites.size(); i++)
	{
		destroySprite(sprites.at(i));
	}

	for (size_t i = 0; i < terrains.size(); i++)
	{
		destroySprite(terrains.at(i));
	}

	sprites.clear();
	spriteSizes.clear();
	terrains.clear();
}

bool MenuInstance::update(double dt)
{
	m_menuPopup.update(dt);

	return m_exit;
}

void MenuInstance::render()
{
	// Background
	TilesFieldSize fieldSize = tiles->tilesFieldSize();
	VisibleTiles vt = tiles->visibleTiles(camera.screenLeftTopCornerPosition(), camera.screenWidth(), camera.screenHeight());
	GroundTile tile;
	CoordinateScreen tilePos;
	Coordinate2D screenCenter = camera.screenCenter();

	for (uint32_t i = vt.left; i < vt.horizontalTilesCount; i++)
	{
		for (uint32_t j = vt.top; j < vt.verticalTilesCount; j++)
		{
			tile = tiles->tiles[j * fieldSize.horizontalCount + i];
			tilePos = camera.translateGlobalToScreen(Coordinate2D(
				static_cast<int32_t>(i*m_tileWidth) - static_cast<int32_t>(screenCenter.x),
				static_cast<int32_t>(j*m_tileHeight) - static_cast<int32_t>(screenCenter.y)
			));
			drawSprite(terrains[tile], tilePos.x, tilePos.y, m_tileWidth, m_tileHeight);
		}
	}

	m_menuPopup.render(camera, sprites, spriteSizes);

	// Mouse pointer
	SpriteSize size = spriteSizes[m_uiCursorArrowSprite];
	drawSprite(sprites[m_uiCursorArrowSprite], m_mouseLastPos.x - size.width / 2, m_mouseLastPos.y - size.height / 2, size.width, size.height);
}

void MenuInstance::onMouseMove(int32_t x, int32_t y, int32_t xrelative, int32_t yrelative)
{
	m_mouseLastPos = CoordinateScreen(x, y);

	m_menuPopup.onMouseMove(x, y);
}

void MenuInstance::onMouseButtonClick(uint8_t button, bool isReleased, uint8_t clicks, int32_t x, int32_t y)
{
	if (button == 1)
	{
		m_menuPopup.onMouseButtonClick(m_mouseLastPos.x, m_mouseLastPos.y, isReleased);
	}
}

void MenuInstance::onKeyPressed(SDL_Keycode k)
{
}

void MenuInstance::onKeyReleased(SDL_Keycode k)
{
}

void MenuInstance::menuStartButtonHandler()
{
	m_stateChangeListener->processCommand(&m_stateChangeCommands->startGameStateCommand);
}

void MenuInstance::menuExitButtonHandler()
{
	m_exit = true;
}
