#pragma once

#include "entities/CameraEntity.h"
#include "pools/GeneralAnimationPool.h"
#include "pools/GroundTilesPool.h"
#include "primitives/Coordinate2D.h"
#include "services/config/ConfigService.h"
#include "services/sound/SoundService.h"
#include "states/IGameState.h"
#include "ui/Popup.h"

#include <memory>

class LoadingInstance : public IGameState
{
public:
	LoadingInstance(int windowWidth, int windowHeight, CommandReceiver *stateChangeListener, GameStateCommands *stateChangeCommands);
	virtual ~LoadingInstance();

	// return : true - ok, false - failed, application will exit
	virtual bool Init() override;

	virtual void Close() override;

	// return value: if true will exit the application
	virtual bool update(double dt) override;
	virtual void render() override;

	virtual void onMouseMove(int32_t x, int32_t y, int32_t xrelative, int32_t yrelative) override;
	virtual void onMouseButtonClick(uint8_t button, bool isReleased, uint8_t clicks, int32_t x, int32_t y) override;
	virtual void onKeyPressed(SDL_Keycode k) override;
	virtual void onKeyReleased(SDL_Keycode k) override;

protected:
	bool m_exit;

	CameraEntity camera;

	std::vector<Sprite *> sprites;
	std::vector<struct SpriteSize> spriteSizes;
	std::vector<Sprite *> terrains;

	uint32_t m_tileWidth, m_tileHeight;
	std::unique_ptr<GroundTilesPool> tiles;

	std::unique_ptr<GeneralAnimationPool> animation;
};
