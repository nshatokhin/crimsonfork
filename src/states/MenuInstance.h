#pragma once

#include "entities/CameraEntity.h"
#include "pools/GroundTilesPool.h"
#include "primitives/Coordinate2D.h"
#include "services/config/ConfigService.h"
#include "services/sound/SoundService.h"
#include "states/IGameState.h"
#include "ui/Popup.h"

#include <memory>

class MenuInstance : public IGameState
{
public:
	MenuInstance(int windowWidth, int windowHeight, CommandReceiver *stateChangeListener, GameStateCommands *stateChangeCommands);
	virtual ~MenuInstance();

	// return : true - ok, false - failed, application will exit
	virtual bool Init() override;

	virtual void Close() override;

	// return value: if true will exit the application
	virtual bool update(double dt) override;
	virtual void render() override;

	virtual void onMouseMove(int32_t x, int32_t y, int32_t xrelative, int32_t yrelative) override;
	virtual void onMouseButtonClick(uint8_t button, bool isReleased, uint8_t clicks, int32_t x, int32_t y) override;
	virtual void onKeyPressed(SDL_Keycode k) override;
	virtual void onKeyReleased(SDL_Keycode k) override;

	void menuStartButtonHandler();
	void menuExitButtonHandler();

protected:
	bool m_exit;
	CoordinateScreen m_mouseLastPos;

	CameraEntity camera;
	ConfigService * config;

	std::unique_ptr<SoundService> sound;

	std::vector<Sprite *> sprites;
	std::vector<struct SpriteSize> spriteSizes;
	std::vector<Sprite *> terrains;

	uint32_t m_tileWidth, m_tileHeight;
	std::unique_ptr<GroundTilesPool> tiles;

	uint32_t m_uiCursorArrowSprite;
	uint32_t m_menuBackgroundSprite;

	uint32_t m_menuStartButtonStatesCount;
	ButtonAnimation * m_menuStartButtonAnimation;
	int32_t m_menuStartButtonX, m_menuStartButtonY;
	uint32_t m_menuStartButtonWidth, m_menuStartButtonHeight;

	uint32_t m_menuExitButtonStatesCount;
	ButtonAnimation * m_menuExitButtonAnimation;
	int32_t m_menuExitButtonX, m_menuExitButtonY;
	uint32_t m_menuExitButtonWidth, m_menuExitButtonHeight;

	typedef void(MenuInstance::*PopupActionHandler)();
	Popup<MenuInstance, PopupActionHandler> m_menuPopup;

};
