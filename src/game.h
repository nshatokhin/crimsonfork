#pragma once

#include "Engine/framework/Framework.h"

#include "commands/CommandReceiver.h"
#include "commands/GameStateCommands.h"
#include "services/ConfigLocator.h"
#include "services/LoggerLocator.h"
#include "states/IGameState.h"
#include "time/Time.h"

#include <string>

enum class GAME_STATES
{
	LOADING_STATE = 0,
	MENU_STATE,
	GAME_STATE
};

class MyFramework : public Framework, public CommandReceiver
{

public:

	MyFramework(const std::string windowTitle, const std::string &configPath, uint32_t maxThreadCount, int windowWidth, int windowHeight, bool fullscreen, int mapWidth, int mapHeight, int enemiesCount, int enemiesOnScreen, int ammoCount, bool autoTest);
	virtual void PreInit(std::string &windowTitle, uint32_t &screenWidth, uint32_t &screenHeight, bool &fullscreen);

	virtual bool Init();

	virtual void Close();

	virtual bool update(double dt) override;

	virtual void render() override;

	virtual void onMouseMove(int32_t x, int32_t y, int32_t xrelative, int32_t yrelative);

	virtual void onMouseButtonClick(uint8_t button, bool isReleased, uint8_t clicks, int32_t x, int32_t y);

	virtual void onKeyPressed(SDL_Keycode k);

	virtual void onKeyReleased(SDL_Keycode k);

	void gameStart();
	void gameRestart();
	void showMenu();

public:
	GameStateCommands commands;

protected:
	void loadConfig();
	void setInitialState();
	void goToState(GAME_STATES state);

	void processGameStart();
	void processGameRestart();
	void processShowMenu();

protected:
	static constexpr double TIME_PER_UPDATE = 1.0 / 60; // 60 FPS
														// TODO: update 2 times faster than render
protected:
	GAME_STATES m_currentStateId;

	bool m_exit;

	std::unique_ptr<ConfigService> m_config;
	std::atomic<bool> m_configReady;
	LoggerService * m_logger;

	IGameState * m_currentState;

	std::string m_configPath;

	int m_windowWidth, m_windowHeight;
	int m_mapHeight, m_mapWidth;
	int m_maxEnemies, m_maxEnemiesSpawn;
	int m_maxAmmo;

	timestamp_t m_previousTimestamp;
	double m_lag;

	bool m_startGame, m_restartGame, m_showMenu;
	bool m_autoTest;
};
