#pragma once

class CommandReceiver;

class ICommand
{
public:
	ICommand() {}
	virtual ~ICommand() {}

	virtual void execute(CommandReceiver *receiver) = 0;
};
