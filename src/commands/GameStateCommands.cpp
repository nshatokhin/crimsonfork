#include "GameStateCommands.h"

#include "game.h"

void CommandGameRestart::execute(CommandReceiver * receiver)
{
	MyFramework * stateController = static_cast<MyFramework *>(receiver);

	stateController->gameRestart();
}

void CommandGameStart::execute(CommandReceiver * receiver)
{
	MyFramework * stateController = static_cast<MyFramework *>(receiver);

	stateController->gameStart();
}

void CommandMenuShow::execute(CommandReceiver * receiver)
{
	MyFramework * stateController = static_cast<MyFramework *>(receiver);

	stateController->showMenu();
}
