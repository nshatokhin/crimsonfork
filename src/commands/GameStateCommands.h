#pragma once

#include "commands/ICommand.h"

class CommandGameStart : public ICommand
{
public:
	CommandGameStart() : ICommand() {}

	void execute(CommandReceiver *receiver) override;
};

class CommandGameRestart : public ICommand
{
public:
	CommandGameRestart() : ICommand() {}

	void execute(CommandReceiver *receiver) override;
};

class CommandMenuShow : public ICommand
{
public:
	CommandMenuShow() : ICommand() {}

	void execute(CommandReceiver *receiver) override;
};

class GameStateCommands
{
public:
	CommandGameStart startGameStateCommand;
	CommandGameRestart restartStateCommand;
	CommandMenuShow menuStateCommand;
};