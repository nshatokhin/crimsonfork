#pragma once

class ICommand;

class CommandReceiver
{
public:
	CommandReceiver() {}

	void processCommand(ICommand *command);
};
