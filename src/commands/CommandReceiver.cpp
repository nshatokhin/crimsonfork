#include "CommandReceiver.h"

#include "ICommand.h"

void CommandReceiver::processCommand(ICommand *command)
{
	command->execute(this);
}
