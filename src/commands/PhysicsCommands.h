#pragma once

#include "commands/ICommand.h"

#include <cstdint>
#include <mutex>

class CommandCollision : public ICommand
{
public:
	uint32_t physicsComponent1;
	uint32_t physicsComponent2;

	std::mutex execMutex;

public:
	CommandCollision() : ICommand() {}

	void execute(CommandReceiver *receiver) override;
};

class CommandOutOfWorld : public ICommand
{
public:
	uint32_t physicsComponent;

	std::mutex execMutex;

public:
	CommandOutOfWorld() : ICommand() {}

	void execute(CommandReceiver *receiver) override;
};
