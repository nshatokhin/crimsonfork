#pragma once

#include "commands/ICommand.h"
#include "primitives/Coordinate2D.h"

class CommandUpPress : public ICommand
{
public:
	CommandUpPress() : ICommand(), isActive(false) {}

	void execute(CommandReceiver *receiver) override;

	bool isActive;
};

class CommandUpRelease : public ICommand
{
public:
	CommandUpRelease() : ICommand() {}

	void execute(CommandReceiver *receiver) override;
};

class CommandDownPress : public ICommand
{
public:
	CommandDownPress() : ICommand(), isActive(false) {}

	void execute(CommandReceiver *receiver) override;

	bool isActive;
};

class CommandDownRelease : public ICommand
{
public:
	CommandDownRelease() : ICommand() {}

	void execute(CommandReceiver *receiver) override;
};

class CommandLeftPress : public ICommand
{
public:
	CommandLeftPress() : ICommand(), isActive(false) {}

	void execute(CommandReceiver *receiver) override;

	bool isActive;
};

class CommandLeftRelease : public ICommand
{
public:
	CommandLeftRelease() : ICommand() {}

	void execute(CommandReceiver *receiver) override;
};

class CommandRightPress : public ICommand
{
public:
	CommandRightPress() : ICommand(), isActive(false) {}

	void execute(CommandReceiver *receiver) override;

	bool isActive;
};

class CommandRightRelease : public ICommand
{
public:
	CommandRightRelease() : ICommand() {}

	void execute(CommandReceiver *receiver) override;
};

class CommandAimTo : public ICommand
{
public:
	Coordinate2D target;

public:
	CommandAimTo() : ICommand() {}

	void execute(CommandReceiver *receiver) override;
};

class PlayerCommands
{
public:
	CommandUpPress upPress;
	CommandUpRelease upRelease;
	CommandDownPress downPress;
	CommandDownRelease downRelease;
	CommandLeftPress leftPress;
	CommandLeftRelease leftRelease;
	CommandRightPress rightPress;
	CommandRightRelease rightRelease;
	CommandAimTo aimTo;
};
