#include "PhysicsCommands.h"

#include "states/GameInstance.h"

void CommandCollision::execute(CommandReceiver * receiver)
{
	GameInstance * gi = static_cast<GameInstance *>(receiver);

	std::unique_lock<std::mutex> lock(execMutex);

	gi->collisionHandler(physicsComponent1, physicsComponent2);
}

void CommandOutOfWorld::execute(CommandReceiver * receiver)
{
	GameInstance * gi = static_cast<GameInstance *>(receiver);

	std::unique_lock<std::mutex> lock(execMutex);

	gi->outOfWorldHandler(physicsComponent);
}
