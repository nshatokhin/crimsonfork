#include "PlayerCommands.h"

#include "entities/PlayerEntity.h"
#include "services/PhysicsPoolLocator.h"

void CommandUpPress::execute(CommandReceiver *receiver)
{
	PlayerEntity *player = static_cast<PlayerEntity *>(receiver);

	player->verticalAxis--;
	player->updateOrientation();

	isActive = true;
}

void CommandUpRelease::execute(CommandReceiver *receiver)
{
	PlayerEntity *player = static_cast<PlayerEntity *>(receiver);

	if (!player->commands.upPress.isActive) return;

	player->verticalAxis++;
	player->updateOrientation();

	player->commands.upPress.isActive = false;
}

void CommandDownPress::execute(CommandReceiver *receiver)
{
	PlayerEntity *player = static_cast<PlayerEntity *>(receiver);

	player->verticalAxis++;
	player->updateOrientation();

	isActive = true;
}

void CommandDownRelease::execute(CommandReceiver *receiver)
{
	PlayerEntity *player = static_cast<PlayerEntity *>(receiver);

	if (!player->commands.downPress.isActive) return;

	player->verticalAxis--;
	player->updateOrientation();

	player->commands.downPress.isActive = false;
}

void CommandLeftPress::execute(CommandReceiver *receiver)
{
	PlayerEntity *player = static_cast<PlayerEntity *>(receiver);

	player->horizontalAxis--;
	player->updateOrientation();

	isActive = true;
}

void CommandLeftRelease::execute(CommandReceiver *receiver)
{
	PlayerEntity *player = static_cast<PlayerEntity *>(receiver);

	if (!player->commands.leftPress.isActive) return;

	player->horizontalAxis++;
	player->updateOrientation();

	player->commands.leftPress.isActive = false;
}

void CommandRightPress::execute(CommandReceiver *receiver)
{
	PlayerEntity *player = static_cast<PlayerEntity *>(receiver);

	player->horizontalAxis++;
	player->updateOrientation();

	isActive = true;
}

void CommandRightRelease::execute(CommandReceiver *receiver)
{
	PlayerEntity *player = static_cast<PlayerEntity *>(receiver);

	if (!player->commands.rightPress.isActive) return;

	player->horizontalAxis--;
	player->updateOrientation();

	player->commands.rightPress.isActive = false;
}

void CommandAimTo::execute(CommandReceiver * receiver)
{
	PlayerEntity *player = static_cast<PlayerEntity *>(receiver);

	PhysicsPool *pp = PhysicsPoolLocator::locate();
	
	Coordinate2D playerPos = pp->getObject(player->physicsComponent)->position();
	player->weapon.aim(playerPos, target);
}
