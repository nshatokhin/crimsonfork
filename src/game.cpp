#include <iostream>
#include <memory>
#include <string>

#include "game.h"

#include "helpers/InputParser.h"
#include "helpers/RandomGenerator.h"
#include "helpers/StringParse.h"
#include "services/ConfigLocator.h"
#include "states/LoadingInstance.h"
#include "states/MenuInstance.h"
#include "states/GameInstance.h"

MyFramework::MyFramework(const std::string windowTitle, const std::string &configPath, uint32_t maxThreadCount, int windowWidth, int windowHeight, bool fullscreen, int mapWidth, int mapHeight, int enemiesCount, int enemiesOnScreen, int ammoCount, bool autoTest) : Framework(windowTitle, maxThreadCount, windowWidth, windowHeight, fullscreen),
	m_exit(false),
	m_configReady(false),
	m_currentState(nullptr),
	m_configPath(configPath),
	m_windowWidth(windowWidth), m_windowHeight(windowHeight),
	m_mapWidth(mapWidth), m_mapHeight(mapHeight),
	m_maxEnemies(enemiesCount), m_maxEnemiesSpawn(enemiesOnScreen),
	m_maxAmmo(ammoCount),
	m_lag(0),
	m_startGame(false),
	m_restartGame(false),
	m_showMenu(false),
	m_autoTest(autoTest)
{
	RandomGenerator::init();

	m_logger = LoggerLocator::locate();

	m_threadPool->enqueue(std::bind(&MyFramework::loadConfig, this));
}

void MyFramework::PreInit(std::string &windowTitle, uint32_t &screenWidth, uint32_t &screenHeight, bool &fullscreen)
{
	windowTitle = m_windowTitle;
	screenWidth = m_windowWidth;
	screenHeight = m_windowHeight;
	fullscreen = false;
}

bool MyFramework::Init()
{
	if (m_exit) return false;

	goToState(GAME_STATES::LOADING_STATE);

	m_previousTimestamp = Time::getTimestamp();
	m_lag = 0;

	return !m_exit;
}

void MyFramework::Close()
{
	if (m_currentState != nullptr)
	{
		m_currentState->Close();
		delete m_currentState;
	}
}

bool MyFramework::update(double dt)
{
	if (m_configReady && m_currentStateId == GAME_STATES::LOADING_STATE)
	{
		setInitialState();
		return m_exit;
	}
	
	if (m_startGame)
	{
		processGameStart();
		return m_exit;
	}
	if (m_restartGame)
	{
		processGameRestart();
		return m_exit;
	}
	if (m_showMenu)
	{
		processShowMenu();
		return m_exit;
	}

	return m_exit || m_currentState->update(dt);
}

void MyFramework::render()
{
	m_currentState->render();
}

void MyFramework::onMouseMove(int32_t x, int32_t y, int32_t xrelative, int32_t yrelative) {
	m_currentState->onMouseMove(x, y, xrelative, yrelative);
}

void MyFramework::onMouseButtonClick(uint8_t button, bool isReleased, uint8_t clicks, int32_t x, int32_t y) {
	m_currentState->onMouseButtonClick(button, isReleased, clicks, x, y);
}

void MyFramework::onKeyPressed(SDL_Keycode k) {
	m_currentState->onKeyPressed(k);
}

void MyFramework::onKeyReleased(SDL_Keycode k) {
	m_currentState->onKeyReleased(k);
}

void MyFramework::gameStart()
{
	m_startGame = true;
}

void MyFramework::gameRestart()
{
	m_restartGame = true;
}

void MyFramework::showMenu()
{
	m_showMenu = true;
}

void MyFramework::loadConfig()
{
	m_config = std::unique_ptr<ConfigService>(new ConfigService(m_configPath));

	if (!m_config->isInitialized())
	{
		m_logger->logFatal("Config loading failed. Exiting...");
		m_exit = true;
	}

	ConfigLocator::provide(m_config.get());

	m_configReady = true;
}

void MyFramework::setInitialState()
{
	if (m_autoTest)
	{
		goToState(GAME_STATES::GAME_STATE);
	}
	else
	{
		goToState(GAME_STATES::MENU_STATE);
	}
}

void MyFramework::goToState(GAME_STATES state)
{
	if (!m_configReady && state != GAME_STATES::LOADING_STATE)
	{
		return;
	}

	if (m_currentState != nullptr)
	{
		m_currentState->Close();
		delete m_currentState;
		m_currentState = nullptr;
	}

	m_currentStateId = state;

	if (m_currentStateId == GAME_STATES::LOADING_STATE)
	{
		m_currentState = new LoadingInstance(m_windowWidth, m_windowHeight, this, &commands);
	}
	else if (m_currentStateId == GAME_STATES::GAME_STATE)
	{
		// TODO: load from config
		m_currentState = new GameInstance(m_windowWidth, m_windowHeight, m_mapWidth, m_mapHeight, m_maxEnemies, m_maxEnemiesSpawn, m_maxAmmo, 10, m_autoTest, this, &commands);
	}
	else
	{
		m_currentState = new MenuInstance(m_windowWidth, m_windowHeight, this, &commands);
	}
	
	m_exit = !m_currentState->Init();
}

void MyFramework::processGameStart()
{
	m_startGame = false;
	goToState(GAME_STATES::GAME_STATE);
}

void MyFramework::processGameRestart()
{
	m_restartGame = false;
	goToState(m_currentStateId);
}

void MyFramework::processShowMenu()
{
	m_showMenu = false;
	goToState(GAME_STATES::MENU_STATE);
}

#undef main

int main(int argc, char *argv[])
{
	std::string configPath = "config.json";
	std::string logPath = "log.txt";
	int threadsCount = 4;
	int windowWidth = 1250;//640;
	int windowHeight = 700;// 480;
	int mapWidth = 1250;
	int mapHeight = 700;
	int enemiesCount = 1000;
	int maxEnemiesSpawn = 100;
	int ammoCount = 10;
	bool autoTest = false;
	bool fullscreen = false;

	InputParser input(argc, argv);

	const std::string &configStr = input.getCmdOption("-config");

	if (!configStr.empty())
	{
		configPath = configStr;
	}

	const std::string &threadsStr = input.getCmdOption("-threads");

	if (!threadsStr.empty())
	{
		int threads = stoi(threadsStr);

		if (threads > 0)
		{
			threadsCount = threads;
		}
	}

	const std::string &logStr = input.getCmdOption("-log");

	if (!logStr.empty())
	{
		logPath = logStr;
	}

	std::unique_ptr<LoggerService> m_logger = std::unique_ptr<LoggerService>(new LoggerService(logPath));
	LoggerLocator::provide(m_logger.get());

	const std::string &windowStr = input.getCmdOption("-window");
	
	if (!windowStr.empty()) {
		getIntFromStr(windowWidth, windowHeight, windowStr);
	}

	const std::string &mapStr = input.getCmdOption("-map");

	if (!mapStr.empty()) {
		getIntFromStr(mapWidth, mapHeight, mapStr);
	}

	const std::string &enemiesStr = input.getCmdOption("-num_enemies");

	if (!enemiesStr.empty()) {
		enemiesCount = stoi(enemiesStr);
	}

	const std::string &spawnStr = input.getCmdOption("-enemies_spawn");

	if (!spawnStr.empty()) {
		maxEnemiesSpawn = stoi(spawnStr);
	}

	if (input.cmdOptionExists("-spawn_all"))
	{
		maxEnemiesSpawn = enemiesCount;
	}

	const std::string &ammoStr = input.getCmdOption("-num_ammo");

	if (!ammoStr.empty()) {
		ammoCount = stoi(ammoStr);
	}

	if (input.cmdOptionExists("-auto_test"))
	{
		autoTest = true;
	}

	if (input.cmdOptionExists("-fullscreen"))
	{
		fullscreen = true;
	}

	return run(new MyFramework("UFO Attack", configPath, threadsCount, windowWidth, windowHeight, fullscreen, mapWidth, mapHeight, enemiesCount, maxEnemiesSpawn, ammoCount, autoTest));
}
