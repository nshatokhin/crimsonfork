#include "OccupationPartition.h"
#include <iostream>
OccupationPartition::OccupationPartition(uint32_t maxCapacity, uint32_t cellWidth, uint32_t cellHeight, uint32_t worldWidth, uint32_t worldHeight) :
	m_cellWidth(cellWidth), m_cellHeight(cellHeight),
	m_worldCenter(worldWidth / 2, worldHeight / 2),
	m_worldWidth(worldWidth), m_worldHeight(worldHeight),
	m_cellsHorCount(m_worldWidth / m_cellWidth),
	m_cellsVertCount(m_worldHeight / m_cellHeight),
	m_grid(m_cellsHorCount * m_cellsVertCount, -1)
{
}

OccupationPartition::~OccupationPartition()
{
}

void OccupationPartition::putToGrid(const Coordinate2D & position, uint32_t object)
{
	uint32_t cellX, cellY;
	calculateGridCoordinates(cellX, cellY, position);
	pushObjectToCell(cellX, cellY, object);
}

void OccupationPartition::removeFromGrid(const Coordinate2D & position, uint32_t object)
{
	uint32_t cellX, cellY;
	calculateGridCoordinates(cellX, cellY, position);
	removeObjectFromCell(cellX, cellY, object);
}

void OccupationPartition::move(const Coordinate2D & oldPosition, const Coordinate2D & newPosition, uint32_t object)
{
	uint32_t oldCellX, oldCellY, newCellX, newCellY;
	calculateGridCoordinates(oldCellX, oldCellY, oldPosition);
	calculateGridCoordinates(newCellX, newCellY, newPosition);

	if (oldCellX != newCellX || oldCellY != newCellY)
	{
		std::unique_lock<std::mutex> lock(moveMutex);

		removeObjectFromCell(oldCellX, oldCellY, object);
		pushObjectToCell(newCellX, newCellY, object);
	}
}

const std::vector<int64_t>& OccupationPartition::grid() const
{
	return m_grid;
}

uint32_t OccupationPartition::cellsHorCount() const
{
	return m_cellsHorCount;
}

uint32_t OccupationPartition::cellsVertCount() const
{
	return m_cellsVertCount;
}

bool OccupationPartition::isOccupied(uint32_t cellX, uint32_t cellY) const
{
	uint64_t index = calculateGridIndex(cellX, cellY);

	return m_grid[index] != -1;
}

bool OccupationPartition::isOccupied(uint32_t index) const
{
	return m_grid[index] != -1;
}

bool OccupationPartition::isOccupied(const Coordinate2D & position) const
{
	uint32_t cellX, cellY;
	calculateGridCoordinates(cellX, cellY, position);
	return m_grid[calculateGridIndex(cellX, cellY)] != -1;
}

bool OccupationPartition::isOccupiedByObject(const Coordinate2D &position, uint32_t object) const
{
	int64_t occupant = cellOccupant(position);
	return occupant != -1 && occupant == object;
}

bool OccupationPartition::isOccupiedNotByObject(const Coordinate2D &position, uint32_t object) const
{
	int64_t occupant = cellOccupant(position);
	return occupant != -1 && occupant != object;
}

int64_t OccupationPartition::cellOccupant(const Coordinate2D & position) const
{
	uint32_t cellX, cellY;
	calculateGridCoordinates(cellX, cellY, position);
	return m_grid[calculateGridIndex(cellX, cellY)];
}

uint64_t OccupationPartition::gridIndex(const Coordinate2D & position) const
{
	uint32_t cellX, cellY;
	calculateGridCoordinates(cellX, cellY, position);
	return calculateGridIndex(cellX, cellY);
}

inline void OccupationPartition::calculateGridCoordinates(uint32_t & cellX, uint32_t & cellY, const Coordinate2D & position) const
{
	cellX = static_cast<uint32_t>((position.x + m_worldCenter.x) / m_cellWidth);
	cellY = static_cast<uint32_t>((position.y + m_worldCenter.y) / m_cellHeight);
}

inline uint64_t OccupationPartition::calculateGridIndex(uint32_t cellX, uint32_t cellY) const
{
	return cellY * m_cellsHorCount + cellX;
}

inline void OccupationPartition::pushObjectToCell(uint32_t cellX, uint32_t cellY, uint32_t object)
{
	uint64_t index = calculateGridIndex(cellX, cellY);

	m_grid[index] = object;
}

inline void OccupationPartition::removeObjectFromCell(uint32_t cellX, uint32_t cellY, uint32_t object)
{
	uint64_t index = calculateGridIndex(cellX, cellY);
	
	m_grid[index] = -1;
}
