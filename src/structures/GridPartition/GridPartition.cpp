#include "GridPartition.h"

GridPartition::GridPartition(uint32_t maxCapacity, uint32_t cellWidth, uint32_t cellHeight, uint32_t worldWidth, uint32_t worldHeight) :
	m_cellWidth(cellWidth), m_cellHeight(cellHeight),
	m_worldCenter(worldWidth / 2, worldHeight / 2),
	m_worldWidth(worldWidth), m_worldHeight(worldHeight),
	m_cellsHorCount(m_worldWidth / m_cellWidth + 1),
	m_cellsVertCount(m_worldHeight / m_cellHeight + 1),
	pool(maxCapacity)
{
	m_grid = new int64_t[m_cellsHorCount * m_cellsVertCount];
	for (uint64_t i = 0; i < m_cellsHorCount * m_cellsVertCount; i++)
	{
		m_grid[i] = -1;
	}
}

GridPartition::~GridPartition()
{
	delete[] m_grid;
}

void GridPartition::putToGrid(const Coordinate2D & position, uint32_t object)
{
	uint32_t cellX, cellY;
	calculateGridCoordinates(cellX, cellY, position);
	pushObjectToCell(cellX, cellY, object);
}

void GridPartition::removeFromGrid(const Coordinate2D & position, uint32_t object)
{
	uint32_t cellX, cellY;
	calculateGridCoordinates(cellX, cellY, position);
	removeObjectFromCell(cellX, cellY, object);
}

void GridPartition::move(const Coordinate2D & oldPosition, const Coordinate2D & newPosition, uint32_t object)
{
	uint32_t oldCellX, oldCellY, newCellX, newCellY;
	calculateGridCoordinates(oldCellX, oldCellY, oldPosition);
	calculateGridCoordinates(newCellX, newCellY, newPosition);

	if (oldCellX != newCellX || oldCellY != newCellY)
	{
		std::unique_lock<std::mutex> lock(moveMutex);

		removeObjectFromCell(oldCellX, oldCellY, object);
		pushObjectToCell(newCellX, newCellY, object);
	}
}

int64_t * GridPartition::grid()
{
	return m_grid;
}

IndexedListNode * GridPartition::gridObjectsChain(uint32_t x, uint32_t y)
{
	uint64_t index = calculateGridIndex(x, y);

	if (m_grid[index] >= 0)
	{
		return pool.getObject(static_cast<uint32_t>(m_grid[index]));
	}
	else
	{
		return nullptr;
	}
}

IndexedListNode * GridPartition::nextObject(IndexedListNode & node)
{
	if (node.next >= 0)
	{
		return pool.getObject(static_cast<uint32_t>(node.next));
	}
	else
	{
		return nullptr;
	}
}

IndexedListNode * GridPartition::objectsInRegion(IndexedListNodePool &chainPool, Rectangle region)
{
	if (region.ltCorner.x < -static_cast<coordinate_t>(m_worldWidth / 2))
		region.ltCorner.x = - static_cast<coordinate_t>(m_worldWidth / 2);
	if (region.ltCorner.y < -static_cast<coordinate_t>(m_worldHeight / 2))
		region.ltCorner.y = -static_cast<coordinate_t>(m_worldHeight / 2);

	if (region.ltCorner.x + region.width > m_worldWidth / 2)
	{
		region.width -= region.ltCorner.x + region.width - m_worldWidth / 2;
	}
	if (region.ltCorner.y + region.height > m_worldHeight / 2)
	{
		region.height -= region.ltCorner.y + region.height - m_worldHeight / 2;
	}

	uint32_t ltCellX, ltCellY, rbCellX, rbCellY;
	calculateGridCoordinates(ltCellX, ltCellY, region.ltCorner);
	calculateGridCoordinates(rbCellX, rbCellY, region.ltCorner + Coordinate2D(region.width, region.height));

	/*// Take neighbour cells
	ltCellX -= static_cast<uint32_t>(ltCellX > 0);
	ltCellY -= static_cast<uint32_t>(ltCellY > 0);
	rbCellX += static_cast<uint32_t>(rbCellX < m_cellsHorCount - 1);
	rbCellY += static_cast<uint32_t>(rbCellY < m_cellsVertCount - 1);*/


	uint64_t index;
	int64_t nodeIndex, prevChainIndex = -1, currChainIndex;
	IndexedListNode *node, *chainNode = nullptr;
	for (uint32_t x = ltCellX; x <= rbCellX; x++)
	{
		for (uint32_t y = ltCellY; y <= rbCellY; y++)
		{
			index = calculateGridIndex(x, y);
			
			if (m_grid[index] >= 0)
			{
				nodeIndex = m_grid[index];

				while (nodeIndex >= 0)
				{
					node = pool.getObject(static_cast<uint32_t>(nodeIndex));

					currChainIndex = chainPool.createObject();
					chainNode = chainPool.getObject(currChainIndex);

					chainNode->value = node->value;
					chainNode->prev = -1;
					chainNode->next = prevChainIndex;

					if (prevChainIndex >= 0)
						chainPool.getObject(prevChainIndex)->prev = currChainIndex;

					prevChainIndex = currChainIndex;

					nodeIndex = node->next;
				}
			}
		}
	}

	return chainNode;
}

uint32_t GridPartition::cellsHorCount() const
{
	return m_cellsHorCount;
}

uint32_t GridPartition::cellsVertCount() const
{
	return m_cellsVertCount;
}

inline void GridPartition::calculateGridCoordinates(uint32_t & cellX, uint32_t & cellY, const Coordinate2D & position)
{
	cellX = static_cast<uint32_t>((position.x + m_worldCenter.x) / m_cellWidth);
	cellY = static_cast<uint32_t>((position.y + m_worldCenter.y) / m_cellHeight);
}

inline uint64_t GridPartition::calculateGridIndex(uint32_t cellX, uint32_t cellY) const
{
	return cellY * m_cellsHorCount + cellX;
}

inline void GridPartition::pushObjectToCell(uint32_t cellX, uint32_t cellY, uint32_t object)
{
	uint64_t index = calculateGridIndex(cellX, cellY);

	uint32_t nodeId = pool.createObject();
	IndexedListNode &node = *pool.getObject(nodeId);

	node.value = object;
	node.next = m_grid[index];
	node.prev = -1;
	
	if (m_grid[index] >= 0)
		pool.getObject(static_cast<uint32_t>(m_grid[index]))->prev = nodeId;

	m_grid[index] = nodeId;
}

inline void GridPartition::removeObjectFromCell(uint32_t cellX, uint32_t cellY, uint32_t object)
{
	uint64_t index = calculateGridIndex(cellX, cellY);
	int64_t nodeId = m_grid[index];

	while (nodeId >= 0)
	{
		IndexedListNode &node = *pool.getObject(static_cast<uint32_t>(nodeId));

		if (node.value == object)
		{
			if (nodeId == m_grid[index])
			{
				m_grid[index] = node.next;

				if (node.next >= 0)
					pool.getObject(static_cast<uint32_t>(node.next))->prev = -1;
			}
			else
			{
				if (node.prev >= 0)
					pool.getObject(static_cast<uint32_t>(node.prev))->next = node.next;

				if (node.next >= 0)
					pool.getObject(static_cast<uint32_t>(node.next))->prev = node.prev;
			}
			
			node.next = -1;
			node.prev = -1;

			pool.destroyObject(static_cast<uint32_t>(nodeId));
			break;
		}

		nodeId = node.next;
	}
}
