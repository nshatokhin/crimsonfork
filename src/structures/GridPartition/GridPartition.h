#pragma once

#include "primitives/Coordinate2D.h"
#include "primitives/Rectangle.h"
#include "pools/IndexedListNode.h"
#include "pools/IndexedListNodePool.h"

#include <mutex>

class GridPartition
{
public:
	GridPartition(uint32_t maxCapacity, uint32_t cellWidth, uint32_t cellHeight, uint32_t worldWidth, uint32_t worldHeight);
	~GridPartition();

	void putToGrid(const Coordinate2D &position, uint32_t object);
	void removeFromGrid(const Coordinate2D &position, uint32_t object);
	void move(const Coordinate2D &oldPosition, const Coordinate2D &newPosition, uint32_t object);

	int64_t * grid();
	IndexedListNode * gridObjectsChain(uint32_t x, uint32_t y);
	IndexedListNode * nextObject(IndexedListNode &node);
	IndexedListNode * objectsInRegion(IndexedListNodePool &listPool, Rectangle region);

	uint32_t cellsHorCount() const;
	uint32_t cellsVertCount() const;

protected:
	uint32_t m_cellWidth, m_cellHeight;
	uint32_t m_worldWidth, m_worldHeight;
	Coordinate2D m_worldCenter;
	uint32_t m_cellsHorCount, m_cellsVertCount;

	int64_t * m_grid;
	IndexedListNodePool pool;

	std::mutex moveMutex;

protected:
	inline void calculateGridCoordinates(uint32_t &cellX, uint32_t &cellY, const Coordinate2D &position);
	inline uint64_t calculateGridIndex(uint32_t cellX, uint32_t cellY) const;
	inline void pushObjectToCell(uint32_t cellX, uint32_t cellY, uint32_t object);
	inline void removeObjectFromCell(uint32_t cellX, uint32_t cellY, uint32_t object);
};