#pragma once

#include "primitives/Coordinate2D.h"

#include <mutex>
#include <vector>

class OccupationPartition
{
public:
	OccupationPartition(uint32_t maxCapacity, uint32_t cellWidth, uint32_t cellHeight, uint32_t worldWidth, uint32_t worldHeight);
	~OccupationPartition();

	void putToGrid(const Coordinate2D &position, uint32_t object);
	void removeFromGrid(const Coordinate2D &position, uint32_t object);
	void move(const Coordinate2D &oldPosition, const Coordinate2D &newPosition, uint32_t object);

	const std::vector<int64_t>& grid() const;

	uint32_t cellsHorCount() const;
	uint32_t cellsVertCount() const;

	bool isOccupied(uint32_t cellX, uint32_t cellY) const;
	bool isOccupied(uint32_t index) const;
	bool isOccupied(const Coordinate2D &position) const;
	bool isOccupiedByObject(const Coordinate2D &position, uint32_t object) const;
	bool isOccupiedNotByObject(const Coordinate2D &position, uint32_t object) const;

	int64_t cellOccupant(const Coordinate2D &position) const;

	uint64_t gridIndex(const Coordinate2D &position) const;

protected:
	uint32_t m_cellWidth, m_cellHeight;
	uint32_t m_worldWidth, m_worldHeight;
	Coordinate2D m_worldCenter;
	uint32_t m_cellsHorCount, m_cellsVertCount;

	std::vector<int64_t> m_grid;

	std::mutex moveMutex;

protected:
	inline void calculateGridCoordinates(uint32_t &cellX, uint32_t &cellY, const Coordinate2D &position) const;
	inline uint64_t calculateGridIndex(uint32_t cellX, uint32_t cellY) const;
	inline void pushObjectToCell(uint32_t cellX, uint32_t cellY, uint32_t object);
	inline void removeObjectFromCell(uint32_t cellX, uint32_t cellY, uint32_t object);
};
