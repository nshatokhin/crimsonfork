#include "DijkstraSearch.h"

bool DijkstraSearch::Search()
{
    m_pathExists = false;

    IdxType nextClosestNode, sourceIdx = static_cast<IdxType>(m_source);
    double newCost;

    m_costToThisNode[sourceIdx] = 0;

    IdxType nodeIdx = m_pq.insert(0);

    m_nodePqIndeces[nodeIdx] = sourceIdx;
    m_pqNodes[sourceIdx] = nodeIdx;

    while(m_pq.heapSize() > 0)
    {
        nextClosestNode = m_nodePqIndeces[m_pq.minIdx()];
        m_pq.extractMin();

        m_shortestPathTree[nextClosestNode] = m_searchFrontier[nextClosestNode];

        if(nextClosestNode == m_target)
        {
            m_pathExists = true;
            return true;
        }

        Graph::ConstEdgeIterator edges(m_graph, nextClosestNode);
        for (const Graph::EdgeType * curEdge = edges.begin();
                    !edges.end();
                    curEdge = edges.next())
        {
            newCost = m_costToThisNode[nextClosestNode] + curEdge->cost;

            if(m_searchFrontier[curEdge->to] == nullptr)
            {
                m_costToThisNode[curEdge->to] = newCost;

                nodeIdx = m_pq.insert(newCost);

                m_nodePqIndeces[nodeIdx] = curEdge->to;
                m_pqNodes[curEdge->to] = nodeIdx;

                m_searchFrontier[curEdge->to] = curEdge;
            }
            else if(newCost < m_costToThisNode[curEdge->to] && m_shortestPathTree[curEdge->to] == nullptr)
            {
                m_costToThisNode[curEdge->to] = newCost;

                m_pq.update(m_pqNodes[curEdge->to], newCost);

                m_searchFrontier[curEdge->to] = curEdge;
            }
        }
    }

    return false;
}

std::list<IdxType> DijkstraSearch::GetPath()
{
    std::list<IdxType> path;

    //just return an empty path if no target or no path found
    if (m_target < 0 || !m_pathExists)  return path;

    IdxType nd = static_cast<IdxType>(m_target);

    path.push_front(nd);

    while ((nd != m_source) && (m_shortestPathTree[nd] != nullptr))
    {
        nd = m_shortestPathTree[nd]->from;

        path.push_front(nd);
    }

    return path;
}
