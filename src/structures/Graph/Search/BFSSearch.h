#pragma once

#include "structures/Graph/Common.h"
#include "structures/Graph/Graph.h"

#include <queue>

class BFSSearch
{
public:
    BFSSearch(Graph &graph) :
        m_graph(graph),
        m_visited(graph.numNodes(), false),
        m_route(graph.numNodes(), -1),
        m_pathExists(false),
        m_source(-1),
        m_target(-1)
    {}

    void Init()
    {
        m_visited = std::vector<bool>(m_graph.numNodes(), false);
        m_route = std::vector<NodeIndex>(m_graph.numNodes(), -1);
        m_source = -1;
        m_target = -1;
    }

    bool Search(IdxType source, IdxType target);

    std::list<IdxType> GetPath();

#ifdef DEBUG
    //returns a vector containing pointers to all the edges the search has examined
    std::vector<const Edge*> GetSearchTree()const{return m_spanningTree;}
#endif

    bool isPathExist()
    {
        return m_pathExists;
    }

protected:
    Graph &m_graph;
    std::queue<const Graph::EdgeType *> m_queue;
    std::vector<bool> m_visited;
    std::vector<NodeIndex> m_route;
    bool m_pathExists;
    NodeIndex m_source;
    NodeIndex m_target;

#ifdef DEBUG
    //As the search progresses, this will hold all the edges the algorithm has
    //examined. THIS IS NOT NECESSARY FOR THE SEARCH, IT IS HERE PURELY
    //TO PROVIDE THE USER WITH SOME VISUAL FEEDBACK
    std::vector<const Edge*>  m_spanningTree;
#endif
};
