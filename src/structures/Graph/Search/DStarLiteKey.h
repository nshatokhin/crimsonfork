#pragma once

#include "Common.h"
#include "utils.h"

class DStarLiteKey
{
public:
    DStarLiteKey(double left = 0, double right = 0) : left(left), right(right) {}

    bool operator==(const DStarLiteKey &other) const
    {
        return isEqual(left, other.left) && isEqual(right, other.right);
    }

    bool operator!=(const DStarLiteKey &other) const
    {
        return !(*this == other);
    }

    bool operator > (const DStarLiteKey &other) const
    {
        if(isEqual(left, other.left))
        {
            return right > other.right;
        }
        else
        {
            return left > other.left;
        }
    }

    bool operator <= (const DStarLiteKey &other) const
    {
        if(isEqual(left, other.left))
        {
            return isEqual(right, other.right) || right < other.right;
        }
        else
        {
            return left < other.left;
        }
    }

    bool operator < (const DStarLiteKey &other) const
    {
        if(isEqual(left, other.left))
        {
            return right < other.right;
        }
        else
        {
            return left < other.left;
        }
    }

public:
    double left, right;

protected:
    static constexpr double EPSILON = std::numeric_limits<double>::epsilon();
};
