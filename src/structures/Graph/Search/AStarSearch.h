#pragma once

#include "structures/Graph/Common.h"
#include "structures/Graph/Graph.h"
#include "structures/GridPartition/OccupationPartition.h"
#include "structures/MinHeap.h"

template <class Heuristic>
class AStarSearch
{
public:
    AStarSearch(Graph * graph = nullptr, OccupationPartition * occupationGrid = nullptr) :
        m_graph(graph),
		m_graphNumNodes(graph ? graph->numNodes() : 1),
		m_occupationGrid(occupationGrid),
        m_pq(),
        m_shortestPathTree(m_graphNumNodes),
        m_nodePqIndeces(m_graphNumNodes),
        m_pqNodes(m_graphNumNodes),
        m_searchFrontier(m_graphNumNodes),
        m_source(-1),
        m_target(-1),
        m_pathExists(false)
    {}

    void Init(Graph * graph, OccupationPartition * occupationGrid, NodeIndex source, NodeIndex target)
    {
		assert(graph && occupationGrid);

		m_graph = graph;
		m_occupationGrid = occupationGrid;
		m_graphNumNodes = m_graph->numNodes();
        m_pq.init(m_graphNumNodes, M_INF, INF);
        m_shortestPathTree = std::vector<const Edge*>(m_graphNumNodes, nullptr);
        m_gCosts = std::vector<double>(m_graphNumNodes, 0);
        m_fCosts = std::vector<double>(m_graphNumNodes, 0);
        m_nodePqIndeces = std::vector<IdxType>(m_graphNumNodes, 0);
        m_pqNodes = std::vector<IdxType>(m_graphNumNodes, 0);
        m_searchFrontier = std::vector<const Edge*>(m_graphNumNodes, nullptr);
        m_source = source;
        m_target = target;
        m_pathExists = false;
    }

    bool Search();

    std::list<IdxType> GetPath();

#ifdef DEBUG
    //returns a vector containing pointers to all the edges the search has examined
    std::vector<const Edge*> GetSearchTree()const{return m_shortestPathTree;}
#endif

    bool isPathExist()
    {
        return m_pathExists;
    }

protected:
    static constexpr double M_INF = std::numeric_limits<double>::min();
    static constexpr double INF = std::numeric_limits<double>::max();

    Graph * m_graph;
	IdxType m_graphNumNodes;
    OccupationPartition * m_occupationGrid;
    MinHeap<double, IdxType> m_pq;
    std::vector<const Edge*> m_shortestPathTree;
    std::vector<IdxType> m_nodePqIndeces;
    std::vector<IdxType> m_pqNodes;
    std::vector<double> m_gCosts;
    std::vector<double> m_fCosts;
    std::vector<const Edge*> m_searchFrontier;
    NodeIndex m_source;
    NodeIndex m_target;
    bool m_pathExists;
};

template <class Heuristic>
bool AStarSearch<Heuristic>::Search()
{
    m_pathExists = false;

    IdxType nextClosestNode, sourceIdx = static_cast<IdxType>(m_source);
    double gCost, hCost;

    m_fCosts[sourceIdx] = 0;

    IdxType nodeIdx = m_pq.insert(0);

    m_nodePqIndeces[nodeIdx] = sourceIdx;
    m_pqNodes[sourceIdx] = nodeIdx;

    while(m_pq.heapSize() > 0)
    {
        nextClosestNode = m_nodePqIndeces[m_pq.minIdx()];
        m_pq.extractMin();

        m_shortestPathTree[nextClosestNode] = m_searchFrontier[nextClosestNode];

        if(nextClosestNode == m_target)
        {
            m_pathExists = true;
            return true;
        }

        Graph::ConstEdgeIterator edges(*m_graph, nextClosestNode);
        for (const Graph::EdgeType * curEdge = edges.begin();
                    !edges.end();
                    curEdge = edges.next())
        {
            if(m_occupationGrid->isOccupied(curEdge->to) && curEdge->to != m_target) continue;

            hCost = Heuristic::calculate(*m_graph, m_target, curEdge->to);
            gCost = m_gCosts[nextClosestNode] + curEdge->cost;

            if(m_searchFrontier[curEdge->to] == nullptr)
            {
                m_fCosts[curEdge->to] = gCost + hCost;
                m_gCosts[curEdge->to] = gCost;

                nodeIdx = m_pq.insert(m_fCosts[curEdge->to]);

                m_nodePqIndeces[nodeIdx] = curEdge->to;
                m_pqNodes[curEdge->to] = nodeIdx;

                m_searchFrontier[curEdge->to] = curEdge;
            }
            else if(gCost < m_gCosts[curEdge->to] && m_shortestPathTree[curEdge->to] == nullptr)
            {
                m_fCosts[curEdge->to] = gCost + hCost;
                m_gCosts[curEdge->to] = gCost;

                m_pq.update(m_pqNodes[curEdge->to], m_fCosts[curEdge->to]);

                m_searchFrontier[curEdge->to] = curEdge;
            }
        }
    }

    return false;
}

template <class Heuristic>
std::list<IdxType> AStarSearch<Heuristic>::GetPath()
{
    std::list<IdxType> path;

    //just return an empty path if no target or no path found
    if (m_target < 0 || !m_pathExists)  return path;

    IdxType nd = static_cast<IdxType>(m_target);

    path.push_front(nd);

    while ((nd != m_source) && (m_shortestPathTree[nd] != nullptr))
    {
        nd = m_shortestPathTree[nd]->from;

        path.push_front(nd);
    }

    return path;
}
