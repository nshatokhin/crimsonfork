#pragma once

#include "structures/Graph/Common.h"
#include "structures/Graph/Graph.h"
#include "structures/MinHeap.h"

#include <vector>

class DijkstraSearch
{
public:
    DijkstraSearch(Graph &graph) :
        m_graph(graph),
        m_pq(),
        m_shortestPathTree(graph.numNodes()),
        m_nodePqIndeces(graph.numNodes()),
        m_pqNodes(graph.numNodes()),
        m_searchFrontier(graph.numNodes()),
        m_source(-1),
        m_target(-1),
        m_pathExists(false)
    {}

    void Init(IdxType source, IdxType target)
    {
        m_pq.init(m_graph.numNodes(), M_INF, INF);
        m_shortestPathTree = std::vector<const Edge*>(m_graph.numNodes(), nullptr);
        m_costToThisNode = std::vector<double>(m_graph.numNodes(), INF);
        m_nodePqIndeces = std::vector<IdxType>(m_graph.numNodes(), 0);
        m_pqNodes = std::vector<IdxType>(m_graph.numNodes(), 0);
        m_searchFrontier = std::vector<const Edge*>(m_graph.numNodes(), nullptr);
        m_source = source;
        m_target = target;
        m_pathExists = false;
    }

    bool Search();

    std::list<IdxType> GetPath();

#ifdef DEBUG
    //returns a vector containing pointers to all the edges the search has examined
    std::vector<const Edge*> GetSearchTree()const{return m_shortestPathTree;}
#endif

    bool isPathExist()
    {
        return m_pathExists;
    }

protected:
    static constexpr double M_INF = std::numeric_limits<double>::min();
    static constexpr double INF = std::numeric_limits<double>::max();

    Graph &m_graph;
    MinHeap<double, IdxType> m_pq;
    std::vector<const Edge*> m_shortestPathTree;
    std::vector<IdxType> m_nodePqIndeces;
    std::vector<IdxType> m_pqNodes;
    std::vector<double> m_costToThisNode;
    std::vector<const Edge*> m_searchFrontier;
    NodeIndex m_source;
    NodeIndex m_target;
    bool m_pathExists;
};
