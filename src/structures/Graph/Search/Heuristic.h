#pragma once

#include "primitives/Vector2D.h"
#include "structures/Graph/Graph.h"

// sqrt(2)
#define M_SQRT2 1.41421356237309504880

class HeuristicDijkstra
{
public:
    HeuristicDijkstra(){}

    static double calculate(const Graph &, IdxType, IdxType)
    {
        return 0;
    }
};

class HeuristicEuclid
{
public:
    HeuristicEuclid(){}

    static double calculate(const Graph &graph, IdxType node1, IdxType node2)
    {
        return Vector2D(graph.GetNode(node1).position, graph.GetNode(node2).position).length();
    }
};

class HeuristicEuclidSquared
{
public:
    HeuristicEuclidSquared(){}

    static double calculate(const Graph &graph, IdxType node1, IdxType node2)
    {
        return Vector2D(graph.GetNode(node1).position, graph.GetNode(node2).position).lengthSqr();
    }
};


class HeuristicEightCondist
{
public:
    HeuristicEightCondist(){}

    static double calculate(const Graph &graph, IdxType node1, IdxType node2)
    {
        Graph::NodeType nd1 = graph.GetNode(node1);
        Graph::NodeType nd2 = graph.GetNode(node2);
        double temp;
        double min = fabs(nd1.position.x - nd2.position.x);
        double max = fabs(nd1.position.y - nd2.position.y);
        if (min > max)
        {
            temp = min;
            min = max;
            max = temp;
        }
        return ((M_SQRT2-1.0)*min + max);
    }
};
