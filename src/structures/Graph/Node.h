#pragma once

#include "Common.h"
#include "primitives/Coordinate2D.h"

#include <fstream>

class Node
{
public:
    Node(NodeIndex index = invalid_node_index, const Coordinate2D &position = Coordinate2D(), CoordType x = 0, CoordType y = 0) : index(index), position(position), x(x), y(y) {}

    //stream constructor
    Node(std::ifstream& stream)
    {
        char buffer[50];
        stream >> buffer >> index >> buffer >> position.x >> buffer >> position.y >> buffer >> x >> buffer >> y;
    }

    friend std::ostream& operator<<(std::ostream& os, const Node& n)
    {
        os << "Index: " << n.index << " PosX: " << n.position.x << " PosY: " << n.position.y << " Y: " << n.x << " X: " << n.y << std::endl;
        return os;
    }

public:
    NodeIndex index;
    Coordinate2D position;
    CoordType x, y;
};

