#pragma once

#include "primitives/Coordinate2D.h"
#include "primitives/Vector2D.h"
#include "structures/Graph/Graph.h"

//--------------------------- ValidNeighbour -----------------------------
//
//  returns true if x,y is a valid position in the map
//------------------------------------------------------------------------
bool ValidNeighbour(int x, int y, int NumCellsX, int NumCellsY)
{
	return !((x < 0) || (x >= NumCellsX) || (y < 0) || (y >= NumCellsY));
}

//------------ GraphHelper_AddAllNeighboursToGridNode ------------------
//
//  use to add he eight neighboring edges of a graph node that 
//  is positioned in a grid layout
//------------------------------------------------------------------------

void Graph_AddAllNeighboursToGridNode(Graph& graph,
	int         row,
	int         col,
	int         NumCellsX,
	int         NumCellsY)
{
	for (int i = -1; i<2; ++i)
	{
		for (int j = -1; j<2; ++j)
		{
			int nodeX = col + j;
			int nodeY = row + i;

			//skip if equal to this node
			if ((i == 0) && (j == 0)) continue;

			//check to see if this is a valid neighbour
			if (ValidNeighbour(nodeX, nodeY, NumCellsX, NumCellsY))
			{
				//calculate the distance to this node
				Coordinate2D PosNode = graph.GetNode(row*NumCellsX + col).position;
				Coordinate2D PosNeighbour = graph.GetNode(nodeY*NumCellsX + nodeX).position;

				double dist = Vector2D(PosNode, PosNeighbour).length();

				//this neighbour is okay so it can be added
				Graph::EdgeType NewEdge(row*NumCellsX + col,
					nodeY*NumCellsX + nodeX,
					dist);
				graph.addEdge(NewEdge);

				//if graph is not a diagraph then an edge needs to be added going
				//in the other direction
				if (!graph.isDigraph())
				{
					Graph::EdgeType NewEdge(nodeY*NumCellsX + nodeX,
						row*NumCellsX + col,
						dist);
					graph.addEdge(NewEdge);
				}
			}
		}
	}
}


//--------------------------- GraphHelper_CreateGrid --------------------------
//
//  creates a graph based on a grid layout. This function requires the 
//  dimensions of the environment and the number of cells required horizontally
//  and vertically 
//-----------------------------------------------------------------------------
void Graph_CreateGrid(Graph& graph,
    double CellWidth,
    double CellHeight,
    IdxType NumCellsX,
    IdxType NumCellsY,
    double offsetX = 0,
    double offsetY = 0)
{
	double midX = CellWidth / 2;
	double midY = CellHeight / 2;


	//first create all the nodes
    for (IdxType row = 0; row<NumCellsY; ++row)
	{
        for (IdxType col = 0; col<NumCellsX; ++col)
		{
            graph.addNode(Graph::NodeType(
                graph.getNextFreeNodeIndex(),
                Vector2D(
                    offsetX + midX + (col*CellWidth),
                    offsetY + midY + (row*CellHeight)
                ),
                col, row
                ));

		}
	}
	//now to calculate the edges. (A position in a 2d array [x][y] is the
	//same as [y*NumCellsX + x] in a 1d array). Each cell has up to eight
	//neighbours.
	for (int row = 0; row<NumCellsY; ++row)
	{
		for (int col = 0; col<NumCellsX; ++col)
		{
			Graph_AddAllNeighboursToGridNode(graph, row, col, NumCellsX, NumCellsY);
		}
	}
}
