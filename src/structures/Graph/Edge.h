#pragma once

#include "Common.h"

#include <fstream>

class Edge
{
public:
    Edge() : from(invalid_node_index), to(invalid_node_index), cost(0) {}
    Edge(IdxType from, IdxType to, double cost) : from(from), to(to), cost(cost) {}

    //stream constructor
    Edge(std::ifstream& stream)
    {
        char buffer[50];
        stream  >> buffer >> from >> buffer >> to >> buffer >> cost;
    }

    bool operator==(const Edge& rhs)
    {
        return rhs.from == this->from &&
               rhs.to   == this->to   &&
               isEqual(rhs.cost, this->cost);
    }

    bool operator!=(const Edge& rhs)
    {
        return !(*this == rhs);
    }

    //for reading and writing to streams.
    friend std::ostream& operator<<(std::ostream& os, const Edge& e)
    {
        os << "from: " << e.from << " to: " << e.to
           << " cost: " << e.cost << std::endl;

        return os;
    }

public:
    IdxType from;
    IdxType to;
    double cost;
};

