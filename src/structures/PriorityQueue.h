#pragma once

#include <queue>
#include <vector>

template <typename Value>
bool compare_value(const Value &left, const Value &right)
{
	return left == right;
}

template <typename Key>
bool compare_key(const Key &left, const Key &right)
{
	return left == right;
}

template <typename Key>
bool key_greater_than(const Key &key, const Key &otherKey)
{
	return key > otherKey;
}

template <typename Key, typename Value>
struct value_comparator : public std::unary_function<Value, bool>
{
	explicit value_comparator(const Value &baseline) : m_baseline(baseline) {}
	bool operator() (const std::pair<Key, Value> &arg)
	{
		return compare_value<Value>(arg.second, m_baseline);
	}
	Value m_baseline;
};

template <typename Key, typename Value>
struct key_comparator : public std::unary_function<Key, bool>
{
	explicit key_comparator(const Key &baseline) : m_baseline(baseline) {}
	bool operator() (const std::pair<Key, Value> &arg)
	{
		return compare_key<Key>(arg.first, m_baseline);
	}
	Key m_baseline;
};

template <typename Key, typename Value>
class record_comparator
{
public:
	bool operator() (const std::pair<Key, Value> &left, const std::pair<Key, Value> &right)
	{
		return key_greater_than<Key>(left.first, right.first);
	}
};

template <typename Key, typename Value, typename Comparator = record_comparator<Key, Value>>
class PriorityQueue : public std::priority_queue<std::pair<Key, Value>, std::vector<std::pair<Key, Value>>, Comparator>
{
public:
	PriorityQueue(size_t reserve_size, Key maxKey, Value zeroValue) : m_maxKey(maxKey), m_zeroValue(zeroValue)
	{
		this->c.reserve(reserve_size);
	}

	/*
	* Returns Value with smallest Key
	*/
	Value Top()
	{
		if (this->empty())
		{
			return m_zeroValue;
		}

		return this->top().second;
	}

	/*
	* Returns smallest key
	* @return Smallest Key
	*/
	Key TopKey()
	{
		if (this->empty())
		{
			return m_maxKey;
		}

		return this->top().first;
	}

	/*
	* Returns Value with smallest Key and removes it
	*/
	Value Pop()
	{
		if (this->empty()) return;

		Value value = Top();

		this->pop();

		return value;
	}

	/*
	* Returns Value with smallest Key and removes it
	*/
	std::pair<Key, Value> PopPair()
	{
		if (this->empty())
		{
			return std::pair<Key, Value>(m_maxKey, m_zeroValue);
		}

		std::pair<Key, Value> pair = this->top();

		this->pop();

		return pair;
	}

	/*
	* Inserts Value with key Key
	*/
	void Insert(Value value, Key key)
	{
		this->push(std::pair<Key, Value>(key, value));
	}

	/*
	* Updates key of Value to Key
	*/
	void Update(const Value &value, const Key &key)
	{
		auto it = std::find_if(this->c.begin(), this->c.end(), value_comparator<Key, Value>(value));
		if (it != this->c.end())
		{
			if (it->first != key)
			{
				removeByKey(it->first);
				Insert(value, key);
			}
		}
	}

	/*
	* Removes Value
	* @return Success flag
	*/
	bool Remove(const Value &value)
	{
		return removeByValue(value);
	}

	/*
	* Returns emptiness flag
	*/
	bool Empty() const
	{
		return this->empty();
	}

protected:
	bool removeByValue(const Value& value) {
		auto it = std::find_if(this->c.begin(), this->c.end(), value_comparator<Key, Value>(value));
		if (it != this->c.end()) {
			this->c.erase(it);
			std::make_heap(this->c.begin(), this->c.end(), this->comp);
			return true;
		}
		else {
			return false;
		}
	}

	bool removeByKey(const Key& key) {
		auto it = std::find_if(this->c.begin(), this->c.end(), key_comparator<Key, Value>(key));
		if (it != this->c.end()) {
			this->c.erase(it);
			std::make_heap(this->c.begin(), this->c.end(), this->comp);
			return true;
		}
		else {
			return false;
		}
	}

protected:
	Key m_maxKey;
	Value m_zeroValue;
};
