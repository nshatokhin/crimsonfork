/*
 * Author: Mykola Shatokhin
 * Date: 04 Sep 2019
 */

#include "QuadTreeNode.h"


QuadTreeNode::QuadTreeNode() :
    childrenCount(0),
    treeLevel(0),
    nodeId(0),
	elementsCount(0)
{

}

void QuadTreeNode::init(Rectangle boundingRect, uint32_t level, uint32_t id)
{
    childrenCount = 0;
    //elements.clear();
	elementsCount = 0;
    aabb = boundingRect;
    treeLevel = level;
    nodeId = id;
}
