/*
 * Author: Mykola Shatokhin
 * Date: 04 Sep 2019
 */

#pragma once

#include "primitives/Rectangle.h"

class QuadTreeNode;

class NodesPool
{
protected:
    uint32_t m_activeCount;
    uint32_t m_leavesCount;
    uint32_t m_nodesCount;
    
    uint32_t m_maxCapacity;
    QuadTreeNode * m_objects;
    
    uint32_t * m_externalIndices;
    uint32_t * m_internalIndices;
    
    uint32_t m_firstLeaf;
    uint32_t m_lastLeaf;
    uint32_t m_firstNode;
    uint32_t m_lastNode;
    uint32_t m_firstFree;
    
public:
    NodesPool(uint32_t maxCapacity);
    ~NodesPool();

    QuadTreeNode * getTreeNode(uint32_t index);
    uint32_t activeCount() const;
    
    uint32_t createLeaf(QuadTreeNode &nodeToProtect, Rectangle boundingRect, uint32_t level);
    uint32_t createNode(QuadTreeNode &nodeToProtect, Rectangle boundingRect, uint32_t level);

    void destroyObject(QuadTreeNode &nodeToProtect, uint32_t objectIndex);
 
    void transformToNode(QuadTreeNode &nodeToProtect, uint32_t objectIndex);
    void transformToLeaf(QuadTreeNode &nodeToProtect, uint32_t objectIndex);

    QuadTreeNode * objects();
    uint32_t leavesCount() const;
    uint32_t nodesCount() const;

protected:
    inline void exchangeObjects(uint32_t object1, uint32_t object2);

    inline void destroyLeaf(uint32_t internalIndex);
    inline void destroyNode(uint32_t internalIndex);

};
