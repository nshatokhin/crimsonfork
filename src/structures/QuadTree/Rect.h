/*
 * Author: Mykola Shatokhin
 * Date: 04 Sep 2019
 */

#pragma once

#include "Point.h"

class Rect {
public:
    int x, y;
    int width, height;

    Rect(int xv = 0, int yv = 0, int w = 0, int h = 0) : x(xv), y(yv), width(w), height(h) {}
    
    bool intersect(Point p)
    {
        return p.x >= x && p.y >= y &&
p.x <= x + width && p.y <= y + height;
    }
};
