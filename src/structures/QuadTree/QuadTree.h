/*
 * Author: Mykola Shatokhin
 * Date: 04 Sep 2019
 */

#pragma once

#include <algorithm>
#include <cassert>
#include <cmath>
#include <memory>
#include <vector>

#include "NodesPool.h"
#include "Point.h"
#include "QuadTreeNode.h"

#include "common/Common.h"
#include "primitives/Hitmap2D.h"
#include "primitives/Rectangle.h"

class QuadTree
{
public:
    static constexpr uint8_t NODE_CHILDREN_COUNT = 4;
    static constexpr uint32_t MAX_OBJ_COUNT = 4;

public:
    QuadTree(Point worldLeftTop, int32_t worldWidth, int32_t worldHeight, uint32_t maxTreeLevel, uint32_t maxElementsInNode);
    
    NodesPool * getNodesPool();

    void insert(const TreeElement &treeElement);
    void remove(const TreeElement &treeElement);
    void removeEmptyNodes();

protected:
    void _insert(NodesPool &pool, uint32_t currentNodeId, /*const Hitmap2D<HITMAP_MAXIMUM_POINTS_COUNT>& h*/const TreeElement &treeElement);
    inline void _createTreeNode(QuadTreeNode &currentTreeNode, int32_t x, int32_t y, int32_t width, int32_t height, uint32_t m_treeLevel);
    void _subdivide(QuadTreeNode &currentTreeNode);
    void _remove(NodesPool &pool, uint32_t currentNodeId, /*const Hitmap2D<HITMAP_MAXIMUM_POINTS_COUNT>& h*/const TreeElement &treeElement);
    void _removeEmptyNodes(NodesPool &pool, uint32_t currentNodeId);

private:
	uint32_t m_maxTreeLevel;
	uint32_t m_maxElementsInNode;
    std::unique_ptr<NodesPool> nodesPool;
    uint32_t m_head;
};
