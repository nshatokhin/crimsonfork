/*
 * Author: Mykola Shatokhin
 * Date: 04 Sep 2019
 */

#include "NodesPool.h"

#include <cassert>
#include <iostream>

#include "QuadTreeNode.h"


NodesPool::NodesPool(uint32_t maxCapacity) :
    m_activeCount(0),
    m_leavesCount(0),
    m_nodesCount(0),
    m_maxCapacity(maxCapacity),
    m_firstLeaf(0),
    m_lastLeaf(0),
    m_firstNode(0),
    m_lastNode(0),
    m_firstFree(0)
{
    m_objects = new QuadTreeNode[m_maxCapacity];
    m_externalIndices = new uint32_t[m_maxCapacity];
    m_internalIndices = new uint32_t[m_maxCapacity];

    for(uint32_t i=0;i<m_maxCapacity;i++)
    {
        m_externalIndices[i] = i;
        m_internalIndices[i] = i;
    }
}

NodesPool::~NodesPool()
{
    delete[] m_objects;
    delete[] m_externalIndices;
    delete[] m_internalIndices;
}

QuadTreeNode * NodesPool::getTreeNode(uint32_t objectIndex)
{
    assert(m_activeCount > 0 && objectIndex < m_maxCapacity);
        
    uint32_t internalIndex = m_internalIndices[objectIndex];

    assert(internalIndex < m_activeCount);
	if (m_objects[internalIndex].nodeId != objectIndex)
		std::cout << "ololo" << std::endl;
    return &m_objects[internalIndex];
}

uint32_t NodesPool::activeCount() const
{
    return m_activeCount;
}

inline void NodesPool::exchangeObjects(uint32_t object1, uint32_t object2)
{
    std::swap(m_objects[object1], m_objects[object2]);
    std::swap(m_internalIndices[m_externalIndices[object1]], m_internalIndices[m_externalIndices[object2]]);
    std::swap(m_externalIndices[object1], m_externalIndices[object2]);
}

uint32_t NodesPool::createLeaf(QuadTreeNode &nodeToProtect, Rectangle boundingRect, uint32_t level)
{
    assert(m_activeCount < m_maxCapacity);

	uint32_t nodeToProtectId = nodeToProtect.nodeId;

    m_lastNode = m_firstFree;
    m_firstFree++;

    if(m_nodesCount > 0 && m_firstNode != m_lastNode)
    {
        exchangeObjects(m_firstNode, m_lastNode);
    }

    m_lastLeaf = m_firstNode;

    uint32_t leafIndex = m_externalIndices[m_lastLeaf];

    // init leaf
    m_objects[m_lastLeaf].init(boundingRect, level, leafIndex);

    m_activeCount++;
    m_leavesCount++;

    m_firstNode++;

	if (nodeToProtect.nodeId != nodeToProtectId)
	{
		nodeToProtect = m_objects[m_internalIndices[nodeToProtectId]];
	}

    return leafIndex;
}

uint32_t NodesPool::createNode(QuadTreeNode &nodeToProtect, Rectangle boundingRect, uint32_t level)
{
    assert(m_activeCount < m_maxCapacity);

	uint32_t nodeToProtectId = nodeToProtect.nodeId;

    m_lastNode = m_firstFree;
    m_firstFree++;

    uint32_t nodeIndex = m_externalIndices[m_lastNode];

    // init node
    m_objects[m_lastNode].init(boundingRect, level, nodeIndex);

    m_activeCount++;
    m_nodesCount++;

	if (nodeToProtect.nodeId != nodeToProtectId)
	{
		nodeToProtect = m_objects[m_internalIndices[nodeToProtectId]];
	}

    return nodeIndex;
}
    
void NodesPool::destroyObject(QuadTreeNode &nodeToProtect, uint32_t objectIndex)
{
    assert(m_activeCount > 0 && objectIndex < m_maxCapacity);
    
	uint32_t nodeToProtectId = nodeToProtect.nodeId;
    uint32_t internalIndex = m_internalIndices[objectIndex];
    
    assert(internalIndex >= m_firstLeaf && internalIndex <= m_lastNode);
    
    if(internalIndex <= m_lastLeaf)
    {
        destroyLeaf(internalIndex);
    }
    else
    {
        destroyNode(internalIndex);
    }

	if (nodeToProtect.nodeId != nodeToProtectId)
	{
		nodeToProtect = m_objects[m_internalIndices[nodeToProtectId]];
	}
}

void NodesPool::destroyLeaf(uint32_t internalIndex)
{
    assert(internalIndex >= m_firstLeaf && internalIndex <= m_lastLeaf);

    if(internalIndex != m_lastLeaf)
    {
        exchangeObjects(internalIndex, m_lastLeaf);
    }

    if(m_lastLeaf != m_lastNode)
    {
        exchangeObjects(m_lastLeaf, m_lastNode);
    }

    m_lastLeaf--;
    m_firstNode--;
    m_lastNode--;
    m_firstFree--;
    m_leavesCount--;
    m_activeCount--;
}

void NodesPool::destroyNode(uint32_t internalIndex)
{
    assert(internalIndex >= m_firstNode && internalIndex <= m_lastNode);

    if(internalIndex != m_lastNode)
    {
        exchangeObjects(internalIndex, m_lastNode);
    }

    m_lastNode--;
    m_firstFree--;
    m_nodesCount--;
    m_activeCount--;
}

void NodesPool::transformToNode(QuadTreeNode &nodeToProtect, uint32_t objectIndex)
{
    assert(objectIndex < m_maxCapacity);

	uint32_t nodeToProtectId = nodeToProtect.nodeId;
    uint32_t internalIndex = m_internalIndices[objectIndex];

    if(internalIndex >= m_firstNode && internalIndex <= m_lastNode)
    {
        // already is node
        return;
    }

    assert(m_leavesCount > 0 && internalIndex >= m_firstLeaf && internalIndex <= m_lastLeaf);

    if(internalIndex != m_lastLeaf)
    {
        exchangeObjects(internalIndex, m_lastLeaf);
    }
    m_lastLeaf--;
    m_firstNode--;
    m_leavesCount--;
    m_nodesCount++;

	if (nodeToProtect.nodeId != nodeToProtectId)
	{
		nodeToProtect = m_objects[m_internalIndices[nodeToProtectId]];
	}
}

void NodesPool::transformToLeaf(QuadTreeNode &nodeToProtect, uint32_t objectIndex)
{
    assert(objectIndex < m_maxCapacity);

	uint32_t nodeToProtectId = nodeToProtect.nodeId;
    uint32_t internalIndex = m_internalIndices[objectIndex];

    if(internalIndex >= m_firstLeaf && internalIndex <= m_lastLeaf)
    {
        // already is leaf
        return;
    }

    assert(m_nodesCount > 0 && internalIndex >= m_firstNode && internalIndex <= m_lastNode);

    if(internalIndex != m_firstNode)
    {
        exchangeObjects(internalIndex, m_firstNode);
    }

    m_lastLeaf++;
    m_firstNode++;
    m_leavesCount++;
    m_nodesCount--;

	if (nodeToProtect.nodeId != nodeToProtectId)
	{
		nodeToProtect = m_objects[m_internalIndices[nodeToProtectId]];
	}
}

QuadTreeNode *NodesPool::objects()
{
    return m_objects;
}

uint32_t NodesPool::leavesCount() const
{
    return m_leavesCount;
}

uint32_t NodesPool::nodesCount() const
{
    return m_nodesCount;
}
