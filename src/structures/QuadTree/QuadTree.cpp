/*
 * Author: Mykola Shatokhin
 * Date: 04 Sep 2019
 */

#include "QuadTree.h"

#include <algorithm>

QuadTree::QuadTree(Point worldLeftTop, int32_t worldWidth, int32_t worldHeight, uint32_t maxTreeLevel, uint32_t maxElementsInNode) :
	m_maxTreeLevel(maxTreeLevel), m_maxElementsInNode(maxElementsInNode)
{
    uint32_t maxNodesCount = 1;
    for (uint32_t i = 1; i <= m_maxTreeLevel; i++)
    {
        maxNodesCount += static_cast<uint32_t>(pow(NODE_CHILDREN_COUNT, i));
    }

    nodesPool = std::unique_ptr<NodesPool>(new NodesPool(maxNodesCount));

    Rectangle worldRect(Coordinate2D(worldLeftTop.x, worldLeftTop.y), worldWidth, worldHeight);
    m_head = nodesPool->createLeaf(QuadTreeNode(), worldRect, 0);
}

NodesPool *QuadTree::getNodesPool()
{
    return nodesPool.get();
}

void QuadTree::insert(const TreeElement &treeElement)
{
    _insert(*(nodesPool.get()), m_head, treeElement);
}

void QuadTree::remove(const TreeElement &treeElement)
{
    _remove(*(nodesPool.get()), m_head, treeElement);
}

void QuadTree::removeEmptyNodes()
{
    _removeEmptyNodes(*(nodesPool.get()), m_head);
}

void QuadTree::_insert(NodesPool &pool, uint32_t currentNodeId, /*const Hitmap2D<HITMAP_MAXIMUM_POINTS_COUNT>& h*/const TreeElement &treeElement)
{
    QuadTreeNode &currentTreeNode = *pool.getTreeNode(currentNodeId);

    if (!currentTreeNode.aabb.intersect(treeElement.hitmap)) return;

    if (currentTreeNode.treeLevel == m_maxTreeLevel || (currentTreeNode.elementsCount < m_maxElementsInNode && currentTreeNode.childrenCount == 0))
    {
        //currentTreeNode.elements.push_back(physicsComponent);

		currentTreeNode.elements[currentTreeNode.elementsCount++] = treeElement;

        return;
    }

    bool transformationNeeded = false;
    if (currentTreeNode.childrenCount == 0)
    {
        _subdivide(currentTreeNode);

        for(uint32_t i=0;i<currentTreeNode.elementsCount;i++)
        {
            for (uint32_t j = 0; j<currentTreeNode.childrenCount; j++)
            {
                _insert(pool, currentTreeNode.children[j], currentTreeNode.elements[i]);
            }
        }

        //currentTreeNode.elements.clear();
		currentTreeNode.elementsCount = 0;

        transformationNeeded = true;
    }

    for (uint32_t i = 0; i<currentTreeNode.childrenCount; i++)
    {
        _insert(pool, currentTreeNode.children[i], treeElement);
    }

    if(transformationNeeded)
    {
        nodesPool->transformToNode(currentTreeNode, currentTreeNode.nodeId);
    }
}

void QuadTree::_createTreeNode(QuadTreeNode &currentTreeNode, int32_t x, int32_t y, int32_t width, int32_t height, uint32_t m_treeLevel)
{
    Rectangle rect(Coordinate2D(x, y), width, height);
    currentTreeNode.children[currentTreeNode.childrenCount++] = nodesPool->createLeaf(currentTreeNode, rect, m_treeLevel);
}

void QuadTree::_subdivide(QuadTreeNode &currentTreeNode)
{
    int32_t cellWidth = currentTreeNode.aabb.width / QuadTreeNode::ROWS;
    int32_t cellHeight = currentTreeNode.aabb.height / QuadTreeNode::COLS;

    int32_t curX = static_cast<int32_t>(currentTreeNode.aabb.ltCorner.x);
    int32_t curY = static_cast<int32_t>(currentTreeNode.aabb.ltCorner.y);

    // Few cycles for prevent losing few pixels because of integer rounding errors
    for (uint8_t i = 0; i < QuadTreeNode::ROWS - 1; i++)
    {
        for (uint8_t j = 0; j < QuadTreeNode::COLS - 1; j++)
        {
            _createTreeNode(currentTreeNode, curX, curY, cellWidth, cellHeight, currentTreeNode.treeLevel + 1);

            curX += cellWidth;
        }

        _createTreeNode(currentTreeNode, curX, curY, currentTreeNode.aabb.width - (QuadTreeNode::COLS - 1) * cellWidth, cellHeight, currentTreeNode.treeLevel + 1);

        curX = static_cast<int32_t>(currentTreeNode.aabb.ltCorner.x);
        curY += cellHeight;
    }

    for (uint8_t j = 0; j < QuadTreeNode::COLS - 1; j++)
    {
        _createTreeNode(currentTreeNode, curX, curY, cellWidth, currentTreeNode.aabb.height - (QuadTreeNode::ROWS - 1) * cellHeight, currentTreeNode.treeLevel + 1);

        curX += cellWidth;
    }

    _createTreeNode(currentTreeNode, curX, curY, currentTreeNode.aabb.width - (QuadTreeNode::COLS - 1) * cellWidth, currentTreeNode.aabb.height - (QuadTreeNode::ROWS - 1) * cellHeight, currentTreeNode.treeLevel + 1);
}

void QuadTree::_remove(NodesPool &pool, uint32_t currentNodeId, /*const Hitmap2D<HITMAP_MAXIMUM_POINTS_COUNT>& h*/const TreeElement &treeElement)
{
	QuadTreeNode &currentTreeNode = *pool.getTreeNode(currentNodeId);//*nodesPool->getTreeNode(currentNodeId);

    if (!currentTreeNode.aabb.intersect(treeElement.hitmap)) return;

    if (currentTreeNode.treeLevel == m_maxTreeLevel || currentTreeNode.childrenCount == 0)
    {
        //currentTreeNode.elements.erase(std::remove(currentTreeNode.elements.begin(), currentTreeNode.elements.end(), physicsComponent), currentTreeNode.elements.end());
		
		for (uint32_t i = 0; i < currentTreeNode.elementsCount; i++)
		{
			if (currentTreeNode.elements[i].physicsComponent == treeElement.physicsComponent)
			{
				if (i < currentTreeNode.elementsCount - 1)
				{
					currentTreeNode.elements[i] = currentTreeNode.elements[currentTreeNode.elementsCount - 1];
					//std::swap(currentTreeNode.elements[i], currentTreeNode.elements[currentTreeNode.elementsCount - 1]);
				}

				currentTreeNode.elementsCount--;
			}
		}
		
        return;
    }

    for (uint32_t i = 0; i<currentTreeNode.childrenCount; i++)
    {
        _remove(pool, currentTreeNode.children[i], treeElement);
    }
}

void QuadTree::_removeEmptyNodes(NodesPool &pool, uint32_t currentNodeId)
{
    QuadTreeNode &currentTreeNode = *nodesPool->getTreeNode(currentNodeId);

    uint32_t notEmptyChildren = 0;
    uint32_t lastNonEmptyIndex = 0;

    for (uint32_t i = 0; i < currentTreeNode.childrenCount; i++)
    {
        QuadTreeNode * treeNode = nodesPool->getTreeNode(currentTreeNode.children[i]);
        _removeEmptyNodes(pool, treeNode->nodeId);

        if (/*treeNode->elements.size() > 0*/treeNode->elementsCount > 0 || treeNode->childrenCount > 0)
        {
            notEmptyChildren++;
            lastNonEmptyIndex = i;
        }
    }

    if (notEmptyChildren > 1)
    {
        return;
    }

    if (notEmptyChildren == 1)
    {
		QuadTreeNode * childNode = nodesPool->getTreeNode(currentTreeNode.children[lastNonEmptyIndex]);
        //std::vector<uint32_t> *childElements = &(childNode->elements);
        //currentTreeNode.elements.insert( currentTreeNode.elements.end(), childElements->begin(), childElements->end() );
		TreeElement *childElements = childNode->elements;
		for (uint32_t i = 0; i < childNode->elementsCount; i++)
		{
			currentTreeNode.elements[currentTreeNode.elementsCount++] = childNode->elements[i];
		}
		currentTreeNode.elementsCount += childNode->elementsCount;
    }

    for (uint32_t i = 0; i<currentTreeNode.childrenCount; i++)
    {
        nodesPool->destroyObject(currentTreeNode, currentTreeNode.children[i]);
    }

    currentTreeNode.childrenCount = 0;
    nodesPool->transformToLeaf(currentTreeNode, currentTreeNode.nodeId);
}
