/*
 * Author: Mykola Shatokhin
 * Date: 04 Sep 2019
 */

#pragma once

#include <cstdint>

class Point {
public:
    int32_t x, y;

    Point(int xv = 0, int yv = 0) : x(xv), y(yv) {}
};
