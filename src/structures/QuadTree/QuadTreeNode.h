/*
 * Author: Mykola Shatokhin
 * Date: 04 Sep 2019
 */

#pragma once

#include "NodesPool.h"

#include <algorithm>
#include <cassert>
#include <cstring>
#include <vector>

#include "primitives/Rectangle.h"

class TreeElement {
public:
	TreeElement(uint32_t index = 0, const GameObjectHitmap &h = GameObjectHitmap()) :
		physicsComponent(index),
		hitmap(h)
	{}

	uint32_t physicsComponent;
	GameObjectHitmap hitmap;

	bool operator==(const TreeElement &other)
	{
		return physicsComponent == other.physicsComponent;
	}

	TreeElement& operator=(const TreeElement &other)
	{
		if (this != &other)
		{
			physicsComponent = other.physicsComponent;
			hitmap = other.hitmap;
		}

		return *this;
	}
};

class QuadTreeNode
{
public:
    static constexpr uint8_t ROWS = 2;
    static constexpr uint8_t COLS = 2;

    uint32_t children[ROWS*COLS];
    uint32_t childrenCount;

    Rectangle aabb;
    uint32_t treeLevel;
    uint32_t nodeId;

    //std::vector<uint32_t> elements;
	TreeElement elements[50];
	uint32_t elementsCount;


public:
    QuadTreeNode();

	QuadTreeNode& operator=(const QuadTreeNode& other)
	{
		if (this != &other)
		{
			childrenCount = other.childrenCount;
			for (uint32_t i = 0; i < other.childrenCount; i++)
			{
				children[i] = other.children[i];
			}

			aabb = other.aabb;
			treeLevel = other.treeLevel;
			nodeId = other.nodeId;

			elementsCount = other.elementsCount;
			for (uint32_t i = 0; i < other.elementsCount; i++)
			{
				elements[i] = other.elements[i];
			}
		}

		return *this;
	}

    void init(Rectangle boundingRect, uint32_t level, uint32_t id);

};
