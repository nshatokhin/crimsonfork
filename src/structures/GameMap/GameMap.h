#pragma once

#include "primitives/Wall2D.h"
#include "structures/Graph/Graph.h"

class GameMap
{
public:
	GameMap(int64_t mapWidth, int64_t mapHeight, uint32_t gridWidthCount, uint32_t gridHeightCount);
	~GameMap();

	uint32_t wallsCount() const;
	WallVector walls();

	Graph& graph();

public:
	static constexpr uint32_t WALLS_COUNT = 8;

protected:
	WallVector m_walls;
	Graph m_graph;

private:
	//GameMap(const GameMap &map) {}
};
