#include "GameMap.h"

GameMap::GameMap(int64_t mapWidth, int64_t mapHeight, uint32_t gridWidthCount, uint32_t gridHeightCount) :
	m_graph(gridWidthCount * gridHeightCount)
{
	m_walls = new Wall2D[WALLS_COUNT];

	// TODO: load from map file
	m_walls[0] = Wall2D(Coordinate2D(-mapWidth / 2, -mapHeight / 2), Coordinate2D(175, -mapHeight / 2));
	m_walls[1] = Wall2D(Coordinate2D(175, -mapHeight / 2), Coordinate2D(175, -150));
	m_walls[2] = Wall2D(Coordinate2D(175, -150), Coordinate2D(225, -150));
	m_walls[3] = Wall2D(Coordinate2D(Coordinate2D(225, -150)), Coordinate2D(225, -mapHeight / 2));
	m_walls[4] = Wall2D(Coordinate2D(225, -mapHeight / 2), Coordinate2D(mapWidth / 2, -mapHeight / 2));
	m_walls[5] = Wall2D(Coordinate2D(mapWidth / 2, -mapHeight / 2), Coordinate2D(mapWidth / 2, mapHeight / 2));
	m_walls[6] = Wall2D(Coordinate2D(mapWidth / 2, mapHeight / 2), Coordinate2D(-mapWidth / 2, mapHeight / 2));
	m_walls[7] = Wall2D(Coordinate2D(-mapWidth / 2, mapHeight / 2), Coordinate2D(-mapWidth / 2, -mapHeight / 2));

	m_graph.load("./assets/maps/map.grf");
}

GameMap::~GameMap()
{
	delete[] m_walls;
}

uint32_t GameMap::wallsCount() const
{
	return WALLS_COUNT;
}

WallVector GameMap::walls()
{
	return m_walls;
}

Graph & GameMap::graph()
{
	return m_graph;
}
