/*
 * Author: Mykola Shatokhin
 * Date: 04 Sep 2019
 */

#pragma once

#include <algorithm>
#include <cassert>
#include <cstdint>


template <typename T>
class ObjectPoolDD
{
protected:
    uint32_t m_activeCount;
    uint32_t m_mainDomainCount;
    uint32_t m_secondaryDomainCount;
    
    uint32_t m_maxCapacity;
    T * m_objects;
    
    uint32_t * m_externalIndices;
    uint32_t * m_internalIndices;
    
    uint32_t m_firstMain;
    uint32_t m_lastMain;
    uint32_t m_firstSecondary;
    uint32_t m_lastSecondary;
    uint32_t m_firstFree;
    
public:
    ObjectPoolDD(uint32_t maxCapacity) :
        m_activeCount(0),
        m_mainDomainCount(0),
        m_secondaryDomainCount(0),
        m_maxCapacity(maxCapacity),
        m_firstMain(0),
        m_lastMain(0),
        m_firstSecondary(0),
        m_lastSecondary(0),
        m_firstFree(0)
    {
        m_objects = new T[m_maxCapacity];
        m_externalIndices = new uint32_t[m_maxCapacity];
        m_internalIndices = new uint32_t[m_maxCapacity];

        for(uint32_t i=0;i<m_maxCapacity;i++)
        {
            m_externalIndices[i] = i;
            m_internalIndices[i] = i;
        }
    }
    
    ~ObjectPoolDD()
    {
        delete[] m_objects;
        delete[] m_externalIndices;
        delete[] m_internalIndices;
    }
    

    T * getObject(uint32_t objectIndex)
    {
        assert(m_activeCount > 0 && objectIndex < m_maxCapacity);
        
        uint32_t internalIndex = m_internalIndices[objectIndex];

        assert(internalIndex < m_activeCount);
        
        return &m_objects[internalIndex];
    }
    
    uint32_t activeCount() const
    {
        return m_activeCount;
    }
    
    uint32_t createMainDomain()
    {
        assert(m_activeCount < m_maxCapacity);

        m_lastSecondary = m_firstFree;
        m_firstFree++;

        if(m_secondaryDomainCount > 0 && m_firstSecondary != m_lastSecondary)
        {
            exchangeObjects(m_firstSecondary, m_lastSecondary);
        }

        m_lastMain = m_firstSecondary;

        uint32_t mainIndex = m_externalIndices[m_lastMain];

        m_activeCount++;
        m_mainDomainCount++;

        m_firstSecondary++;

        return mainIndex;
    }
        
    uint32_t createSecondaryDomain()
    {
        assert(m_activeCount < m_maxCapacity);

        m_lastSecondary = m_firstFree;
        m_firstFree++;

        uint32_t secondaryIndex = m_externalIndices[m_lastSecondary];

        m_activeCount++;
        m_secondaryDomainCount++;

        return secondaryIndex;
    }
    

    void destroyObject(uint32_t objectIndex)
    {
        assert(m_activeCount > 0 && objectIndex < m_maxCapacity);
    
        uint32_t internalIndex = m_internalIndices[objectIndex];
    
        assert(internalIndex >= m_firstMain && internalIndex <= m_lastSecondary);
    
        if(internalIndex <= m_lastMain)
        {
            destroyMainDomainObject(internalIndex);
        }
        else
        {
            destroySecondaryDomainObject(internalIndex);
        }
    }
 
    void transformToSecondary(uint32_t objectIndex)
    {
        assert(objectIndex < m_maxCapacity);

        uint32_t internalIndex = m_internalIndices[objectIndex];

        if(internalIndex >= m_firstSecondary && internalIndex <= m_lastSecondary)
        {
            // already is secondary domain
            return;
        }

        assert(m_mainDomainCount > 0 && internalIndex >= m_firstMain && internalIndex <= m_lastMain);

        if(internalIndex != m_lastMain)
        {
            exchangeObjects(internalIndex, m_lastMain);
        }
        m_lastMain--;
        m_firstSecondary--;
        m_mainDomainCount--;
        m_secondaryDomainCount++;
    }
    
    void transformToMain(uint32_t objectIndex)
    {
        assert(objectIndex < m_maxCapacity);

        uint32_t internalIndex = m_internalIndices[objectIndex];

        if(internalIndex >= m_firstMain && internalIndex <= m_lastMain)
        {
            // already is main domain
            return;
        }

        assert(m_secondaryDomainCount > 0 && internalIndex >= m_firstSecondary && internalIndex <= m_lastSecondary);

        if(internalIndex != m_firstSecondary)
        {
            exchangeObjects(internalIndex, m_firstSecondary);
        }

        m_lastMain++;
        m_firstSecondary++;
        m_mainDomainCount++;
        m_secondaryDomainCount--;
    }

    T * objects()
    {
        return m_objects;
    }
    
    uint32_t mainDomainCount() const
    {
        return m_mainDomainCount;
    }
    
    uint32_t secondaryDomainCount() const
    {
        return m_secondaryDomainCount;
    }

protected:
    inline void exchangeObjects(uint32_t object1, uint32_t object2)
    {
        std::swap(m_objects[object1], m_objects[object2]);
        std::swap(m_internalIndices[m_externalIndices[object1]], m_internalIndices[m_externalIndices[object2]]);
        std::swap(m_externalIndices[object1], m_externalIndices[object2]);
    }

    inline void destroyMainDomainObject(uint32_t internalIndex)
    {
        assert(internalIndex >= m_firstMain && internalIndex <= m_lastMain);

        if(internalIndex != m_lastMain)
        {
            exchangeObjects(internalIndex, m_lastMain);
        }

        if(m_lastMain != m_lastSecondary)
        {
            exchangeObjects(m_lastMain, m_lastSecondary);
        }

        m_lastMain--;
        m_firstSecondary--;
        m_lastSecondary--;
        m_firstFree--;
        m_mainDomainCount--;
        m_activeCount--;
    }
    
    inline void destroySecondaryDomainObject(uint32_t internalIndex)
    {
        assert(internalIndex >= m_firstSecondary && internalIndex <= m_lastSecondary);

        if(internalIndex != m_lastSecondary)
        {
            exchangeObjects(internalIndex, m_lastSecondary);
        }

        m_lastSecondary--;
        m_firstFree--;
        m_secondaryDomainCount--;
        m_activeCount--;
    }
};
