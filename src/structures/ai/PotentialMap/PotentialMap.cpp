#include "PotentialMap.h"

PotentialMap::PotentialMap(uint32_t maxCapacity, uint32_t cellWidth, uint32_t cellHeight, uint32_t worldWidth, uint32_t worldHeight) :
	m_cellWidth(cellWidth), m_cellHeight(cellHeight),
	m_worldCenter(worldWidth / 2, worldHeight / 2),
	m_worldWidth(worldWidth), m_worldHeight(worldHeight),
	m_cellsHorCount(m_worldWidth / m_cellWidth + 1),
	m_cellsVertCount(m_worldHeight / m_cellHeight + 1)
{
	m_map = new uint32_t[m_cellsHorCount * m_cellsVertCount];
	for (uint64_t i = 0; i < m_cellsHorCount * m_cellsVertCount; i++)
	{
		m_map[i] = 0;
	}
}

PotentialMap::~PotentialMap()
{
	delete[] m_map;
}

void PotentialMap::setTarget(const Coordinate2D & target)
{
	uint32_t cellX, cellY;
	m_target = target;
	calculateGridCoordinates(cellX, cellY, m_target);

	setPotential(cellX, cellY, 0);
}

void PotentialMap::setPotential(uint32_t cellX, uint32_t cellY, uint32_t weight)
{
	m_map[calculateGridIndex(cellX, cellY)] = weight;

	if (cellX > 0) setPotential(cellX - 1, cellY, weight + 1);
	if (cellY > 0) setPotential(cellX, cellY - 1, weight + 1);
	if (cellX < m_cellsHorCount - 1) setPotential(cellX + 1, cellY, weight + 1);
	if (cellY < m_cellsVertCount - 1) setPotential(cellX, cellY + 1, weight + 1);
}

uint32_t * PotentialMap::map()
{
	return m_map;
}

uint32_t PotentialMap::cellsHorCount() const
{
	return m_cellsHorCount;
}

uint32_t PotentialMap::cellsVertCount() const
{
	return m_cellsVertCount;
}

inline void PotentialMap::calculateGridCoordinates(uint32_t & cellX, uint32_t & cellY, const Coordinate2D & position)
{
	cellX = static_cast<uint32_t>((position.x + m_worldCenter.x) / m_cellWidth);
	cellY = static_cast<uint32_t>((position.y + m_worldCenter.y) / m_cellHeight);
}

inline uint64_t PotentialMap::calculateGridIndex(uint32_t cellX, uint32_t cellY) const
{
	return cellY * m_cellsHorCount + cellX;
}
