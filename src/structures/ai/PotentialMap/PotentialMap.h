#pragma once

#include "primitives/Coordinate2D.h"
#include "primitives/Rectangle.h"
#include "pools/IndexedListNode.h"
#include "pools/IndexedListNodePool.h"

class PotentialMap
{
public:
	PotentialMap(uint32_t maxCapacity, uint32_t cellWidth, uint32_t cellHeight, uint32_t worldWidth, uint32_t worldHeight);
	~PotentialMap();

	//void putToGrid(const Coordinate2D &position, uint32_t object);
	//void removeFromGrid(const Coordinate2D &position, uint32_t object);
	//void move(const Coordinate2D &oldPosition, const Coordinate2D &newPosition, uint32_t object);

	void setTarget(const Coordinate2D &target);

	uint32_t * map();

	uint32_t cellsHorCount() const;
	uint32_t cellsVertCount() const;

protected:
	uint32_t m_cellWidth, m_cellHeight;
	uint32_t m_worldWidth, m_worldHeight;
	Coordinate2D m_worldCenter;
	uint32_t m_cellsHorCount, m_cellsVertCount;

	uint32_t * m_map;

	Coordinate2D m_target;

protected:
	inline void calculateGridCoordinates(uint32_t &cellX, uint32_t &cellY, const Coordinate2D &position);
	inline uint64_t calculateGridIndex(uint32_t cellX, uint32_t cellY) const;
	inline void setPotential(uint32_t cellX, uint32_t cellY, uint32_t weight = 0);
};