/*
 * Author: Mykola Shatokhin
 * Date: 11 Sep 2019
 */

#pragma once

#include <algorithm>
#include <cassert>
#include <cstdint>

// parameter class should provide function void onDestroying() called on destruction
template <class T>
class LoopBuffer
{
protected:
    uint32_t m_activeCount;
    
    uint32_t m_maxCapacity;
    T * m_objects;
    
    uint32_t * m_externalIndices;
    uint32_t * m_internalIndices;
    uint32_t * m_nextIndices;
    uint32_t * m_prevIndices;
    
    uint32_t m_firstActiveObject;
    uint32_t m_lastActiveObject;
    uint32_t m_firstFree;
    uint32_t m_head;
    uint32_t m_tail;
    
public:
    LoopBuffer(uint32_t maxCapacity) :
        m_activeCount(0),
        m_maxCapacity(maxCapacity),
        m_firstActiveObject(0),
        m_lastActiveObject(0),
        m_firstFree(0),
        m_head(0),
        m_tail(0)
    {
        m_objects = new T[m_maxCapacity];
        m_externalIndices = new uint32_t[m_maxCapacity];
        m_internalIndices = new uint32_t[m_maxCapacity];
        m_nextIndices = new uint32_t[maxCapacity];
        m_prevIndices = new uint32_t[maxCapacity];

        for(uint32_t i=0;i<m_maxCapacity;i++)
        {
            m_externalIndices[i] = i;
            m_internalIndices[i] = i;
        }
    }
    
    ~LoopBuffer()
    {
        delete[] m_objects;
        delete[] m_externalIndices;
        delete[] m_internalIndices;
        delete[] m_nextIndices;
        delete[] m_prevIndices;
    }
    

    T * getObject(uint32_t objectIndex)
    {
        assert(objectIndex < m_maxCapacity);
        
        uint32_t internalIndex = m_internalIndices[objectIndex];

        assert(internalIndex <= m_activeCount);
        
        return &m_objects[internalIndex];
    }
    
    uint32_t activeCount() const
    {
        return m_activeCount;
    }
        
    uint32_t createObject()
    {
        if(m_activeCount >= m_maxCapacity)
        {
            uint32_t nextHead = m_nextIndices[m_internalIndices[m_head]];
            
            destroyObject(m_head);
            
            m_head = nextHead;
        }

        m_lastActiveObject = m_firstFree;
        m_firstFree++;
        
        uint32_t objectIndex = m_externalIndices[m_lastActiveObject];

       if(m_activeCount == 0)
        {
            m_head = objectIndex;
            m_tail = objectIndex;
        }
        else
        {
            m_nextIndices[m_internalIndices[m_tail]] = objectIndex;
            m_prevIndices[m_internalIndices[objectIndex]] = m_tail;
            m_tail = objectIndex;
        }
        
        m_activeCount++;

        return objectIndex;
    }
    

    void destroyObject(uint32_t objectIndex)
    {
        assert(m_activeCount > 0 && objectIndex < m_maxCapacity);
    
        uint32_t internalIndex = m_internalIndices[objectIndex];
    
        assert(internalIndex >= m_firstActiveObject && internalIndex <= m_lastActiveObject);

		m_objects[internalIndex].onDestroying();

        if(m_activeCount > 1)
        {
            uint32_t prev = m_prevIndices[internalIndex];
            uint32_t next = m_nextIndices[internalIndex];

            if(objectIndex == m_head)
            {
                m_head = m_nextIndices[m_internalIndices[m_head]];
            }
            else
            {
                m_nextIndices[m_internalIndices[prev]] = next;
            }

            if(objectIndex == m_tail)
            {
                m_tail = m_prevIndices[m_internalIndices[m_tail]];
            }
            else
            {
                m_prevIndices[m_internalIndices[next]] = prev;
            }
        }
        
        if(internalIndex != m_lastActiveObject)
        {
            exchangeObjects(internalIndex, m_lastActiveObject);
        }

        m_lastActiveObject--;
        m_firstFree--;
        m_activeCount--;
    }

    T * objects()
    {
        return m_objects;
    }

protected:
    inline void exchangeObjects(uint32_t object1, uint32_t object2)
    {
		std::swap(m_objects[object1], m_objects[object2]);
        std::swap(m_nextIndices[object1], m_nextIndices[object2]);
        std::swap(m_prevIndices[object1], m_prevIndices[object2]);
        std::swap(m_internalIndices[m_externalIndices[object1]], m_internalIndices[m_externalIndices[object2]]);
        std::swap(m_externalIndices[object1], m_externalIndices[object2]);
    }
};
