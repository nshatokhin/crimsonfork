/*
 * Author: Mykola Shatokhin
 * Date: 04 Sep 2019
 */

#pragma once

#include <algorithm>
#include <cassert>
#include <cstdint>

template <typename T>
class ObjectPool
{
protected:
    uint32_t m_activeCount;
    
    uint32_t m_maxCapacity;
    T * m_objects;
    
    uint32_t * m_externalIndices;
    uint32_t * m_internalIndices;
    
    uint32_t m_firstActiveObject;
    uint32_t m_lastActiveObject;
    uint32_t m_firstFree;
    
public:
    ObjectPool(uint32_t maxCapacity) :
        m_activeCount(0),
        m_maxCapacity(maxCapacity),
        m_firstActiveObject(0),
        m_lastActiveObject(0),
        m_firstFree(0)
    {
		assert(m_maxCapacity > 0);

        m_objects = new T[m_maxCapacity];
        m_externalIndices = new uint32_t[m_maxCapacity];
        m_internalIndices = new uint32_t[m_maxCapacity];

        for(uint32_t i=0;i<m_maxCapacity;i++)
        {
            m_externalIndices[i] = i;
            m_internalIndices[i] = i;
        }
    }
    
    ~ObjectPool()
    {
        delete[] m_objects;
        delete[] m_externalIndices;
        delete[] m_internalIndices;
    }
    

    T * getObject(uint32_t objectIndex)
    {
        assert(m_activeCount > 0 && objectIndex < m_maxCapacity);
        
        uint32_t internalIndex = m_internalIndices[objectIndex];

        assert(internalIndex < m_activeCount);
        
        return &m_objects[internalIndex];
    }
    
    uint32_t activeCount() const
    {
        return m_activeCount;
    }
        
    uint32_t createObject()
    {
		assert(m_activeCount < m_maxCapacity);

        m_lastActiveObject = m_firstFree;
        m_firstFree++;

        uint32_t objectIndex = m_externalIndices[m_lastActiveObject];

        m_activeCount++;

        return objectIndex;
    }
    

    void destroyObject(uint32_t objectIndex)
    {
        assert(m_activeCount > 0 && objectIndex < m_maxCapacity);
    
        uint32_t internalIndex = m_internalIndices[objectIndex];
    
        assert(internalIndex >= m_firstActiveObject && internalIndex <= m_lastActiveObject);

        if(internalIndex != m_lastActiveObject)
        {
            exchangeObjects(internalIndex, m_lastActiveObject);
        }

        m_lastActiveObject--;
        m_firstFree--;
        m_activeCount--;
    }

    T * objects()
    {
        return m_objects;
    }

	void clear()
	{
		m_activeCount = 0;
		m_firstActiveObject = 0;
		m_lastActiveObject = 0;
		m_firstFree = 0;
	}

protected:
    inline void exchangeObjects(uint32_t object1, uint32_t object2)
    {
        std::swap(m_objects[object1], m_objects[object2]);
        std::swap(m_internalIndices[m_externalIndices[object1]], m_internalIndices[m_externalIndices[object2]]);
        std::swap(m_externalIndices[object1], m_externalIndices[object2]);
    }
};
