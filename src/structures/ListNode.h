#pragma once

#include <cstdint>

template <typename T>
class ListNode
{
public:
	ListNode() :
		next(-1),
		prev(-1)
	{}

	int64_t next;
	int64_t prev;
	T value;
};