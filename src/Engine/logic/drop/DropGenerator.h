#pragma once

#include "helpers/RandomGenerator.h"

#include <vector>

class DropGenerator
{
public:
	DropGenerator() : m_itemsCount(0), m_randomSpaceSize(0) {}

	void clear()
	{
		m_chances.clear();
		m_itemsCount = 0;
		m_randomSpaceSize = 0;
	}

	void add(uint32_t itemId, uint32_t dropChance)
	{
		m_randomSpaceSize += dropChance;
		m_chances.push_back(m_randomSpaceSize);
		m_indices.push_back(itemId);
		m_itemsCount++;
	}

	uint32_t generate()
	{
		uint32_t drop = RandomGenerator::generate(0, m_randomSpaceSize);

		uint32_t i = 0;
		while (i < m_itemsCount && m_chances[i] < drop) { i++; }

		if (i < m_itemsCount)
		{
			return m_indices[i];
		}
		else
		{
			return std::numeric_limits<uint32_t>::max();
		}
	}

protected:
	std::vector<uint32_t> m_chances;
	std::vector<uint32_t> m_indices;
	uint32_t m_itemsCount;
	uint32_t m_randomSpaceSize;
};
