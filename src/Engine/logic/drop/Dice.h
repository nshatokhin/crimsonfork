#pragma once

#include "helpers/RandomGenerator.h"
#include "helpers/StringParse.h"

#include <string>

class Dice
{
	static uint32_t roll(uint32_t count = 1, uint32_t size = 6)
	{
		uint32_t dropSum = 0;

		for (uint32_t i = 0; i < count; i++)
		{
			dropSum += RandomGenerator::generate(1, size);
		}

		return dropSum;
	}

	static uint32_t roll(std::string notation)
	{
		uint32_t count, size;

		if (parseDiceNotation(count, size, notation))
		{
			return roll(count, size);
		}
		else
		{
			return 0;
		}
	}
};
