#pragma once

#include "helpers/RandomGenerator.h"

class ChanceChecker
{
public:
	uint32_t randomSpaceSize;

public:
	ChanceChecker(uint32_t randomSpaceSize = 1000) : randomSpaceSize(randomSpaceSize) {}

	bool check(double probability)
	{
		uint32_t chanceRange = probability * randomSpaceSize;

		return RandomGenerator::generate(0, randomSpaceSize) < chanceRange;
	}
};
