#pragma once

#include "helpers/RandomGenerator.h"
#include "pools/ThreadPool.h"
#include "primitives/Rectangle.h"
#include "services/ConfigLocator.h"
#include "services/LoggerLocator.h"
#include "services/config/ConfigService.h"
#include "services/logger/LoggerService.h"

#include <cstdint>
#include <string>

#include "SDL.h"

typedef SDL_Texture Sprite;

Sprite* loadSprite(std::string path, int32_t &width, int32_t &height);
void drawSprite(Sprite * sprite, int32_t x, int32_t y, int32_t width, int32_t height);
void drawSpriteRotated(Sprite * sprite, int32_t x, int32_t y, int32_t width, int32_t height, double andgle, int32_t rotCX, int32_t rotCY, bool flipVertical = false, bool flipHorizontal = false);
void destroySprite(Sprite * sprite);

void showCursor(bool show);

void drawRectangle(int32_t x, int32_t y, uint32_t width, uint32_t height, uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255);
void fillRectangle(int32_t x, int32_t y, uint32_t width, uint32_t height, uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255);
void drawLine(int32_t x1, int32_t y1, int32_t x2, int32_t y2, uint8_t r, uint8_t g, uint8_t b, uint8_t a);

class IFramework
{
public:
	enum class ErrorCode : int32_t
	{
		SUCCESS = 0,
		INIT_FAILED
	};

public:
	virtual ~IFramework() {}

	virtual void PreInit(std::string &windowTitle, uint32_t &screenWidth, uint32_t &screenHeight, bool &fullscreen) = 0;

	virtual bool Init() = 0;

	virtual void Close() = 0;

	virtual bool update(double dt) = 0;

	virtual void render() = 0;

	virtual double timePerUpdate() const = 0;

	virtual void onMouseMove(int32_t x, int32_t y, int32_t xrelative, int32_t yrelative) = 0;

	virtual void onMouseButtonClick(uint8_t button, bool isReleased, uint8_t clicks, int32_t x, int32_t y) = 0;

	virtual void onKeyPressed(SDL_Keycode k) = 0;

	virtual void onKeyReleased(SDL_Keycode k) = 0;

	virtual void onJoyButtonPressed(SDL_JoystickID joyId, uint8_t k) = 0;

	virtual void onJoyButtonReleased(SDL_JoystickID joyId, uint8_t k) = 0;

	virtual void onJoyAxisMotion(SDL_JoystickID joyId, uint8_t axis, int16_t value) = 0;

	virtual void onJoyBallMotion(SDL_JoystickID joyId, uint8_t ball, int16_t xrel, int16_t yrel) = 0;

	virtual void onJoyHatMotion(SDL_JoystickID joyId, uint8_t hat, uint8_t value) = 0;
};

class Framework : public IFramework
{
public:
	Framework(const std::string windowTitle = std::string("Engine"), uint32_t maxThreadCount = 4, uint32_t width = 640, uint32_t height = 480, bool fullscreen = false);
	virtual ~Framework();

	virtual void PreInit(std::string &windowTitle, uint32_t &screenWidth, uint32_t &screenHeight, bool &fullscreen) override;

	virtual bool Init() override;

	virtual void Close() override;

	virtual bool update(double dt) override;

	virtual void render() override;

	virtual double timePerUpdate() const override;

	virtual void onMouseMove(int32_t x, int32_t y, int32_t xrelative, int32_t yrelative) override;

	virtual void onMouseButtonClick(uint8_t button, bool isReleased, uint8_t clicks, int32_t x, int32_t y) override;

	virtual void onKeyPressed(SDL_Keycode k) override;

	virtual void onKeyReleased(SDL_Keycode k) override;

	virtual void onJoyButtonPressed(SDL_JoystickID joyId, uint8_t button) override;

	virtual void onJoyButtonReleased(SDL_JoystickID joyId, uint8_t button) override;

	virtual void onJoyAxisMotion(SDL_JoystickID joyId, uint8_t axis, int16_t value) override;

	virtual void onJoyBallMotion(SDL_JoystickID joyId, uint8_t ball, int16_t xrel, int16_t yrel) override;

	virtual void onJoyHatMotion(SDL_JoystickID joyId, uint8_t hat, uint8_t value) override;

protected:
	static constexpr double TIME_PER_UPDATE = 1.0 / 60; // 60 FPS

	ConfigService &m_configService;
	LoggerService &m_loggerService;

	std::unique_ptr<ThreadPool> m_threadPool;

	std::string m_windowTitle;

	uint32_t m_windowWidth;
	uint32_t m_windowHeight;
	bool m_fullscreen, m_exit;

};

int32_t run(IFramework * framework);