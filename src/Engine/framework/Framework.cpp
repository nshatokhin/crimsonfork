#include "Framework.h"

#include "services/ThreadPoolLocator.h"
#include "time/Time.h"

#include "SDL_image.h"

#include <string>

//The window we'll be rendering to
SDL_Window* window = nullptr;

// Screen renderer
SDL_Renderer * renderer = nullptr;

Sprite* loadSprite(std::string path, int32_t &width, int32_t &height)
{
	if (renderer == nullptr) return nullptr;

	Sprite * texture = nullptr;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
	}
	else
	{
		width = loadedSurface->w;
		height = loadedSurface->h;

		texture = SDL_CreateTextureFromSurface(renderer, loadedSurface);

		if (texture == NULL)
		{
			printf("Unable to create texture for image %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	return texture;
}

void drawSprite(Sprite * sprite, int32_t x, int32_t y, int32_t width, int32_t height)
{
	if (!renderer) return;

	SDL_Rect stretchRect;
	stretchRect.x = x;
	stretchRect.y = y;
	stretchRect.w = width;
	stretchRect.h = height;

	SDL_RenderCopy(renderer, sprite, nullptr, &stretchRect);
}

void drawSpriteRotated(Sprite * sprite, int32_t x, int32_t y, int32_t width, int32_t height, double angle, int32_t rotCX, int32_t rotCY, bool flipVertical, bool flipHorizontal)
{
	if (!renderer) return;

	SDL_Rect stretchRect;
	stretchRect.x = x;
	stretchRect.y = y;
	stretchRect.w = width;
	stretchRect.h = height;

	SDL_Point rotationCenterPoint = {rotCX, rotCY};

	SDL_RendererFlip flip;

	if (flipVertical && flipHorizontal)
	{
		flip = static_cast<SDL_RendererFlip>(SDL_FLIP_VERTICAL | SDL_FLIP_HORIZONTAL);
	}
	else if (flipVertical)
	{
		flip = static_cast<SDL_RendererFlip>(SDL_FLIP_VERTICAL);
	}
	else if (flipHorizontal)
	{
		flip = static_cast<SDL_RendererFlip>(SDL_FLIP_HORIZONTAL);
	}
	else
	{
		flip = static_cast<SDL_RendererFlip>(SDL_FLIP_NONE);
	}

	SDL_RenderCopyEx(renderer, sprite, nullptr, &stretchRect, angle, &rotationCenterPoint, flip);
}

void destroySprite(Sprite * sprite)
{
	SDL_DestroyTexture(sprite);
}

void showCursor(bool show)
{
	SDL_ShowCursor(show ? SDL_ENABLE : SDL_DISABLE);
}

void drawRectangle(int32_t x, int32_t y, uint32_t width, uint32_t height, uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	SDL_Rect rectangle;
	rectangle.x = x;
	rectangle.y = y;
	rectangle.w = width;
	rectangle.h = height;

	SDL_SetRenderDrawColor(renderer, r, g, b, a);
	SDL_RenderDrawRect(renderer, &rectangle);
}

void fillRectangle(int32_t x, int32_t y, uint32_t width, uint32_t height, uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	SDL_Rect rectangle;
	rectangle.x = x;
	rectangle.y = y;
	rectangle.w = width;
	rectangle.h = height;

	SDL_SetRenderDrawColor(renderer, r, g, b, a);
	SDL_RenderFillRect(renderer, &rectangle);
}

void drawLine(int32_t x1, int32_t y1, int32_t x2, int32_t y2, uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	SDL_SetRenderDrawColor(renderer, r, g, b, a);
	SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
}

Framework::Framework(const std::string windowTitle, uint32_t maxThreadCount, uint32_t width, uint32_t height, bool fullscreen) :
	m_configService(*ConfigLocator::locate()),
	m_loggerService(*LoggerLocator::locate()),
	m_threadPool(std::unique_ptr<ThreadPool>(new ThreadPool(maxThreadCount))),
	m_windowTitle(windowTitle),
	m_windowWidth(width),
	m_windowHeight(height),
	m_fullscreen(fullscreen),
	m_exit(false)
{
	ThreadPoolLocator::provide(m_threadPool.get());
	RandomGenerator::init();
}

Framework::~Framework()
{

}

void Framework::PreInit(std::string &windowTitle, uint32_t & screenWidth, uint32_t & screenHeight, bool & fullscreen)
{
	windowTitle = m_windowTitle;
	screenWidth = m_windowWidth;
	screenHeight = m_windowHeight;
	fullscreen = m_fullscreen;
}

bool Framework::Init()
{
	return true;
}

void Framework::Close()
{

}

bool Framework::update(double dt)
{
	return true;
}

void Framework::render()
{
}

double Framework::timePerUpdate() const
{
	return TIME_PER_UPDATE;
}

void Framework::onMouseMove(int32_t x, int32_t y, int32_t xrelative, int32_t yrelative)
{
}

void Framework::onMouseButtonClick(uint8_t button, bool isReleased, uint8_t clicks, int32_t x, int32_t y)
{
}

void Framework::onKeyPressed(SDL_Keycode k)
{
}

void Framework::onKeyReleased(SDL_Keycode k)
{
}

void Framework::onJoyButtonPressed(SDL_JoystickID joyId, uint8_t button)
{
}

void Framework::onJoyButtonReleased(SDL_JoystickID joyId, uint8_t button)
{
}

void Framework::onJoyAxisMotion(SDL_JoystickID joyId, uint8_t axis, int16_t value)
{
}

void Framework::onJoyBallMotion(SDL_JoystickID joyId, uint8_t ball, int16_t xrel, int16_t yrel)
{
}

void Framework::onJoyHatMotion(SDL_JoystickID joyId, uint8_t hat, uint8_t value)
{
}

int32_t run(IFramework * framework)
{
	//Initialization flag
	bool success = true;

	// TODO: move instatiation to framework core
	assert(LoggerLocator::locate());
	LoggerService &loggerService = *LoggerLocator::locate();

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		loggerService.logFatal(std::string("SDL could not initialize! SDL_Error: ").append(SDL_GetError()));
		success = false;
	}
	else
	{
		std::string windowTitle;
		uint32_t windowWidth, windowHeight;
		bool fullscreen;

		framework->PreInit(windowTitle, windowWidth, windowHeight, fullscreen);

		//Initialize PNG loading
		int imgFlags = IMG_INIT_PNG;
		if (!(IMG_Init(imgFlags) & imgFlags))
		{
			loggerService.logFatal(std::string("SDL_image could not initialize! SDL_image Error: ").append(SDL_GetError()));
			success = false;
		}
		else
		{
			//Create window
			window = SDL_CreateWindow(windowTitle.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowWidth, windowHeight, SDL_WINDOW_SHOWN);

			if (fullscreen)
			{
				SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN);
			}
			else
			{
				SDL_SetWindowFullscreen(window, 0);
			}

			if (window == NULL)
			{
				loggerService.logFatal(std::string("Window could not be created! SDL_Error: ").append(SDL_GetError()));
				success = false;
			}
			else
			{
				//Get window surface
				//m_screenSurface = SDL_GetWindowSurface(m_window);
				renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
			}
		}
	}

	if (!success || !framework->Init())
	{
		return static_cast<int32_t>(IFramework::ErrorCode::INIT_FAILED);
	}

	SDL_Event e;

	// game loop
	timestamp_t previousTimestamp = Time::getTimestamp();
	bool exit = false;
	double lag = 0, timePerUpdate = framework->timePerUpdate();

	while (!exit)
	{
		timestamp_t currentTimestamp = Time::getTimestamp();
		double elapsed = Time::timeDifference(currentTimestamp, previousTimestamp);
		previousTimestamp = currentTimestamp;

		lag += elapsed;

		while (lag >= timePerUpdate)
		{
			//Handle events on queue
			while (SDL_PollEvent(&e) != 0)
			{
				//User requests quit
				if (e.type == SDL_QUIT || e.type == SDL_WINDOWEVENT_CLOSE)
				{
					exit = true;
				}
				// Keyboard events
				else if (e.type == SDL_KEYUP && e.key.repeat == 0)
				{
					framework->onKeyReleased(e.key.keysym.sym);
				}
				else if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
				{
					framework->onKeyPressed(e.key.keysym.sym);
				}
				// Mouse events
				else if (e.type == SDL_MOUSEMOTION)
				{
					framework->onMouseMove(e.motion.x, e.motion.y, e.motion.xrel, e.motion.yrel);
				}
				else if (e.type == SDL_MOUSEBUTTONDOWN)
				{
					framework->onMouseButtonClick(e.button.button, false, e.button.clicks, e.button.x, e.button.y);
				}
				else if (e.type == SDL_MOUSEBUTTONUP)
				{
					framework->onMouseButtonClick(e.button.button, true, e.button.clicks, e.button.x, e.button.y);
				}
				else if (e.type == SDL_JOYBUTTONDOWN)
				{
					framework->onJoyButtonPressed(e.jbutton.which, e.jbutton.button);
				}
				else if (e.type == SDL_JOYBUTTONUP)
				{
					framework->onJoyButtonReleased(e.jbutton.which, e.jbutton.button);
				}
				else if (e.type == SDL_JOYAXISMOTION)
				{
					framework->onJoyAxisMotion(e.jaxis.which, e.jaxis.axis, e.jaxis.value);
				}
				else if (e.type == SDL_JOYBALLMOTION)
				{
					framework->onJoyBallMotion(e.jball.which, e.jball.ball, e.jball.xrel, e.jball.yrel);
				}
				else if (e.type == SDL_JOYHATMOTION)
				{
					framework->onJoyAxisMotion(e.jhat.which, e.jhat.hat, e.jhat.value);
				}
			}

			if (exit)
				break;

			exit = framework->update(timePerUpdate);
			lag -= timePerUpdate;
		}

		SDL_RenderClear(renderer);
		//SDL_SetRenderDrawColor(renderer, 128, 128, 128, 255);
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

		framework->render();

		SDL_RenderPresent(renderer);

		Time::sleep(timePerUpdate - Time::timeDifference(Time::getTimestamp(), previousTimestamp));

	}

	framework->Close();
	delete framework;

	// unload the dynamically loaded image libraries
	IMG_Quit();

	// Destroy renderer
	SDL_DestroyRenderer(renderer);

	//Destroy window
	SDL_DestroyWindow(window);
	window = NULL;

	//Quit SDL subsystems
	SDL_Quit();

	return static_cast<int32_t>(IFramework::ErrorCode::SUCCESS);
}
