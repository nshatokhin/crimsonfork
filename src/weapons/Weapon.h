#pragma once

#include "common/Common.h"
#include "helpers/ProgressCalculator.h"
#include "primitives/Coordinate2D.h"
#include "primitives/Vector2D.h"
#include "services/AnimationPoolLocator.h"
#include "services/BulletsPoolLocator.h"
#include "services/PhysicsPoolLocator.h"
#include "services/SoundLocator.h"
#include "time/BinaryTimer.h"

#include <iostream>
#include <functional>

enum class WeaponType : uint8_t
{
	CANNON = 0,
	COUNT
};

struct WeaponParams
{
	uint32_t id;
	WeaponType type;
	uint32_t bulletSpeed;
	uint32_t bulletHitmapRadius;
	uint32_t ammoCount;
	uint32_t weaponAnimationStates;
	WeaponAnimation weaponAnimation[GENERAL_ANIMATION_STATES];
	Coordinate2D bulletSpawnPoint;
	uint32_t bulletAnimationStates;
	BulletAnimation bulletAnimation[GENERAL_ANIMATION_STATES];
	double shootCooldownDelay;
	double reloadDelay;
	uint32_t shootSound;
	uint32_t reloadSound;
	uint32_t reloadCompleteSound;
	uint32_t noAmmoSound;
	bool automatic;
	bool targeted;
};

class Weapon
{
public:
	Weapon() :
		baseWeaponOrientation(0, -1),
		weaponOrientation(0, -1),
		ammoLoadedCount(0),
		m_active(false)
	{
		m_shootingTimer.setInterval(0);
		m_reloadTimer.setInterval(0);
	}

	~Weapon()
	{
		m_shootingTimer.setInterval(0);
		m_reloadTimer.setInterval(0);
	}

	void Init(uint32_t animComp, const WeaponParams& params)
	{
		animationComponent = animComp;
		type = params.type;
		bulletSpeed = params.bulletSpeed;
		bulletHitmapRadius = params.bulletHitmapRadius;
		ammoCount = params.ammoCount;
		ammoLeft = ammoCount;
		bulletSpawnPoint = params.bulletSpawnPoint;

		bulletAnimationStates = params.bulletAnimationStates;

		for (uint32_t i = 0; i < bulletAnimationStates; i++)
		{
			bulletAnimation[i] = params.bulletAnimation[i];
		}

		shootCooldownDelay = params.shootCooldownDelay;
		reloadDelay = params.reloadDelay;
		shootSound = params.shootSound;
		reloadSound = params.reloadSound;
		reloadCompleteSound = params.reloadCompleteSound;
		noAmmoSound = params.noAmmoSound;
		shootCooldownActive = false;
		reloadActive = false;
		baseWeaponOrientation = Vector2D(0, -1);
		weaponOrientation = Vector2D(0, -1);
		m_shootingTimer.setInterval(0);
		m_reloadTimer.setInterval(0);
		ammoLoadedCount = ammoLeft;
		automatic = params.automatic;
		targeted = params.targeted;
	}

	void Init(uint32_t animComp, size_t weaponId)
	{
		Init(animComp, weaponParams[weaponId]);
	}

	Coordinate2D getBulletSpawnPoint(const Coordinate2D &position) const
	{
		return (position + bulletSpawnPoint).rotate(position, weaponOrientation.rotationAngle(baseWeaponOrientation));
	}

	void aim(const Coordinate2D &playerPos, const Coordinate2D &target)
	{
		weaponOrientation = Vector2D(playerPos, target).unitVector();
	}

	void reload()
	{
		ammoLeft = ammoCount;
	}

	void pullTrigger()
	{
		m_active = true;

		spawnBullet();
	}

	void releaseTriger()
	{
		m_active = false;
	}

	void update(double dt)
	{
		m_shootingTimer.update(dt);
		m_reloadTimer.update(dt);

		if (reloadActive)
		{
			uint32_t currentAmmoCount = static_cast<uint32_t>(ProgressCalculator::progress(m_reloadTimer.value(), 0, reloadDelay) * ammoCount);

			if (ammoLoadedCount != currentAmmoCount)
			{
				ammoLoadedCount = currentAmmoCount;
				SoundLocator::locate()->playSound(reloadSound);
			}
		}
		else
		{
			ammoLoadedCount = ammoLeft;
		}

		if (m_shootingTimer.fired)
		{
			m_shootingTimer.fired = false;
			shootingTimeoutHandler();
		}

		if (m_reloadTimer.fired)
		{
			m_reloadTimer.fired = false;
			reloadTimeoutHandler();
		}
	}

protected:
	void spawnBullet()
	{
		if (shootCooldownActive) return;

		SoundService &sound = *SoundLocator::locate();

		if (reloadActive)
		{
			sound.playSound(noAmmoSound);
			return;
		}

		GeneralAnimationPool &animation = *AnimationPoolLocator::locate();
		BulletsPool &bullets = *BulletsPoolLocator::locate();
		PhysicsPool &physics = *PhysicsPoolLocator::locate();

		Coordinate2D playerPos = physics.getObject(ownerPhysicsComponent)->position();

		// Bullet creation will destroy oldest bullet and free space for physicsComponent and animationComponent
		// So, we need to create bullet before it physicsComponent and animationComponent
		uint32_t bulletId = bullets.createBullet(bulletSpeed, 0, 0);
		uint32_t animationId = animation.createAnimation(bulletAnimationStates, bulletAnimation);
		uint32_t physicsId = physics.createPhysicsComponent(EntityType::BULLET, getBulletSpawnPoint(playerPos), weaponOrientation * bulletSpeed, bulletHitmapRadius, 1, false); // TODO: load mass from config

		// Connecting animationComponent and physicsComponent to bullet entity
		BulletEntity &bullet = *bullets.getObject(bulletId);
		bullet.animationComponent = animationId;
		bullet.physicsComponent = physicsId;

		physics.getObject(physicsId)->entityId = bulletId;

		sound.playSound(shootSound);

		shootCooldownActive = true;
		m_shootingTimer.setInterval(shootCooldownDelay);

		if (ammoCount > 0)
		{
			ammoLeft--;

			if (ammoLeft <= 0)
			{
				reloadActive = true;
				m_reloadTimer.setInterval(reloadDelay);
				ammoLoadedCount = 0;
			}
		}
	}

	void destroyBullet(uint32_t bulletId)
	{
		BulletsPoolLocator::locate()->destroyBullet(bulletId);
	}

	void shootingTimeoutHandler()
	{
		shootCooldownActive = false;
		m_shootingTimer.setInterval(0);

		if (automatic && m_active)
		{
			spawnBullet();
		}
	}

	void reloadTimeoutHandler()
	{
		reloadActive = false;
		m_reloadTimer.setInterval(0);
		reload();
		SoundLocator::locate()->playSound(reloadCompleteSound);
	}

public:
	uint32_t ownerPhysicsComponent;
	uint32_t animationComponent;
	WeaponType type;
	uint32_t bulletSpeed;
	uint32_t bulletHitmapRadius;
	uint32_t ammoCount;
	uint32_t ammoLeft;
	Coordinate2D bulletSpawnPoint;
	Vector2D baseWeaponOrientation;
	Vector2D weaponOrientation;
	uint32_t bulletAnimationStates;
	BulletAnimation bulletAnimation[GENERAL_ANIMATION_STATES];
	double shootCooldownDelay;
	double reloadDelay;
	uint32_t shootSound;
	uint32_t reloadSound;
	uint32_t reloadCompleteSound;
	uint32_t noAmmoSound;
	bool shootCooldownActive;
	bool reloadActive;
	uint32_t ammoLoadedCount;
	bool automatic;
	bool targeted;

protected:
	bool m_active;
	BinaryTimer m_shootingTimer;
	BinaryTimer m_reloadTimer;

public:
	static std::vector<WeaponParams> weaponParams;

public:
	static Weapon CreateWeapon(size_t weaponId)
	{
		Weapon weapon;

		GeneralAnimationPool &pool = *AnimationPoolLocator::locate();
		WeaponParams &params = weaponParams[weaponId];

		uint32_t animationComponent = pool.createAnimation(params.weaponAnimationStates, params.weaponAnimation);

		weapon.Init(animationComponent, weaponId);

		return weapon;
	}

	static void DestroyWeapon(const Weapon& weapon)
	{
		GeneralAnimationPool &pool = *AnimationPoolLocator::locate();

		pool.destroyObject(weapon.animationComponent);
	}

};