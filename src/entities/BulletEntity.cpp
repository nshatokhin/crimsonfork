#include "BulletEntity.h"

#include "services/AnimationPoolLocator.h"
#include "services/PhysicsPoolLocator.h"

#include <iostream>

BulletEntity::BulletEntity() : GameEntity()
{
}

void BulletEntity::onDestroying()
{
	GeneralAnimationPool*ap = AnimationPoolLocator::locate();
	ap->destroyObject(animationComponent);
	PhysicsPool *pp = PhysicsPoolLocator::locate();
	pp->destroyPhysicsComponent(physicsComponent);
}
