#pragma once

#include "GameEntity.h"

class BulletEntity : public GameEntity
{
public:
	BulletEntity();

	void onDestroying();
};