#include "GameEntity.h"

GameEntity::GameEntity()
{
}

GameEntity::~GameEntity()
{
}

void GameEntity::init(uint32_t speedValue, uint32_t physics, uint32_t animation,
	bool staticEnt, bool undestruct, double maxHP)
{
	maxSpeed = speedValue;
	physicsComponent = physics;
	animationComponent = animation;
	staticEntity = staticEnt;
	undestructable = undestruct;
	maxHealth = maxHP;
	health = maxHealth;
}
