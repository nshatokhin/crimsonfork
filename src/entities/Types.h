#pragma once

#include <cstdint>

enum class EntityType : uint8_t
{
	PLAYER = 0,
	ENEMY,
	BULLET,
	BONUS
};
