#pragma once

#include "GameEntity.h"

#include "ai/PathPlanner/PathPlanner.h"
#include "common/Common.h"
#include "primitives/Vector2D.h"

class Graph;
class OccupationPartition;

enum class Steering : uint8_t
{
	SEEK = 0,
	FLEE,
	ARRIVE,
	PURSUIT,
	EVADE,
	PATH_FOLLOW
};

class EnemyParams
{
public:
	EnemyParams() {}

public:
	uint32_t maxSpeed;
	uint32_t hitmapRadius;
	uint32_t animationStatesCount;
	EnemyAnimation animationStates[GENERAL_ANIMATION_STATES];
	uint32_t deathSound;
	double mass;
	double maxHP;
};

class EnemyEntity : public GameEntity
{

public:
	EnemyEntity(Graph * graph = nullptr, OccupationPartition * occupationGrid = nullptr);

	void init(Graph * graph, OccupationPartition * occupationGrid, uint32_t speedValue, uint32_t physicsComponent, uint32_t animationComponent, double maxHP);

public:
	static constexpr uint32_t FEELERS_COUNT = 3;

	Steering currentSteering;
	Coordinate2D target, currentTarget;
	double maximumForce;
	bool wallAvoidance, obstacleAvoidance, separation;

	Vector2D feelers[FEELERS_COUNT];

	PathPlanner pathPlannner;
	std::list<Coordinate2D> path;

protected:
	OccupationPartition * m_occupationGrid;
};