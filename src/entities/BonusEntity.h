#pragma once

#include "GameEntity.h"

#include "components/Modifier.h"

class BonusEntity : public GameEntity
{
public:
	BonusEntity();

public:
	Modifier modifier;
};