#include "EnemyEntity.h"

EnemyEntity::EnemyEntity(Graph * graph, OccupationPartition * occupationGrid) : GameEntity(),
	m_occupationGrid(occupationGrid),
	pathPlannner(graph, occupationGrid)
{
    currentSteering = Steering::ARRIVE;
	maximumForce = 1000; // TODO: load from config
	wallAvoidance = true;
	obstacleAvoidance = true;
	separation = true;

	// TODO: load from config
	feelers[0] = Vector2D(20, 40);
	feelers[1] = Vector2D(60, 0);
	feelers[2] = Vector2D(20, -40);
}

void EnemyEntity::init(Graph * graph, OccupationPartition * occupationGrid, uint32_t speedValue, uint32_t physicsComponent, uint32_t animationComponent, double maxHP)
{
	m_occupationGrid = occupationGrid;
	pathPlannner.init(graph, occupationGrid);
	GameEntity::init(speedValue, physicsComponent, animationComponent, false, false, maxHP);
}
