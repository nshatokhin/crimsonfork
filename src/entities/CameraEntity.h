#pragma once

#include "primitives/Coordinate2D.h"
#include "primitives/CoordinateScreen.h"

class CameraEntity
{
public:
	CameraEntity();

	CoordinateScreen translateGlobalToScreen(const Coordinate2D &global) const;
	Coordinate2D translateScreenToGlobal(const CoordinateScreen &screen) const;
	
	void init(Coordinate2D screenCenter, uint32_t screenWidth, uint32_t screenHeight, uint32_t worldWidth, uint32_t worldHeight);
	void setPosition(Coordinate2D newPosition);
	Coordinate2D position() const;
	Coordinate2D screenCenter() const;

	Coordinate2D screenLeftTopCornerPosition() const;
	uint32_t screenWidth() const;
	uint32_t screenHeight() const;

protected:
	Coordinate2D m_position;
	Coordinate2D m_screenCenter;
	Coordinate2D m_leftTop;
	uint32_t m_screenWidth, m_screenHeight;
	uint32_t m_worldWidth, m_worldHeight;
};