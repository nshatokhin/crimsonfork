#include "CameraEntity.h"

CameraEntity::CameraEntity()
{
}

CoordinateScreen CameraEntity::translateGlobalToScreen(const Coordinate2D &global) const
{
	Coordinate2D translated = global - m_position + m_screenCenter;

	return CoordinateScreen(static_cast<int32_t>(translated.x), static_cast<int32_t>(translated.y));
}

Coordinate2D CameraEntity::translateScreenToGlobal(const CoordinateScreen &screen) const
{
	Coordinate2D translated(static_cast<coordinate_t>(screen.x), static_cast<coordinate_t>(screen.y));

	return translated - m_screenCenter + m_position;
}

void CameraEntity::init(Coordinate2D screenCenter, uint32_t screenWidth, uint32_t screenHeight, uint32_t worldWidth, uint32_t worldHeight)
{
	m_screenCenter = screenCenter;
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
	m_worldWidth = worldWidth;
	m_worldHeight = worldHeight;
}

void CameraEntity::setPosition(Coordinate2D newPosition)
{
	Coordinate2D oldCameraPosition = m_position;

	m_position = newPosition;

	if (m_position.x - static_cast<int32_t>(m_screenWidth / 2) <= -static_cast<int32_t>(m_worldWidth / 2) ||
		m_position.x + m_screenWidth / 2 >= m_worldWidth / 2)
	{
		m_position.x = oldCameraPosition.x;
	}

	if (m_position.y - static_cast<int32_t>(m_screenHeight / 2) <= -static_cast<int32_t>(m_worldHeight / 2) ||
		m_position.y + m_screenHeight / 2 >= m_worldHeight / 2)
	{
		m_position.y = oldCameraPosition.y;
	}

	m_leftTop = Coordinate2D(m_position.x - m_screenWidth / 2, m_position.y - m_screenHeight / 2);
}

Coordinate2D CameraEntity::position() const
{
	return m_position;
}

Coordinate2D CameraEntity::screenCenter() const
{
	return m_screenCenter;
}

Coordinate2D CameraEntity::screenLeftTopCornerPosition() const
{
	return m_leftTop;
}

uint32_t CameraEntity::screenWidth() const
{
	return m_screenWidth;
}

uint32_t CameraEntity::screenHeight() const
{
	return m_screenHeight;
}
