#pragma once

#include "GameEntity.h"
#include "commands/CommandReceiver.h"
#include "commands/PlayerCommands.h"
#include "primitives/Coordinate2D.h"
#include "primitives/Vector2D.h"
#include "weapons/Weapon.h"

class PlayerEntity : public GameEntity, public CommandReceiver
{
public:
	enum class Orientation : uint8_t
	{
		IDLE = 0,
		UP,
		UP_RIGHT,
		RIGHT,
		DOWN_RIGHT,
		DOWN,
		DOWN_LEFT,
		LEFT,
		UP_LEFT,
		COUNT
	};

public:
	Weapon weapon;
	

public:
	PlayerEntity();

	void init(uint32_t speedValue, uint32_t physicsComponent, uint32_t animationComponent, double maxHP);

	PlayerCommands commands;
	int8_t horizontalAxis, verticalAxis;

	void updateOrientation();

	Coordinate2D bulletSpawnPoint(const Coordinate2D &position) const;

protected:
	void updateView(Orientation orientation);

protected:
	void stop();
	void move();
};