#pragma once

#include "primitives/Coordinate2D.h"

class GameEntity
{
public:
	GameEntity();
	~GameEntity();

	void init(uint32_t speedValue, uint32_t physicsComponent, uint32_t animationComponent,
		bool staticEntity, bool undestructable, double maxHealth);

public:
	uint32_t maxSpeed;
	uint32_t physicsComponent;
	uint32_t animationComponent;
	bool staticEntity;
	bool undestructable;
	double health;
	double maxHealth;
};