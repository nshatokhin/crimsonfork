#include "PlayerEntity.h"

#include "services/PhysicsPoolLocator.h"
#include "services/PlayersAnimationPoolLocator.h"

#include <iostream>


PlayerEntity::PlayerEntity() : GameEntity(), CommandReceiver(),
	horizontalAxis(0),
	verticalAxis(0)
{
}

void PlayerEntity::init(uint32_t speedValue, uint32_t physicsComponent, uint32_t animationComponent, double maxHP)
{
	GameEntity::init(speedValue, physicsComponent, animationComponent, false, false, maxHP);

	horizontalAxis = 0;
	verticalAxis = 0;

	updateOrientation();
}

void PlayerEntity::updateOrientation()
{
	if (horizontalAxis == 0 && verticalAxis == 0) {
		stop();
	}
	else {
		move();
	}

	if (horizontalAxis == 0 && verticalAxis < 0) {
		updateView(Orientation::UP);
	}
	else if (horizontalAxis > 0 && verticalAxis < 0) {
		updateView(Orientation::UP_RIGHT);
	}
	else if (horizontalAxis > 0 && verticalAxis == 0) {
		updateView(Orientation::RIGHT);
	}
	else if (horizontalAxis > 0 && verticalAxis > 0) {
		updateView(Orientation::DOWN_RIGHT);
	}
	else if (horizontalAxis == 0 && verticalAxis > 0) {
		updateView(Orientation::DOWN);
	}
	else if (horizontalAxis < 0 && verticalAxis > 0) {
		updateView(Orientation::DOWN_LEFT);
	}
	else if (horizontalAxis < 0 && verticalAxis == 0) {
		updateView(Orientation::LEFT);
	}
	else if (horizontalAxis < 0 && verticalAxis < 0) {
		updateView(Orientation::UP_LEFT);
	}
	else {
		updateView(Orientation::IDLE);
	}
}

Coordinate2D PlayerEntity::bulletSpawnPoint(const Coordinate2D &position) const
{
	return weapon.getBulletSpawnPoint(position);
}

void PlayerEntity::updateView(Orientation orientation)
{
	PlayersAnimationPool* ap = PlayersAnimationPoolLocator::locate();
	ap->setState(animationComponent, static_cast<uint32_t>(orientation));
}

void PlayerEntity::stop()
{
	PhysicsPool *pp = PhysicsPoolLocator::locate();
	pp->getObject(physicsComponent)->speed = Vector2D(0, 0);
}

void PlayerEntity::move()
{
	PhysicsPool *pp = PhysicsPoolLocator::locate();
	pp->getObject(physicsComponent)->speed = Vector2D(horizontalAxis, verticalAxis).unitVector() * maxSpeed;
}
