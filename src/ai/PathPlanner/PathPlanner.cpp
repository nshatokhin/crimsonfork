#include "PathPlanner.h"

#include "components/PhysicsComponent.h"
#include "entities/EnemyEntity.h"
#include "services/PhysicsPoolLocator.h"

PathPlanner::PathPlanner(Graph * graph = nullptr, OccupationPartition * occupationGrid = nullptr) :
	m_graph(graph), m_occupationGrid(occupationGrid), m_searchAlgorithm(m_graph, m_occupationGrid)
{
}

void PathPlanner::init(Graph * graph, OccupationPartition * occupationGrid)
{
	m_graph = graph;
	m_occupationGrid = occupationGrid;

	m_searchAlgorithm.Init(m_graph, m_occupationGrid, -1, -1);
}

bool PathPlanner::createPathToPosition(std::list<Coordinate2D>& path, const Coordinate2D &source, const Coordinate2D & target)
{
	m_targetPos = target;

	NodeIndex agentClosestNode = closestNodeToPosition(source);

	if (agentClosestNode == NODE_NOT_EXIST)
	{
		return false;
	}

	NodeIndex targetClosestNode = closestNodeToPosition(m_targetPos);

	if (targetClosestNode == NODE_NOT_EXIST)
	{
		return false;
	}

	m_searchAlgorithm.Init(m_graph, m_occupationGrid, agentClosestNode, targetClosestNode);

	if (!m_searchAlgorithm.Search())
	{
		return false;
	}

	std::list<IdxType> pathOfNodes = m_searchAlgorithm.GetPath();

	if (!pathOfNodes.empty())
	{
		for (std::list<IdxType>::const_iterator iter = pathOfNodes.begin(); iter != pathOfNodes.end(); iter++)
		{
			path.push_back(m_graph->GetNode(*iter).position);
		}

		path.push_back(m_targetPos);

		return true;
	}
	else
	{
		return false;
	}
}

NodeIndex PathPlanner::closestNodeToPosition(const Coordinate2D & position) const
{
	NodeIndex closestNode = static_cast<NodeIndex>(m_occupationGrid->gridIndex(position));

	if (m_graph->isNodePresent(closestNode))
	{
		return closestNode;
	}

	return NODE_NOT_EXIST;
}
