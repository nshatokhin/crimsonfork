#pragma once

#include "primitives/Vector2D.h"
#include "structures/Graph/Graph.h"
#include "structures/Graph/Search/AStarSearch.h"
#include "structures/Graph/Search/Heuristic.h"
#include "structures/GridPartition/OccupationPartition.h"

class EnemyEntity;

class PathPlanner
{
public:
	PathPlanner(Graph * graph, OccupationPartition * occupationGrid);

	void init(Graph * graph, OccupationPartition * occupationGrid);

	bool createPathToPosition(std::list<Coordinate2D>& path, const Coordinate2D &source, const Coordinate2D &target);

protected:
	static constexpr NodeIndex NODE_NOT_EXIST = -1;

	NodeIndex closestNodeToPosition(const Coordinate2D& position) const;

protected:
	Graph * m_graph;
	OccupationPartition * m_occupationGrid;

	Coordinate2D m_targetPos;

	AStarSearch<HeuristicEuclid> m_searchAlgorithm;
};
