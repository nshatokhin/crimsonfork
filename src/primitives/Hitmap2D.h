#pragma once

#include <cstdint>

#include "Coordinate2D.h"

template <uint32_t maxPointsCount>
class Hitmap2D
{
public:
	Hitmap2D() {}
	~Hitmap2D() {}

	void initCircle(Coordinate2D cntr, double radius)
	{
		center = cntr;
		pointsCount = 1;
		points[0].x = radius;
	}

	void initEllipse(Coordinate2D cntr, double radius1, double radius2)
	{
		center = cntr;
		pointsCount = 2;
		points[0].x = radius1;
		points[1].x = radius2;
	}

	void initPolygon(Coordinate2D cntr, uint32_t countOfPoints, Coordinate2D points[])
	{
		center = cntr;
		pointsCount = countOfPoints;
		
		for (uint32_t i = 0; i < pointsCount; i++)
		{
			points[i] = points[i];
		}
	}

	bool isIntersects(const Hitmap2D<maxPointsCount> &other, int32_t &penetrationDepth) const
	{
		if (pointsCount == 1) // is Circle?
		{
			if (pointsCount == 1) // is Circle too
			{
				coordinate_t radius = points[0].x;
				coordinate_t otherRadius = other.points[0].x;

				double centersDistance = Vector2D(center, other.center).length();

				penetrationDepth = static_cast<int32_t>(radius + otherRadius - centersDistance);

				return penetrationDepth > 0;
			}
		}

		return false;
	}

	coordinate_t radius() const
	{
		if (pointsCount == 1)
		{
			return points[0].x;
		}

		return 0;
	}

public:
	Coordinate2D center;
	uint32_t pointsCount;
	Coordinate2D points[maxPointsCount];

};