/*
 * Author: Mykola Shatokhin
 * Date: 04 Sep 2019
 */

#pragma once

#include "Coordinate2D.h"
#include "common/Common.h"
#include "primitives/GameObjectHitmap.h"
#include "services/LoggerLocator.h"

class Rectangle
{
public:
    int width, height;
	Coordinate2D ltCorner;

	Rectangle(Coordinate2D leftTopCorner = Coordinate2D(), uint32_t w = 0, uint32_t h = 0) :
		ltCorner(leftTopCorner), width(w), height(h) {}
    
    bool intersect(Coordinate2D p)
    {
        return p.x >= ltCorner.x && p.y >= ltCorner.y &&
				p.x <= ltCorner.x + width && p.y <= ltCorner.y + height;
    }

	bool intersect(const GameObjectHitmap &h)
	{
		if (h.pointsCount == 1) // Circle
		{
			//coordinate_t radius = h.points[0].x;
			//Coordinate2D center = h.center;

			return (ltCorner.x < h.center.x - h.points[0].x && ltCorner.x + width > h.center.x - h.points[0].x ||
				ltCorner.x < h.center.x + h.points[0].x && ltCorner.x + width > h.center.x + h.points[0].x) &&
				(ltCorner.y < h.center.y - h.points[0].x && ltCorner.y + height > h.center.y - h.points[0].x ||
					ltCorner.y < h.center.y + h.points[0].x && ltCorner.y + height > h.center.y + h.points[0].x);
		}
		else
		{
			LoggerLocator::locate()->logError("Only circles are supported now. TODO: add polygons");
			return false;
		}
	}
};
