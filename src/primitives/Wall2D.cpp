#include "Wall2D.h"

#include <algorithm>
#include <limits>

Wall2D::Wall2D(const Coordinate2D & startPoint, const Coordinate2D & endPoint) :
	begin(startPoint), end(endPoint), normal(Vector2D(begin.y - end.y, end.x - begin.x).unitVector())
{
}

void Wall2D::initWall(const Coordinate2D & startPoint, const Coordinate2D & endPoint)
{
	begin = startPoint;
	end = endPoint;
	normal = Vector2D(begin.y - end.y, end.x - begin.x).unitVector();
}

bool Wall2D::between(coordinate_t a, coordinate_t b, coordinate_t c)
{
	return std::min(a, b) <= c + EPS && c <= std::max(a, b) + EPS;
}

bool Wall2D::intersectsWithLine(const Coordinate2D &linePoint1, const Coordinate2D &linePoint2, Coordinate2D &intersectionPoint)
{
	// Line AB represented as a1x + b1y = c1 
	/*coordinate_t a1 = end.y - begin.y;
	coordinate_t b1 = begin.x - end.x;
	coordinate_t c1 = a1*(begin.x) + b1*(begin.y);

	// Line CD represented as a2x + b2y = c2 
	double a2 = linePoint2.y - linePoint1.y;
	double b2 = linePoint1.x - linePoint2.x;
	double c2 = a2*(linePoint1.x) + b2*(linePoint2.y);

	double determinant = a1*b2 - a2*b1;

	if (determinant == 0)
	{
		// The lines are parallel
		intersectionPoint.x = std::numeric_limits<coordinate_t>::max();
		intersectionPoint.y = std::numeric_limits<coordinate_t>::max();

		return false;
	}
	else
	{
		intersectionPoint.x = (b2*c1 - b1*c2) / determinant;
		intersectionPoint.y = (a1*c2 - a2*c1) / determinant;

		return between(begin.x, end.x, intersectionPoint.x) && between(begin.y, end.y, intersectionPoint.y)
			&& between(linePoint1.x, linePoint2.x, intersectionPoint.x) && between(linePoint1.y, linePoint2.y, intersectionPoint.y);
	}*/

	double divider = (begin.x - end.x) * (linePoint1.y - linePoint2.y) - (begin.y - end.y) * (linePoint1.x - linePoint2.x);

	if (divider == 0)
	{
		// The lines are parallel
		intersectionPoint.x = std::numeric_limits<coordinate_t>::max();
		intersectionPoint.y = std::numeric_limits<coordinate_t>::max();

		return false;
	}
	else
	{
		intersectionPoint.x = ((begin.x * end.y - begin.y * end.x) * (linePoint1.x - linePoint2.x) - (begin.x - end.x) * (linePoint1.x * linePoint2.y - linePoint1.y * linePoint2.x)) / divider;
		intersectionPoint.y = ((begin.x * end.y - begin.y * end.x) * (linePoint1.y - linePoint2.y) - (begin.y - end.y) * (linePoint1.x * linePoint2.y - linePoint1.y * linePoint2.x)) / divider;

		return between(begin.x, end.x, intersectionPoint.x) && between(begin.y, end.y, intersectionPoint.y)
			&& between(linePoint1.x, linePoint2.x, intersectionPoint.x) && between(linePoint1.y, linePoint2.y, intersectionPoint.y);
	}
}
