#include "Vector2D.h"

#include <cmath>

coordinate_t Vector2D::length() const
{
	return sqrt(x*x + y*y);
}

coordinate_t Vector2D::lengthSqr() const
{
	return x*x + y*y;
}

Vector2D Vector2D::unitVector() const
{
	double l = length();

	if (l != 0)
	{
		return Vector2D(x / l, y / l);
	}

	return Vector2D(0, 0);
}

Vector2D Vector2D::crossProduct(const Vector2D & other)
{
	return Vector2D(0, 0); // All data in Z coordinate (0, 0, va.x * vb.y - va.y * vb.x)
}

double Vector2D::dotProduct(const Vector2D & other) const
{
	return x * other.x + y * other.y;
}

Vector2D Vector2D::perpendicular()
{
	return Vector2D(-y, x);
}

Vector2D Vector2D::rotate(const Coordinate2D & centerOfRotation, double angle) const
{
	return Vector2D(
		centerOfRotation.x + (x - centerOfRotation.x) * cos(angle) - (y - centerOfRotation.y) * sin(angle),
		centerOfRotation.y + (y - centerOfRotation.y) * cos(angle) + (x - centerOfRotation.x) * sin(angle)
	);
}

double Vector2D::rotationAngle(const Vector2D &base) const
{
	return atan2(y, x) - atan2(base.y, base.x);
}

double Vector2D::rotationAngle(coordinate_t rx, coordinate_t ry) const
{
	return atan2(y, x) - atan2(ry, rx);
}

Vector2D Vector2D::rotate(const Vector2D &base, const Vector2D &vector) const
{
	return rotate(Coordinate2D(x, y), base.rotationAngle(vector));
}

Vector2D operator*(const Vector2D & left, double right)
{
	return Vector2D(left.x * right, left.y * right);
}

Vector2D operator*(double left, const Vector2D & right)
{
	return operator*(right, left);
}

Vector2D operator/(const Vector2D &left, double right)
{
	return Vector2D(left.x / right, left.y / right);
}

Vector2D operator+(const Vector2D &left, const Vector2D &right)
{
	return Vector2D(left.x + right.x, left.y + right.y);
}

Vector2D operator-(const Vector2D &left, const Vector2D &right)
{
	return Vector2D(left.x - right.x, left.y - right.y);
}

Vector2D operator+(const Vector2D & left, const Coordinate2D & right)
{
	return Vector2D(left.x + right.x, left.y + right.y);
}

Vector2D operator+(const Coordinate2D & left, const Vector2D & right)
{
	return Vector2D(left.x + right.x, left.y + right.y);
}

Vector2D operator-(const Vector2D & left, const Coordinate2D & right)
{
	return Vector2D(left.x - right.x, left.y - right.y);
}

Vector2D operator-(const Coordinate2D & left, const Vector2D & right)
{
	return Vector2D(left.x - right.x, left.y - right.y);
}
