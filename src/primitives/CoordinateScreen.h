#pragma once

#include <cstdint>

#include "Coordinate2D.h"

class CoordinateScreen
{
public:
	int32_t x, y;

public:
	CoordinateScreen(int32_t xv = 0, int32_t yv = 0) : x(xv), y(yv) {}

	friend CoordinateScreen operator+(const CoordinateScreen &left, const Coordinate2D &right)
	{
		return CoordinateScreen(left.x + static_cast<int32_t>(right.x), left.y + static_cast<int32_t>(right.y));
	}
	friend CoordinateScreen operator+(const Coordinate2D &left, const CoordinateScreen &right)
	{
		return operator+(right, left);
	}

};
