#pragma once

#include "primitives/Coordinate2D.h"

class Vector2D : public Coordinate2D
{
public:
	Vector2D(coordinate_t x = 0, coordinate_t y = 0) : Coordinate2D(x, y) {}
	Vector2D(const Coordinate2D &start, const Coordinate2D &end) : Vector2D(end.x - start.x, end.y - start.y) {}
	Vector2D(const Coordinate2D &coord) : Coordinate2D(coord) {}

	double length() const;
	double lengthSqr() const;
	Vector2D unitVector() const;

	Vector2D crossProduct(const Vector2D &other);
	double dotProduct(const Vector2D &other) const;

	Vector2D perpendicular();

	Vector2D rotate(const Coordinate2D &centerOfRotation, double angle) const;
	double rotationAngle(const Vector2D &base) const;
	double rotationAngle(coordinate_t rx, coordinate_t ry) const;
	Vector2D rotate(const Vector2D &base, const Vector2D &vector) const;

	friend Vector2D operator*(const Vector2D &left, double right);
	friend Vector2D operator*(double left, const Vector2D &right);

	friend Vector2D operator/(const Vector2D &left, double right);

	friend Vector2D operator+(const Vector2D &left, const Vector2D &right);
	friend Vector2D operator-(const Vector2D &left, const Vector2D &right);
	friend Vector2D operator+(const Vector2D &left, const Coordinate2D &right);
	friend Vector2D operator+(const Coordinate2D &left, const Vector2D &right);
	friend Vector2D operator-(const Vector2D &left, const Coordinate2D &right);
	friend Vector2D operator-(const Coordinate2D &left, const Vector2D &right);

};