#pragma once

#include <cmath>
#include <cstdint>

typedef double coordinate_t;

class Coordinate2D
{
public:
	coordinate_t x, y;

	Coordinate2D(coordinate_t xv = 0, coordinate_t yv = 0) : x(xv), y(yv) {}
	Coordinate2D(const Coordinate2D &other) : x(other.x), y(other.y) {}
	
	Coordinate2D rotate(const Coordinate2D &centerOfRotation, double angle)
	{
		return Coordinate2D(
			centerOfRotation.x + (x - centerOfRotation.x) * cos(angle) - (y - centerOfRotation.y) * sin(angle),
			centerOfRotation.y + (y - centerOfRotation.y) * cos(angle) + (x - centerOfRotation.x) * sin(angle)
		);
	}

	Coordinate2D& operator+=(const Coordinate2D &other)
	{
		this->x += other.x;
		this->y += other.y;

		return *this;
	}

	friend Coordinate2D operator+(const Coordinate2D &left, const Coordinate2D &right)
	{
		return Coordinate2D(left.x + right.x, left.y + right.y);
	}
	friend Coordinate2D operator-(const Coordinate2D &left, const Coordinate2D &right)
	{
		return Coordinate2D(left.x - right.x, left.y - right.y);
	}
};
