#pragma once

#include "Coordinate2D.h"
#include "Vector2D.h"

#include <vector>

class Wall2D;

typedef Wall2D * WallVector;

class Wall2D
{
public:
	Coordinate2D begin;
	Coordinate2D end;
	Vector2D normal;

public:
	Wall2D(const Coordinate2D &startPoint = Coordinate2D(0, 0), const Coordinate2D &endPoint = Coordinate2D(1, 0));
	~Wall2D() {}

	void initWall(const Coordinate2D &startPoint, const Coordinate2D &endPoint);

	bool between(coordinate_t a, coordinate_t b, coordinate_t c);

	bool intersectsWithLine(const Coordinate2D &linePoint1, const Coordinate2D &linePoint2, Coordinate2D &intersectionPoint);

protected:
	static constexpr double EPS = 1E-9;
};