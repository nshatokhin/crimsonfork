#pragma once

#include "commands/PhysicsCommands.h"
#include "components/PhysicsComponent.h"
#include "pools/RepositioningPool.h"
#include "pools/ThreadPool.h"
#include "primitives/Rectangle.h"
#include "structures/ObjectPool.h"
#include "structures/GridPartition/GridPartition.h"
#include "structures/GridPartition/OccupationPartition.h"
#include "time/Time.h"

class CommandReceiver;

class PhysicsPool : public ObjectPool<PhysicsComponent>
{
public:
	PhysicsPool(uint32_t maxCapacity, Coordinate2D worldLeftTop, int32_t worldWidth, int32_t worldHeight, uint32_t cellWidth, uint32_t cellHeight, uint32_t occupationCellWidth, uint32_t occupationCellHeight, CommandReceiver *collisionListener);

	uint32_t createPhysicsComponent(EntityType entityType, Coordinate2D position, Vector2D speed, uint32_t hitmapRadius, double mass, bool shiftAllowed = true);
	void destroyPhysicsComponent(uint32_t index);

	void update(double dt);
	
	IndexedListNode * objectsInRegion(IndexedListNodePool &pool, const Rectangle &region);

	OccupationPartition& occupationGrid();

protected:
	inline void checkIntersections(IndexedListNode * object, IndexedListNode * chain);
	inline void processIntersection(uint32_t object1Index, uint32_t object2Index);

	void updatePositionsBatch(uint32_t startIdx, uint32_t count, double dt);
	void PhysicsPool::checkIntersectionsBatch(uint32_t startX, uint32_t startY, uint32_t endX, uint32_t endY, uint32_t cellsHorCount, uint32_t cellsVertCount);

protected:
	//QuadTree m_quadTree;
	GridPartition m_gridPartition;
	OccupationPartition m_occupationPartition;
	RepositioningPool m_repositioningPool;
	CommandReceiver * m_commandsProcessor;
	uint32_t m_worldWidth, m_worldHeight;

	CommandCollision collisionCommand;
	CommandOutOfWorld outOfWorldCommand;

	ThreadPool &m_threadPool;
	uint32_t m_threadCount;
	std::vector<std::future<void>> m_futures;
};
