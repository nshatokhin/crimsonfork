#pragma once

#include "entities/BulletEntity.h"
#include "structures/LoopBuffer.h"

class BulletsPool : public LoopBuffer<BulletEntity>
{
public:
	BulletsPool(uint32_t maxCapacity) : LoopBuffer<BulletEntity>(maxCapacity)
	{

	}

	uint32_t createBullet(uint32_t speed, uint32_t physicsComponent, uint32_t spriteComponent)
	{
		uint32_t index = createObject();

		getObject(index)->init(speed, physicsComponent, spriteComponent, false, true, 0);

		return index;
	}

	void destroyBullet(uint32_t bulletIndex)
	{
		destroyObject(bulletIndex);
	}
};
