#pragma once

#include "entities/BonusEntity.h"
#include "structures/ObjectPool.h"

class BonusesPool : public ObjectPool<BonusEntity>
{
public:
	BonusesPool(uint32_t maxCapacity) : ObjectPool<BonusEntity>(maxCapacity)
	{

	}

	uint32_t createBonus(uint32_t speed, const Modifier &modifier, uint32_t physicsComponent, uint32_t spriteComponent)
	{
		uint32_t index = createObject();

		BonusEntity &entity = *getObject(index);
		entity.init(speed, physicsComponent, spriteComponent, true, true, 0);
		entity.modifier = modifier;

		return index;
	}

	void destroyBonus(uint32_t bulletIndex)
	{
		destroyObject(bulletIndex);
	}
};
