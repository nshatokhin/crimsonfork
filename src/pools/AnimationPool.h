#pragma once

#include "components/AnimationComponent.h"
#include "pools/ThreadPool.h"
#include "services/ThreadPoolLocator.h"
#include "structures/ObjectPool.h"

template<uint32_t States, uint32_t Frames>
class AnimationPool : public ObjectPool<AnimationComponent<States, Frames> >
{
public:
	AnimationPool(uint32_t maxCapacity) : ObjectPool<AnimationComponent<States, Frames> >(maxCapacity),
		m_threadPool(*ThreadPoolLocator::locate()),
		m_threadCount(static_cast<uint32_t>(m_threadPool.threads())),
		m_futures(m_threadCount)
	{}

	uint32_t createAnimation(uint32_t statesCount, State<Frames> states[])
	{
		uint32_t index = createObject();

		getObject(index)->init(statesCount, states);

		return index;
	}

	void setState(uint32_t animationId, uint32_t stateId)
	{
		assert(animationId < m_maxCapacity);

		uint32_t index = m_internalIndices[animationId];

		assert(index < m_activeCount && stateId < m_objects[index].m_statesCount);

		m_objects[index].m_currentStateIndex = stateId;
		m_objects[index].m_currentFrameIndex = 0;

		m_objects[index].m_currentSprite = m_objects[index].m_states[m_objects[index].m_currentStateIndex].frames[m_objects[index].m_currentFrameIndex].spriteId;
	}

	void update(double dt)
	{
		uint32_t threadId = 0;
		uint32_t objectsInThread = static_cast<uint32_t>(std::ceil(static_cast<double>(m_activeCount) / m_threadCount));

		for (uint32_t i = 0; i < m_activeCount; i += objectsInThread)
		{
			m_futures[threadId] = m_threadPool.enqueue(std::bind(&AnimationPool::updateBatch, this, i, i + objectsInThread, dt));
			threadId++;
		}

		for (uint32_t i = 0; i < threadId; i++)
		{
			m_futures[i].get();
		}
	}

protected:
	void updateBatch(uint32_t startIdx, uint32_t endIdx, double dt)
	{
		for (uint32_t i = startIdx; i < endIdx && i < m_activeCount; i++)
		{
			m_objects[i].m_playingTime += dt;

			if (m_objects[i].m_playingTime >= m_objects[i].m_states[m_objects[i].m_currentStateIndex].frames[m_objects[i].m_currentFrameIndex].duration)
			{
				m_objects[i].m_playingTime = 0;

				// switch frame
				if (m_objects[i].m_states[m_objects[i].m_currentStateIndex].frames[m_objects[i].m_currentFrameIndex].next == Transition::STAY_HERE)
				{
					return;
				}
				if (m_objects[i].m_states[m_objects[i].m_currentStateIndex].frames[m_objects[i].m_currentFrameIndex].next == Transition::GO_NEXT)
				{
					m_objects[i].m_currentFrameIndex++;

					if (m_objects[i].m_currentFrameIndex >= m_objects[i].m_states[m_objects[i].m_currentStateIndex].framesCount)
					{
						m_objects[i].m_currentFrameIndex = 0;
					}
				}
				else if (m_objects[i].m_states[m_objects[i].m_currentStateIndex].frames[m_objects[i].m_currentFrameIndex].next == Transition::GO_PREV)
				{
					m_objects[i].m_currentFrameIndex--;

					if (m_objects[i].m_currentFrameIndex < 0)
					{
						m_objects[i].m_currentFrameIndex = m_objects[i].m_states[m_objects[i].m_currentStateIndex].framesCount - 1;
					}
				}
				else if (m_objects[i].m_states[m_objects[i].m_currentStateIndex].frames[m_objects[i].m_currentFrameIndex].next == Transition::GO_TO_FRAME)
				{
					if (m_objects[i].m_states[m_objects[i].m_currentStateIndex].frames[m_objects[i].m_currentFrameIndex].transitionArg >= 0 &&
						static_cast<uint32_t>(m_objects[i].m_states[m_objects[i].m_currentStateIndex].frames[m_objects[i].m_currentFrameIndex].transitionArg) < m_objects[i].m_states[m_objects[i].m_currentStateIndex].framesCount)
					{
						m_objects[i].m_currentFrameIndex = m_objects[i].m_states[m_objects[i].m_currentStateIndex].frames[m_objects[i].m_currentFrameIndex].transitionArg;
					}
				}
				else if (m_objects[i].m_states[m_objects[i].m_currentStateIndex].frames[m_objects[i].m_currentFrameIndex].next == Transition::GO_TO_STATE)
				{
					if (m_objects[i].m_states[m_objects[i].m_currentStateIndex].frames[m_objects[i].m_currentFrameIndex].transitionArg >= 0 &&
						static_cast<uint32_t>(m_objects[i].m_states[m_objects[i].m_currentStateIndex].frames[m_objects[i].m_currentFrameIndex].transitionArg) < m_objects[i].m_statesCount)
					{
						m_objects[i].m_currentStateIndex = m_objects[i].m_states[m_objects[i].m_currentStateIndex].frames[m_objects[i].m_currentFrameIndex].transitionArg;
						m_objects[i].m_currentFrameIndex = 0;
					}
				}

				m_objects[i].m_currentSprite = m_objects[i].m_states[m_objects[i].m_currentStateIndex].frames[m_objects[i].m_currentFrameIndex].spriteId;
			}
		}
	}

protected:
	ThreadPool &m_threadPool;
	uint32_t m_threadCount;
	std::vector<std::future<void>> m_futures;
};
