#pragma once

#include "entities/EnemyEntity.h"
#include "primitives/Wall2D.h"
#include "services/PhysicsPoolLocator.h"
#include "structures/GameMap/GameMap.h"
#include "structures/ObjectPoolDD.h"

#include <algorithm>
#include <limits>
#include <memory>

#include "pools/ThreadPool.h"
#include "services/ThreadPoolLocator.h"
#include "structures/PriorityQueue.h"

#include <iostream>
class EnemiesPool : public ObjectPoolDD<EnemyEntity>
{
public:
	static constexpr double WALL_AVOIDANCE_MULT = 0.3;
	static constexpr double OBSTACLE_AVOIDANCE_MULT = 0.4;
	static constexpr double SEPARATION_MULT = 0.5;
	static constexpr double BEHAVIOR_MULT = 0.2;

	static constexpr double waypointTriggerDistSqr = 2500; // TODO: load from config

	Coordinate2D targetPos;
	static constexpr double minimalDetectionBoxLength = 50; // TODO: load from config


	GameMap &m_map;
	ThreadPool &m_threadPool;
	uint32_t m_threadCount;

	std::vector<std::unique_ptr<IndexedListNodePool>> m_objectsChainPools;
	std::vector<std::future<void>> m_futures;

public:

	EnemiesPool(int32_t maxEnemiesSpawn, uint32_t maxPhysObjects, GameMap &gameMap);

	uint32_t createEnemy(const Coordinate2D &target, uint32_t speed, uint32_t physicsComponent, uint32_t spriteComponent, double maxHP);
	void destroyEnemy(uint32_t enemyIndex);

	void update(double dt);

protected:
	void updateBatch(uint32_t startIdx, uint32_t endIdx, uint32_t threadId, double dt);

	inline Vector2D seekSteering(PhysicsPool * pp, PhysicsComponent * pc, uint32_t index, Coordinate2D targetPos)
	{
		return Vector2D(pc->position(), targetPos).unitVector() * m_objects[index].maxSpeed - pc->speed;
	}

	inline Vector2D fleeSteering(PhysicsPool * pp, PhysicsComponent * pc, uint32_t index, Coordinate2D targetPos)
	{
		return Vector2D(targetPos, pc->position()).unitVector() * m_objects[index].maxSpeed - pc->speed;
	}

	inline Vector2D arriveSteering(PhysicsPool * pp, PhysicsComponent * pc, uint32_t index, Coordinate2D targetPos)
	{
		Vector2D toTarget = Vector2D(pc->position(), targetPos);
		double dist = toTarget.length();

		if (dist > 0)
		{
			const double decelerationCoefficient = 0.1; // TODO: load from config
			double speed = dist / decelerationCoefficient;
			speed = std::min<double>(speed, m_objects[index].maxSpeed);

			return toTarget * speed / dist - pc->speed;
		}

		return Vector2D(0, 0);
	}

	inline Vector2D wallAvoidanceSteering(uint32_t wallsCount, WallVector walls, PhysicsPool * pp, PhysicsComponent * pc, uint32_t index)
	{
		EnemyEntity &entity = m_objects[index];

		double distToThisIP = 0.0;
		double distToClosestIP = std::numeric_limits<double>::max();

		int64_t closestWallIndex = -1;

		Vector2D steeringForce;
		Coordinate2D intersectionPoint, closestPoint;

		Vector2D globalFeeler;
		Coordinate2D objectPosition = pc->position();
		
		for (uint32_t feeler = 0; feeler < entity.FEELERS_COUNT; feeler++)
		{
			globalFeeler = translateSpeedLocalVectorToGlobal(entity.feelers[feeler], *pc);

			for (uint32_t wall = 0; wall < wallsCount; wall++)
			{
				if (walls[wall].intersectsWithLine(objectPosition, objectPosition + globalFeeler, intersectionPoint))
				{
					distToThisIP = Vector2D(objectPosition, intersectionPoint).length();

					if (distToThisIP < distToClosestIP)
					{
						distToClosestIP = distToThisIP;
						closestWallIndex = wall;
						closestPoint = intersectionPoint;
					}
				}
			}

			if (closestWallIndex >= 0)
			{
				steeringForce = walls[closestWallIndex].normal * Vector2D(closestPoint, globalFeeler).length();
			}
		}

		return steeringForce;
	}

	inline Vector2D obstacleAvoidanceSteering(IndexedListNodePool &objectsChainPool, double dBoxLength, IndexedListNode *chain, PhysicsPool * pp, PhysicsComponent * pc, uint32_t index)
	{
		PhysicsComponent *closestIntersectingObstacle = nullptr;
		double distToClosestIP = std::numeric_limits<double>::max();
		Coordinate2D localPosOfClosestObstacle, localPos;
		PhysicsComponent *obstaclePhysComponent;

		IndexedListNode * currObj = chain;

		while (currObj)
		{
			if (currObj->value != m_objects[index].physicsComponent)
			{
				obstaclePhysComponent = pp->getObject(currObj->value);

				localPos = translateGlobalToLocalBySpeed(obstaclePhysComponent->position(), *pc);

				if (localPos.x >= 0)
				{
					double expandedRadius = obstaclePhysComponent->hitmap.radius() + pc->hitmap.radius();

					if (fabs(localPos.y) < expandedRadius)
					{
						double cX = localPos.x;
						double cY = localPos.y;
						double sqrtPart = sqrt(expandedRadius*expandedRadius - cY*cY);

						double ip = cX - sqrtPart;

						if (ip <= 0)
						{
							ip = cX + sqrtPart;
						}

						if (ip < distToClosestIP)
						{
							distToClosestIP = ip;

							closestIntersectingObstacle = obstaclePhysComponent;

							localPosOfClosestObstacle = localPos;
						}
					}
				}
			}
			currObj = objectsChainPool.getNext(*currObj);
		}

		Vector2D steeringForce;

		if (closestIntersectingObstacle)
		{
			double multiplier = 1.0 + (dBoxLength - localPosOfClosestObstacle.x) / dBoxLength;

			steeringForce.y = (closestIntersectingObstacle->hitmap.radius() - localPosOfClosestObstacle.y) * multiplier;

			const double brakingWeight = 0.2; // TODO: load from config

			steeringForce.x = (closestIntersectingObstacle->hitmap.radius() - localPosOfClosestObstacle.x) * brakingWeight;
		}

		return translateSpeedLocalVectorToGlobal(steeringForce, *pc);
	}

	inline Vector2D separationSteering(IndexedListNodePool &objectsChainPool, IndexedListNode *chain, PhysicsPool * pp, PhysicsComponent * pc, uint32_t index)
	{
		Vector2D toAgent, steeringForce;

		IndexedListNode * currObj = chain;
		
		while (currObj)
		{
			if (currObj->value != m_objects[index].physicsComponent)
			{
				PhysicsComponent &neighborPhysComponent = *pp->getObject(currObj->value);

				toAgent = Vector2D(neighborPhysComponent.position(), pc->position());

				steeringForce += toAgent.unitVector() / toAgent.length();
			}

			currObj = objectsChainPool.getNext(*currObj);
		}

		return steeringForce;
	}

	inline Vector2D pathFollowSteering(PhysicsPool * pp, PhysicsComponent * pc, uint32_t index)
	{
		EnemyEntity &entity = m_objects[index];

		OccupationPartition &occupationGrid = pp->occupationGrid();

		if (occupationGrid.isOccupiedNotByObject(entity.currentTarget, entity.physicsComponent))
		{
			entity.path.clear();
			entity.pathPlannner.createPathToPosition(entity.path, pc->position(), entity.target);
			entity.currentTarget = pc->position();
			return Vector2D(0, 0);
		}

		//if(occupationGrid.gridIndex(pc->position()) == occupationGrid.gridIndex(entity.currentTarget))
		coordinate_t lengthSqr = Vector2D(pc->position(), entity.currentTarget).lengthSqr();
		if (lengthSqr <= waypointTriggerDistSqr)
		{
			if (entity.path.empty())
			{
				entity.currentSteering = Steering::ARRIVE;
				return Vector2D(0, 0);
			}

			occupationGrid.removeFromGrid(entity.currentTarget, entity.physicsComponent);
			entity.currentTarget = entity.path.front();
			entity.path.pop_front();
			occupationGrid.putToGrid(entity.currentTarget, entity.physicsComponent);
		}

		if (!entity.path.empty())
		{
			return seekSteering(pp, pc, index, entity.currentTarget);
		}
		else
		{
			return arriveSteering(pp, pc, index, entity.currentTarget);
		}
	}

	inline Coordinate2D translateGlobalToLocal(const Coordinate2D &point, PhysicsComponent &object)
	{
		return (point - object.position()).rotate(Coordinate2D(0, 0), - object.orientationAngle());
	}

	inline Coordinate2D translateGlobalToLocalBySpeed(const Coordinate2D &point, PhysicsComponent &object)
	{
		return (point - object.position()).rotate(Coordinate2D(0, 0), -object.speedAngle());
	}

	inline Vector2D translateLocalVectorToGlobal(const Vector2D &vector, PhysicsComponent &object) const
	{
		return vector.rotate(Coordinate2D(0, 0), object.orientationAngle());
	}

	inline Vector2D translateSpeedLocalVectorToGlobal(const Vector2D &vector, PhysicsComponent &object) const
	{
		return vector.rotate(Coordinate2D(0, 0), object.speedAngle());
	}

	inline bool accumulateForce(Vector2D &totalForce, Vector2D forceToAdd, uint32_t index)
	{
		double totalForceMagnitude = totalForce.length();

		double magnitudeRemaining = m_objects[index].maximumForce - totalForceMagnitude;

		if (magnitudeRemaining <= 0) return false;

		double magnitudeToAdd = forceToAdd.length();

		if (magnitudeToAdd < magnitudeRemaining)
		{
			totalForce += forceToAdd;
		}
		else
		{
			totalForce += forceToAdd.unitVector() * magnitudeRemaining;
		}

		return true;
	}

	inline Vector2D calculateSteeringForce(IndexedListNodePool &objectsChainPool, PhysicsPool * pp, PhysicsComponent * pc, uint32_t index)
	{
		Vector2D totalSteeringForce, force;
		EnemyEntity &entity = m_objects[index];

		double dBoxLength = minimalDetectionBoxLength + pc->speed.length() / entity.maxSpeed * minimalDetectionBoxLength;

		objectsChainPool.clear();
		IndexedListNode *chain = pp->objectsInRegion(objectsChainPool, Rectangle(pc->position() - Coordinate2D(dBoxLength, dBoxLength), 2 * dBoxLength, 2 * dBoxLength));


		if (entity.wallAvoidance)
		{
			force = wallAvoidanceSteering(m_map.wallsCount(), m_map.walls(), pp, pc, index) * WALL_AVOIDANCE_MULT;

			if (!accumulateForce(totalSteeringForce, force, index))
				return totalSteeringForce;
		}

		if (entity.obstacleAvoidance)
		{
			force = obstacleAvoidanceSteering(objectsChainPool, dBoxLength, chain, pp, pc, index) * OBSTACLE_AVOIDANCE_MULT;

			if (!accumulateForce(totalSteeringForce, force, index))
				return totalSteeringForce;
		}

		if (entity.separation)
		{
			force = separationSteering(objectsChainPool, chain, pp, pc, index) * SEPARATION_MULT;

			if (!accumulateForce(totalSteeringForce, force, index))
				return totalSteeringForce;
		}

		switch (entity.currentSteering)
		{
		case Steering::SEEK:
			force = seekSteering(pp, pc, index, m_objects[index].target);
			break;
		case Steering::FLEE:
			force = fleeSteering(pp, pc, index, m_objects[index].target);
			break;
		case Steering::ARRIVE:
			force = arriveSteering(pp, pc, index, m_objects[index].target);
			break;
		case Steering::PATH_FOLLOW:
			force = pathFollowSteering(pp, pc, index);
			break;
		default: force = Vector2D(0, 0);
		}

		force = force * BEHAVIOR_MULT;

		accumulateForce(totalSteeringForce, force, index);

		return totalSteeringForce;
	}
};
