#pragma once

#include "pools/ThreadPool.h"
#include "services/ThreadPoolLocator.h"
#include "structures/GridPartition/GridPartition.h"
#include "structures/GridPartition/OccupationPartition.h"
#include "structures/ObjectPool.h"

#include <cassert>
#include <mutex>

class PositionValue
{
public:
	uint32_t index;
	Coordinate2D position;
};

class RepositioningPool : public ObjectPool<PositionValue>
{
public:
	RepositioningPool(uint32_t maxCapacity, GridPartition &gridPartition, OccupationPartition &occupationPartition, PhysicsComponent * objectsArray, uint32_t * objectsInternalIndices) : ObjectPool(maxCapacity),
		m_gridPartition(gridPartition),
		m_occupationPartition(occupationPartition),
		m_repositioningObjects(objectsArray),
		m_objectsInternalIndices(objectsInternalIndices),
		m_threadPool(*ThreadPoolLocator::locate()),
		m_threadCount(static_cast<uint32_t>(m_threadPool.threads())),
		m_futures(m_threadCount)
	{
		assert(m_repositioningObjects);
		assert(m_objectsInternalIndices);
	}

	// TODO: fix bug, objects can be added multiple time to pool
	uint32_t putRepositioningObject(uint32_t index, Coordinate2D position)
	{
		std::unique_lock<std::mutex> lock(putMutex);

		uint32_t objIndex = createObject();

		PositionValue &object = *getObject(objIndex);
		object.index = index;
		object.position = position;

		return objIndex;
	}

	void removeRepositioningObject(uint32_t index)
	{
		destroyObject(index);
	}

	void clear()
	{
		m_activeCount = 0;
		m_firstActiveObject = 0;
		m_lastActiveObject = 0;
		m_firstFree = 0;
	}

	void repositionObjects()
	{
		uint32_t threadId = 0;
		uint32_t objectsInThread = static_cast<uint32_t>(std::ceil(static_cast<double>(m_activeCount) / m_threadCount));

		for (uint32_t i = 0; i < m_activeCount; i += objectsInThread)
		{
			m_futures[threadId] = m_threadPool.enqueue(std::bind(&RepositioningPool::updatePositionsBatch, this, i, i + objectsInThread));
			threadId++;
		}

		for (uint32_t i = 0; i < threadId; i++)
		{
			m_futures[i].get();
		}

		clear();
	}

protected:
	void updatePositionsBatch(uint32_t startIdx, uint32_t endIdx)
	{
		uint32_t internalIndex;
		Coordinate2D oldPosition;

		for (uint32_t i = startIdx; i < endIdx && i < m_activeCount; i++)
		{
			PositionValue &value = m_objects[i];
			internalIndex = m_objectsInternalIndices[value.index];

			oldPosition = m_repositioningObjects[internalIndex].position();
			m_repositioningObjects[internalIndex].setPosition(value.position);
			m_gridPartition.move(oldPosition, value.position, value.index);
			m_occupationPartition.move(oldPosition, value.position, value.index); // TODO: do it only fo AI entities
		}
	}

protected:
	GridPartition& m_gridPartition;
	OccupationPartition& m_occupationPartition;
	PhysicsComponent * m_repositioningObjects;
	uint32_t * m_objectsInternalIndices;

	ThreadPool &m_threadPool;
	uint32_t m_threadCount;
	std::vector<std::future<void>> m_futures;

	std::mutex putMutex;
};
