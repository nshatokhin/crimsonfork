#include "EnemiesPool.h"

EnemiesPool::EnemiesPool(int32_t maxEnemiesSpawn, uint32_t maxPhysObjects, GameMap & gameMap) : ObjectPoolDD(maxEnemiesSpawn),
	m_map(gameMap),
	m_threadPool(*ThreadPoolLocator::locate()),
	m_threadCount(static_cast<uint32_t>(m_threadPool.threads())),
	m_objectsChainPools(m_threadCount),
	m_futures(m_threadCount)
{
	for (uint32_t i = 0; i < m_threadCount; i++)
	{
		m_objectsChainPools[i] = std::unique_ptr<IndexedListNodePool>(new IndexedListNodePool(maxPhysObjects));
	}
}

uint32_t EnemiesPool::createEnemy(const Coordinate2D & target, uint32_t speed, uint32_t physicsComponent, uint32_t spriteComponent, double maxHP)
{
	uint32_t index = createMainDomain();

	EnemyEntity &entity = *getObject(index);
	entity.init(&m_map.graph(), &PhysicsPoolLocator::locate()->occupationGrid(), speed, physicsComponent, spriteComponent, maxHP);
	entity.target = target;

	return index;
}

void EnemiesPool::destroyEnemy(uint32_t enemyIndex)
{
	destroyObject(enemyIndex);
}

void EnemiesPool::update(double dt)
{
	uint32_t threadId = 0;
	uint32_t objectsInThread = static_cast<uint32_t>(std::ceil(static_cast<double>(m_activeCount) / m_threadCount));

	for (uint32_t i = 0; i < m_activeCount; i += objectsInThread)
	{
		m_futures[threadId] = m_threadPool.enqueue(std::bind(&EnemiesPool::updateBatch, this, i, i + objectsInThread, threadId, dt));
		threadId++;
	}

	for (uint32_t i = 0; i < threadId; i++)
	{
		m_futures[i].get();
	}
}

void EnemiesPool::updateBatch(uint32_t startIdx, uint32_t endIdx, uint32_t threadId, double dt)
{
	// calculate target direction vector
	PhysicsPool * pp = PhysicsPoolLocator::locate();
	PhysicsComponent * pc;
	Vector2D steeringForce, acceleration;

	for (uint32_t i = startIdx; i < endIdx && i < m_activeCount; i++)
	{
		//m_objects[i].target = targetPos;

		pc = pp->getObject(m_objects[i].physicsComponent);

		pc->orientation = Vector2D(pc->position(), m_objects[i].target).unitVector();

		if (m_objects[i].path.empty())
			m_objects[i].pathPlannner.createPathToPosition(m_objects[i].path, pc->position(), m_objects[i].target);
		else
		{
			if (m_objects[i].currentSteering != Steering::PATH_FOLLOW)
			{
				m_objects[i].currentSteering = Steering::PATH_FOLLOW;
				m_objects[i].currentTarget = pc->position();
			}
		}

		steeringForce = calculateSteeringForce(*m_objectsChainPools[threadId].get(), pp, pc, i);

		acceleration = steeringForce / pc->mass;
		pc->speed += acceleration * dt;
	}
}
