#include "GroundTilesPool.h"

#include "helpers/RandomGenerator.h"

GroundTilesPool::GroundTilesPool(uint32_t tilesCount, uint32_t mapWidth, uint32_t mapHeight, uint32_t tileWidth, uint32_t tileHeight) : 
	m_tilesCount(tilesCount),
	m_horizontalTilesCount(static_cast<uint32_t>(std::ceil(static_cast<double>(mapWidth) / tileWidth))),
	m_verticalTilesCount(static_cast<uint32_t>(std::ceil(static_cast<double>(mapHeight) / tileHeight))),
	m_mapWidth(mapWidth),
	m_mapHeight(mapHeight),
	m_tileWidth(tileWidth),
	m_tileHeight(tileHeight)
{
	tiles = new GroundTile[m_verticalTilesCount*m_horizontalTilesCount];
}

GroundTilesPool::~GroundTilesPool()
{
	delete[] tiles;
}

void GroundTilesPool::generateTiles()
{
	for (uint32_t i = 0; i < m_horizontalTilesCount; i++)
	{
		for (uint32_t j = 0; j < m_verticalTilesCount; j++)
		{
			tiles[j * m_horizontalTilesCount + i] = RandomGenerator::generate(0, m_tilesCount);
		}
	}
}

uint32_t GroundTilesPool::tilesCount() const
{
	return m_tilesCount;
}

VisibleTiles GroundTilesPool::visibleTiles(const Coordinate2D & screenLeftTop, uint32_t screenWidth, uint32_t screenHeight)
{
	VisibleTiles vt;

	uint32_t mapOffsetX = m_mapWidth / 2;
	uint32_t mapOffsetY = m_mapHeight / 2;

	int64_t left = (screenLeftTop.x + mapOffsetX) / m_tileWidth;
	int64_t top = (screenLeftTop.y + mapOffsetY) / m_tileHeight;
	double horizontalTilesCount = std::ceil((screenLeftTop.x + mapOffsetX + screenWidth) / m_tileWidth);
	double verticalTilesCount = std::ceil((screenLeftTop.y + mapOffsetY + screenHeight) / m_tileHeight);
	
	if(left < 0)
		vt.left = 0;
	else if(left >= m_horizontalTilesCount)
		vt.left = m_horizontalTilesCount - 1;
	else
		vt.left = static_cast<uint32_t>(left);
	
	if(top < 0)
		vt.top = 0;
	else if(top >= m_verticalTilesCount)
		vt.top = m_verticalTilesCount - 1;
	else
		vt.top = static_cast<uint32_t>(top);
		
	if(horizontalTilesCount < 0)
		vt.horizontalTilesCount = 0;
	else if(horizontalTilesCount > m_horizontalTilesCount)
		vt.horizontalTilesCount = m_horizontalTilesCount;
	else
		vt.horizontalTilesCount = static_cast<uint32_t>(horizontalTilesCount);
		
	if(verticalTilesCount < 0)
		vt.verticalTilesCount = 0;
	else if(verticalTilesCount > m_verticalTilesCount)
		vt.verticalTilesCount = m_verticalTilesCount;
	else
		vt.verticalTilesCount = static_cast<uint32_t>(verticalTilesCount);

	return vt;
}

TilesFieldSize GroundTilesPool::tilesFieldSize() const
{
	TilesFieldSize size;
	size.horizontalCount = m_horizontalTilesCount;
	size.verticalCount = m_verticalTilesCount;

	return size;
}
