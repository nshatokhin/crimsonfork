#pragma once

#include "common/Common.h"
#include "pools/AnimationPool.h"

typedef AnimationPool<GENERAL_ANIMATION_STATES, GENERAL_ANIMATION_FRAMES> GeneralAnimation;

class GeneralAnimationPool : public GeneralAnimation
{
public:
	GeneralAnimationPool(uint32_t maxCapacity) : GeneralAnimation(maxCapacity) {}
};
