#pragma once

#include "common/Common.h"
#include "pools/AnimationPool.h"

typedef AnimationPool<PLAYER_ANIMATION_STATES, PLAYER_ANIMATION_FRAMES> PlayerPool;

class PlayersAnimationPool : public PlayerPool
{
public:
	PlayersAnimationPool(uint32_t maxCapacity) : PlayerPool(maxCapacity) {}
};
