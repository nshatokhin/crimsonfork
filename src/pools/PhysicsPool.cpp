#include "PhysicsPool.h"

#include "commands/CommandReceiver.h"
#include "entities/CameraEntity.h"
#include "pools/IndexedListNode.h"
#include "services/ThreadPoolLocator.h"


PhysicsPool::PhysicsPool(uint32_t maxCapacity, Coordinate2D worldLeftTop, int32_t worldWidth, int32_t worldHeight, uint32_t cellWidth, uint32_t cellHeight, uint32_t occupationCellWidth, uint32_t occupationCellHeight, CommandReceiver *commandsProcessor) : ObjectPool<PhysicsComponent>(maxCapacity),
m_gridPartition(maxCapacity, cellWidth, cellHeight, worldWidth, worldHeight),
m_occupationPartition(maxCapacity, occupationCellWidth, occupationCellHeight, worldWidth, worldHeight),
m_repositioningPool(maxCapacity, m_gridPartition, m_occupationPartition, m_objects, m_internalIndices),
m_commandsProcessor(commandsProcessor),
m_worldWidth(worldWidth),
m_worldHeight(worldHeight),
m_threadPool(*ThreadPoolLocator::locate()),
m_threadCount(static_cast<uint32_t>(m_threadPool.threads())),
m_futures(m_threadCount)
{
	assert(m_commandsProcessor);
}

uint32_t PhysicsPool::createPhysicsComponent(EntityType entityType, Coordinate2D position, Vector2D speed, uint32_t hitmapRadius, double mass, bool shiftAllowed)
{
	uint32_t index = createObject();

	PhysicsComponent * component = getObject(index);
	component->init(entityType, position, speed, hitmapRadius, mass, shiftAllowed);

	m_gridPartition.putToGrid(component->position(), index);

	if (entityType == EntityType::ENEMY)
	{
		m_occupationPartition.putToGrid(component->position(), index);
	}

	return index;
}

void PhysicsPool::destroyPhysicsComponent(uint32_t index)
{
	PhysicsComponent &comp = *getObject(index);
	m_gridPartition.removeFromGrid(comp.position(), index);
	
	if (comp.type == EntityType::ENEMY)
	{
		m_occupationPartition.removeFromGrid(getObject(index)->position(), index);
	}

	destroyObject(index);
}

void PhysicsPool::update(double dt)
{
	// update positions
	uint32_t threadId = 0;
	uint32_t objectsInThread = static_cast<uint32_t>(std::ceil(static_cast<double>(m_activeCount) / m_threadCount));

	for (uint32_t i = 0; i < m_activeCount; i += objectsInThread)
	{
		m_futures[threadId] = m_threadPool.enqueue(std::bind(&PhysicsPool::updatePositionsBatch, this, i, objectsInThread, dt));
		threadId++;
	}

	for (uint32_t i = 0; i < threadId; i++)
	{
		m_futures[i].get();
	}

	// check intersections
	int64_t * grid = m_gridPartition.grid();
	uint32_t cellsHorCount = m_gridPartition.cellsHorCount();
	uint32_t cellsVertCount = m_gridPartition.cellsVertCount();
	
	threadId = 0;
	uint32_t threadsForDimension = m_threadCount / 2;
	uint32_t gridXSize = static_cast<uint32_t>(std::ceil(static_cast<double>(cellsHorCount) / threadsForDimension));
	uint32_t gridYSize = static_cast<uint32_t>(std::ceil(static_cast<double>(cellsVertCount) / threadsForDimension));

	for (uint32_t x = 0; x < cellsHorCount; x += gridXSize)
	{
		for (uint32_t y = 0; y < cellsVertCount; y += gridYSize)
		{
			m_futures[threadId] = m_threadPool.enqueue(std::bind(&PhysicsPool::checkIntersectionsBatch, this, x, y, x + gridXSize, y + gridYSize, cellsHorCount, cellsVertCount));
			threadId++;
		}
	}

	for (uint32_t i = 0; i < threadId; i++)
	{
		m_futures[i].get();
	}

	// Reposition shifted objects
	m_repositioningPool.repositionObjects();
}

void PhysicsPool::updatePositionsBatch(uint32_t startIdx, uint32_t count, double dt)
{
	bool outOfWorld;
	Coordinate2D newPosition, oldPosition;
	for (uint32_t i = startIdx; i < startIdx + count && i < m_activeCount; i++)
	{
		oldPosition = m_objects[i].position();

		newPosition = oldPosition;
		newPosition += m_objects[i].speed * dt;

		outOfWorld = false;
		if (newPosition.x < -static_cast<coordinate_t>(m_worldWidth / 2))
		{
			newPosition.x = -static_cast<coordinate_t>(m_worldWidth / 2);
			outOfWorld = true;
		}
		else if (newPosition.x >= m_worldWidth / 2)
		{
			newPosition.x = m_worldWidth / 2 - 1;
			outOfWorld = true;
		}

		if (newPosition.y < -static_cast<coordinate_t>(m_worldHeight / 2))
		{
			newPosition.y = -static_cast<coordinate_t>(m_worldHeight / 2);
			outOfWorld = true;
		}
		else if (newPosition.y >= m_worldHeight / 2)
		{
			newPosition.y = m_worldHeight / 2 - 1;
			outOfWorld = true;
		}

		m_objects[i].setPosition(newPosition);

		if (outOfWorld)
		{
			outOfWorldCommand.physicsComponent = m_externalIndices[i];
			m_commandsProcessor->processCommand(&outOfWorldCommand);
		}

		m_gridPartition.move(oldPosition, newPosition, m_externalIndices[i]);

		if (m_objects[i].type == EntityType::ENEMY)
		{
			m_occupationPartition.move(oldPosition, newPosition, m_externalIndices[i]);
		}
	}
}

void PhysicsPool::checkIntersectionsBatch(uint32_t startX, uint32_t startY, uint32_t endX, uint32_t endY, uint32_t cellsHorCount, uint32_t cellsVertCount)
{
	IndexedListNode * object;

	for (uint32_t x = startX; x < endX && x < cellsHorCount; x++)
	{
		for (uint32_t y = startY; y < endY && y < cellsVertCount; y++)
		{
			object = m_gridPartition.gridObjectsChain(x, y);

			if (object == nullptr)
				continue;

			while (object != nullptr)
			{
				checkIntersections(object, m_gridPartition.nextObject(*object));

				if (x > 0) checkIntersections(object, m_gridPartition.gridObjectsChain(x - 1, y));
				if (y > 0) checkIntersections(object, m_gridPartition.gridObjectsChain(x, y - 1));
				if (x > 0 && y > 0) checkIntersections(object, m_gridPartition.gridObjectsChain(x - 1, y - 1));
				if (x > 0 && y < cellsVertCount - 1) checkIntersections(object, m_gridPartition.gridObjectsChain(x - 1, y + 1));

				object = m_gridPartition.nextObject(*object);
			}
		}
	}
}

IndexedListNode * PhysicsPool::objectsInRegion(IndexedListNodePool & pool, const Rectangle & region)
{
	return m_gridPartition.objectsInRegion(pool, region);
}

OccupationPartition & PhysicsPool::occupationGrid()
{
	return m_occupationPartition;
}

inline void PhysicsPool::checkIntersections(IndexedListNode * object, IndexedListNode * chain)
{
	while (chain != nullptr)
	{
		processIntersection(object->value, chain->value);

		chain = m_gridPartition.nextObject(*chain);
	}
}

inline void PhysicsPool::processIntersection(uint32_t object1Index, uint32_t object2Index)
{
	int32_t penetrationDepth;
	Coordinate2D obj1Pos, obj2Pos;
	Vector2D shiftingVec;
	uint32_t object1InternalIndex = m_internalIndices[object1Index];
	uint32_t object2InternalIndex = m_internalIndices[object2Index];

	if (m_objects[object1InternalIndex].hitmap.isIntersects(m_objects[object2InternalIndex].hitmap, penetrationDepth))
	{
		obj1Pos = m_objects[object1InternalIndex].position();
		obj2Pos = m_objects[object2InternalIndex].position();
		shiftingVec = Vector2D(obj1Pos, obj2Pos).unitVector() * penetrationDepth;

		// Static collision resolving
		if (m_objects[object1InternalIndex].canBeShifted)
		{
			m_repositioningPool.putRepositioningObject(object1Index, obj1Pos - shiftingVec);
		}
		else if (m_objects[object2InternalIndex].canBeShifted)
		{
			m_repositioningPool.putRepositioningObject(object2Index, obj2Pos + shiftingVec);
		}
		else
		{
			m_objects[object1InternalIndex].speed = m_objects[object2InternalIndex].speed = Vector2D(0, 0);
		}

		collisionCommand.physicsComponent1 = object1Index;
		collisionCommand.physicsComponent2 = object2Index;
		m_commandsProcessor->processCommand(&collisionCommand);
	}
}
