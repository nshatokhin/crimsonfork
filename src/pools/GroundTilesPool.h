#pragma once

#include "primitives/Coordinate2D.h"
#include "structures/ObjectPool.h"

#include <cmath>

typedef uint8_t GroundTile;

struct VisibleTiles
{
	uint32_t left, top;
	uint32_t horizontalTilesCount;
	uint32_t verticalTilesCount;
};

struct TilesFieldSize
{
	uint32_t horizontalCount;
	uint32_t verticalCount;
};

class GroundTilesPool
{
public:
	GroundTilesPool(uint32_t tilesCount, uint32_t mapWidth, uint32_t mapHeight, uint32_t tileWidth, uint32_t tileHeight);
	~GroundTilesPool();

	void generateTiles();

	uint32_t tilesCount() const;

	VisibleTiles visibleTiles(const Coordinate2D &screenLeftTop, uint32_t screenWidth, uint32_t screenHeight);

	GroundTile * tiles;
	TilesFieldSize tilesFieldSize() const;

protected:
	uint32_t m_tilesCount;
	uint32_t m_horizontalTilesCount, m_verticalTilesCount;
	uint32_t m_mapWidth, m_mapHeight;
	uint32_t m_tileWidth, m_tileHeight;

};