#pragma once

#include "common/Common.h"
#include "pools/AnimationPool.h"

typedef AnimationPool<BUTTON_ANIMATION_STATES, BUTTON_ANIMATION_FRAMES> ButtonsPool;

class ButtonsAnimationPool : public ButtonsPool
{
public:
	ButtonsAnimationPool(uint32_t maxCapacity) : ButtonsPool(maxCapacity) {}
};
