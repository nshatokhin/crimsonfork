#pragma once

#include "IndexedListNode.h"
#include "structures/ObjectPool.h"

class IndexedListNodePool : public ObjectPool<IndexedListNode>
{
public:
	IndexedListNodePool(uint32_t maxCapacity) : ObjectPool(maxCapacity)
	{}

	IndexedListNode * getNext(IndexedListNode& chain)
	{
		if (chain.next >= 0)
		{
			return getObject(chain.next);
		}

		return nullptr;
	}
};
