#pragma once

#include "SDL_mixer.h"

#include <cstdint>
#include <string>
#include <vector>

class SoundService
{
public:
	SoundService(std::vector<std::string> sounds, std::vector<std::string> music);
	~SoundService();

	void playSound(uint32_t soundId);
	void playMusic(uint32_t musicId, bool inLoop);

	void setSoundsVolume(double volume);
	void setMusicVolume(double volume);

	bool isMusicPlaying() const;
	bool isMusicPaused() const;

	void pauseMusic();
	void resumeMusic();
	void stopMusic();


protected:
	static constexpr int32_t FREQUENCY = 22050;
	static constexpr int32_t FORMAT = MIX_DEFAULT_FORMAT;
	static constexpr int8_t CHANNELS = 2;
	static constexpr int32_t CHUNK_SIZE = 4096;
	static constexpr int32_t MIX_CHANNELS_COUNT = 16;

protected:
	std::vector<std::string> m_music;
	std::vector<std::string> m_sounds;

	std::vector<Mix_Music *> m_musicChunks;
	std::vector<Mix_Chunk *> m_soundChunks;
};