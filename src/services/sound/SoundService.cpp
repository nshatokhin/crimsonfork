#include "SoundService.h"

#include "services/LoggerLocator.h"
#include <cassert>

SoundService::SoundService(std::vector<std::string> sounds, std::vector<std::string> music)
{
	m_sounds = sounds;
	m_music = music;

	//Initialize SDL_mixer 
	if (Mix_OpenAudio(FREQUENCY, FORMAT, CHANNELS, CHUNK_SIZE) == -1)
	{
		LoggerLocator::locate()->logError(std::string("Can't init SDL_Mixer: ").append(Mix_GetError()));
		return;
	}

	Mix_Chunk * soundChunk;
	for (size_t i = 0; i < m_sounds.size(); i++)
	{
		soundChunk = Mix_LoadWAV(m_sounds.at(i).c_str());
		if (soundChunk == nullptr)
		{
			LoggerLocator::locate()->logError(std::string("Load sound error: ").append(Mix_GetError()));
		}
		m_soundChunks.push_back(soundChunk);
	}

	Mix_Music * musicChunk;
	for (size_t i = 0; i < m_music.size(); i++)
	{
		musicChunk = Mix_LoadMUS(m_music.at(i).c_str());
		if (musicChunk == nullptr)
		{
			LoggerLocator::locate()->logError(std::string("Load music error: ").append(Mix_GetError()));
		}
		m_musicChunks.push_back(musicChunk);
	}

	Mix_AllocateChannels(MIX_CHANNELS_COUNT);
}

SoundService::~SoundService()
{
	for (size_t i = 0; i < m_soundChunks.size(); i++)
	{
		Mix_FreeChunk(m_soundChunks.at(i));
	}

	for (size_t i = 0; i < m_musicChunks.size(); i++)
	{
		Mix_FreeMusic(m_musicChunks.at(i));
	}

	Mix_CloseAudio();
}

void SoundService::playSound(uint32_t soundId)
{
	assert(soundId < m_soundChunks.size());

	if (Mix_PlayChannel(-1, m_soundChunks.at(soundId), 0) == -1)
	{
		LoggerLocator::locate()->logError(std::string("Mix_PlayChannel: ").append(Mix_GetError()));
	}
}

void SoundService::playMusic(uint32_t musicId, bool inLoop)
{
	assert(musicId < m_musicChunks.size());

	if (Mix_PlayMusic(m_musicChunks.at(musicId), inLoop ? -1 : 1) == -1)
	{
		LoggerLocator::locate()->logError(std::string("Mix_PlayMusic: ").append(Mix_GetError()));
	}
}

void SoundService::setSoundsVolume(double volume)
{
	Mix_Volume(-1, static_cast<uint32_t>(128 * volume));
}

void SoundService::setMusicVolume(double volume)
{
	Mix_VolumeMusic(static_cast<uint32_t>(128 * volume));
}

bool SoundService::isMusicPlaying() const
{
	return Mix_PlayingMusic() != 0;
}

bool SoundService::isMusicPaused() const
{
	return Mix_PausedMusic() == 1;
}

void SoundService::pauseMusic()
{
	Mix_PauseMusic();
}

void SoundService::resumeMusic()
{
	Mix_ResumeMusic();
}

void SoundService::stopMusic()
{
	Mix_HaltMusic();
}

