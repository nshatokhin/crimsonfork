#pragma once

#include <fstream>
#include <string>

class LoggerService
{
public:
	LoggerService(std::string logFileName = "log.txt", bool append = false);
	~LoggerService();

	void logDebug(std::string message);
	void logInfo(std::string message);
	void logWarning(std::string message);
	void logError(std::string message);
	void logFatal(std::string message);

protected:
	std::ofstream m_logFile;

};