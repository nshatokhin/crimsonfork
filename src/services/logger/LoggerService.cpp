#include "LoggerService.h"

#include <iostream>

LoggerService::LoggerService(std::string logFileName, bool append)
{
	m_logFile.open(logFileName, std::ios_base::out | (append ? std::ios_base::app : std::ios_base::trunc));

	if (!m_logFile.is_open())
	{
		std::cout << "File \"" << logFileName << "\" can't be opened" << std::endl;
		return;
	}
}

LoggerService::~LoggerService()
{
	m_logFile.close();
}

void LoggerService::logDebug(std::string message)
{
	m_logFile << "Debug: " << message << std::endl;
}

void LoggerService::logInfo(std::string message)
{
	m_logFile << "Info: " << message << std::endl;
}

void LoggerService::logWarning(std::string message)
{
	m_logFile << "Warning: " << message << std::endl;
}

void LoggerService::logError(std::string message)
{
	m_logFile << "Error: " << message << std::endl;
}

void LoggerService::logFatal(std::string message)
{
	m_logFile << "Fatal: " << message << std::endl;
}
