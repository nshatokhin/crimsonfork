#pragma once

#include <assert.h>

template <typename T>
class ServiceLocator
{
public:
	static void provide(T * service) {
		assert(service != nullptr);

		m_service = service;
	}

	static T * locate() {
		return m_service;
	}

private:
	static T * m_service;

};

template<typename T>
T * ServiceLocator<T>::m_service = nullptr;
