#pragma once

#include <map>
#include <string>
#include <vector>

#include "3rdparty/rapidjson/document.h"

#include <bonuses/BonusParams.h>
#include "common/Common.h"
#include "components/Modifier.h"
#include "entities/EnemyEntity.h"
#include "weapons/Weapon.h"

typedef std::map<std::string, uint32_t> IdMap;

typedef std::vector<std::string> SpritesVector;
typedef std::vector<std::string> TerrainVector;
typedef std::vector<std::string> SoundsVector;
typedef std::vector<std::string> MusicVector;
typedef std::vector<uint32_t> FontVector;
typedef std::vector<std::string> BonusesVector;
typedef std::vector<std::string> WeaponsVector;

class ConfigService
{
public:
	ConfigService(std::string filename = "config.json");
	~ConfigService();

	bool isInitialized() const;

	SpritesVector sprites() const;
	SoundsVector sounds() const;
	MusicVector music() const;
	TerrainVector terrains() const;

	SpritesVector menuSprites() const;
	SoundsVector menuSounds() const;
	MusicVector menuMusic() const;
	TerrainVector menuTerrains() const;

	uint32_t terrainTilesCount() const;
	uint32_t menuTilesCount() const;

	uint32_t tileWidth() const;
	uint32_t tileHeight() const;

	uint32_t minEnemySpawnDistance() const;
	uint32_t maxEnemySpawnDistance() const;
	uint32_t enemiesSpawnSimultaneously() const;
	double enemiesSpawnCooldownTime() const;
	uint32_t gridCellWidth() const;
	uint32_t gridCellHeight() const;

	EnemyParams enemyParams() const;

	uint32_t playerSpeed() const;
	uint32_t playerHitmapRadius() const;
	uint32_t playerDefaultWeapon() const;
	uint32_t playerStatesCount() const;
	PlayerAnimation * playerAnimationStates();

	uint32_t gameBulletIndicatorX() const;
	uint32_t gameBulletIndicatorY() const;
	uint32_t gameBulletIndicatorSpriteId() const;
	uint32_t menuCursorArrowSpriteId() const;
	uint32_t cursorArrowSpriteId() const;
	uint32_t cursorAimSpriteId() const;
	uint32_t scoreIndicatorY() const;
	uint32_t timeIndicatorX() const;
	uint32_t timeIndicatorY() const;

	uint32_t menuMusicTheme() const;
	uint32_t gameMusic() const;
	uint32_t gameOverMusic() const;
	uint32_t victoryMusic() const;
	double soundsVolume() const;
	double musicVolume() const;

	uint32_t gameOverPopupVictoryBackground() const;
	uint32_t gameOverPopupFailBackground() const;

	uint32_t gameOverPopupExitButtonStates() const;
	ButtonAnimation * gameOverPopupExitButtonAnimation();
	int32_t gameOverPopupExitButtonX() const;
	uint32_t gameOverPopupExitButtonY() const;
	uint32_t gameOverPopupExitButtonWidth() const;
	uint32_t gameOverPopupExitButtonHeight() const;

	uint32_t gameOverPopupRestartButtonStates() const;
	ButtonAnimation * gameOverPopupRestartButtonAnimation();
	int32_t gameOverPopupRestartButtonX() const;
	uint32_t gameOverPopupRestartButtonY() const;
	uint32_t gameOverPopupRestartButtonWidth() const;
	uint32_t gameOverPopupRestartButtonHeight() const;

	uint32_t menuBackground() const;

	uint32_t menuStartButtonStates() const;
	ButtonAnimation * menuStartButtonAnimation();
	uint32_t menuStartButtonX() const;
	uint32_t menuStartButtonY() const;
	uint32_t menuStartButtonWidth() const;
	uint32_t menuStartButtonHeight() const;

	uint32_t menuExitButtonStates() const;
	ButtonAnimation * menuExitButtonAnimation();
	uint32_t menuExitButtonX() const;
	uint32_t menuExitButtonY() const;
	uint32_t menuExitButtonWidth() const;
	uint32_t menuExitButtonHeight() const;

	uint32_t fontTimeDelimiter() const;
	FontVector scoreFont() const;
	FontVector timeFont() const;

	uint32_t wallSpriteId() const;

	std::vector<WeaponParams> weapons() const;
	std::vector<BonusParams> bonuses() const;

private:
	bool readSprites(rapidjson::Document &json);
	bool readSounds(rapidjson::Document &json);
	bool readMusic(rapidjson::Document &json);
	bool readTerrains(rapidjson::Document &json);

	bool readMenuSprites(rapidjson::Document &json);
	bool readMenuSounds(rapidjson::Document &json);
	bool readMenuMusic(rapidjson::Document &json);
	bool readMenuTerrains(rapidjson::Document &json);

	bool readGameSettings(rapidjson::Document &json);
	bool readMenuSettings(rapidjson::Document &json);

	bool readEnemyParams(rapidjson::Document &json);
	bool readPlayerParams(rapidjson::Document &json);
	bool readWeaponType(WeaponType& type, const std::string &key, rapidjson::Value &object);
	bool readWeaponParams(WeaponParams& params, uint32_t &currId, IdMap& idMap, rapidjson::Value &object);
	bool readWeapons(rapidjson::Document &json);
	bool readBonusParams(BonusParams& params, uint32_t &currId, IdMap& idMap, rapidjson::Value &object);
	bool readBonuses(rapidjson::Document &json);
	bool readUIParams(rapidjson::Document &json);
	bool readMainMenuPopup(rapidjson::Document &json);
	bool readGameOverPopup(rapidjson::Document &json);
	bool readFont(rapidjson::Document &json);

	bool readButtonParams(int32_t& x, int32_t &y, uint32_t &width, uint32_t &height, uint32_t &animationStatesCount, ButtonAnimation * animation, std::map<std::string, uint32_t>& spritesIdMap, rapidjson::Value& buttonObj);

	bool readResources(std::vector<std::string> &pathesList, IdMap &idMap, const std::string &tagId, const std::string &tagPath, rapidjson::Value& resourcesArray);

	bool readId(uint32_t &id, const IdMap& idMap, const std::string &key, rapidjson::Value &object);
	bool readId(uint32_t &id, const IdMap& idMap, rapidjson::Value &object);

	bool readBoolean(bool &var, const std::string &key, rapidjson::Value &object);
	bool readInt(int32_t &var, const std::string &key, rapidjson::Value &object);
	bool readUint(uint32_t &var, const std::string &key, rapidjson::Value &object);
	bool readDouble(double &var, const std::string &key, rapidjson::Value &object);
	bool readString(std::string &var, const std::string &key, rapidjson::Value &object);
	bool readArray(rapidjson::Value &array, const std::string &key, rapidjson::Value &object);
	bool readCoordinate2D(Coordinate2D &coordinate, const std::string &key, rapidjson::Value &object);
	bool readModifier(Modifier &modifier, const std::string &key, rapidjson::Value &object);

private:
	bool m_initialized;

	IdMap m_spriteIds, m_menuSpriteIds;
	SpritesVector m_sprites, m_menuSprites;

	IdMap m_soundsIds;
	SoundsVector m_sounds, m_menuSounds;

	IdMap m_musicIds, m_menuMusicIds;
	MusicVector m_music, m_menuMusic;

	uint32_t m_tileWidth, m_tileHeight, m_menuTileWidth, m_menuTileHeight;
	TerrainVector m_terrains, m_menuTerrains;
	uint32_t m_terrainTilesCount, m_menuTilesCount;

	uint32_t m_minEnemySpawnDistance, m_maxEnemySpawnDistance;
	uint32_t m_enemiesSpawnSimultaneously;
	double m_enemiesSpawnCooldownTime;
	uint32_t m_gridCellWidth, m_gridCellHeight;

	EnemyParams m_enemyParams;

	uint32_t m_playerSpeed;
	uint32_t m_playerHitmapRadius;
	uint32_t m_playerDefaultWeapon;
	double m_playerMass;
	double m_playerMaxHP;
	uint32_t m_playerStatesCount;
	PlayerAnimation m_playerAnimationStates[PLAYER_ANIMATION_STATES];

	uint32_t m_menuMusicTheme, m_gameMusic, m_gameOverMusic, m_victoryMusic;
	double m_soundsVolume, m_musicVolume;

	uint32_t m_gameBulletIndicatorX, m_gameBulletIndicatorY, m_gameBulletIndicatorSpriteId;
	uint32_t m_menuCursorArrowSpriteId;
	uint32_t m_cursorArrowSpriteId;
	uint32_t m_cursorAimSpriteId;
	uint32_t m_scoreIndicatorY, m_timeIndicatorX, m_timeIndicatorY;

	uint32_t m_gameOverPopupVictoryBackground;
	uint32_t m_gameOverPopupFailBackground;

	uint32_t m_gameOverPopupExitButtonStates;
	ButtonAnimation m_gameOverPopupExitButtonAnimation[BUTTON_ANIMATION_STATES];
	int32_t m_gameOverPopupExitButtonX, m_gameOverPopupExitButtonY;
	uint32_t m_gameOverPopupExitButtonWidth, m_gameOverPopupExitButtonHeight;

	uint32_t m_gameOverPopupRestartButtonStates;
	ButtonAnimation m_gameOverPopupRestartButtonAnimation[BUTTON_ANIMATION_STATES];
	int32_t m_gameOverPopupRestartButtonX, m_gameOverPopupRestartButtonY;
	uint32_t m_gameOverPopupRestartButtonWidth, m_gameOverPopupRestartButtonHeight;

	uint32_t m_menuBackground;

	uint32_t m_menuStartButtonStates;
	ButtonAnimation m_menuStartButtonAnimation[BUTTON_ANIMATION_STATES];
	int32_t m_menuStartButtonX, m_menuStartButtonY;
	uint32_t m_menuStartButtonWidth, m_menuStartButtonHeight;

	uint32_t m_menuExitButtonStates;
	ButtonAnimation m_menuExitButtonAnimation[BUTTON_ANIMATION_STATES];
	int32_t m_menuExitButtonX, m_menuExitButtonY;
	uint32_t m_menuExitButtonWidth, m_menuExitButtonHeight;

	uint32_t m_fontTimeDelimiter;
	FontVector m_scoreFont, m_timeFont;

	uint32_t m_wallSpriteId;

	IdMap m_bonusIds, m_weaponIds;
	
	std::vector<WeaponParams> m_weapons;
	std::vector<BonusParams> m_bonuses;

private:
	template<class T> bool readEntityParams(const std::string &tag, uint32_t &speed, uint32_t &hitmapRadius, double &mass, double &maxHP, uint32_t &entityStatesCount, T * states, rapidjson::Document & json)
	{
		auto& rootObj = json[tag.c_str()];

		return readUint(speed, "speed", rootObj) &&
			readUint(hitmapRadius, "hitmapRadius", rootObj) &&
			readDouble(mass, "mass", rootObj) &&
			readDouble(maxHP, "maxHP", rootObj) &&
			readAnimation<T>(entityStatesCount, states, m_spriteIds, rootObj["animation"]);
	}

	template<class T> bool readAnimation(uint32_t &entityStatesCount, T * states, const std::map<std::string, uint32_t> &spritesIdMap, rapidjson::Value& animationArray)
	{
		uint32_t statesCount = 0, framesCount = 0, nextFrameId;
		Transition next;
		std::string transitionStr;
		std::map<uint32_t, std::map<uint32_t, std::string> > goToStateFrames;
		std::map<std::string, uint32_t> stateIds;
		std::string stateId, nextStateIdStr;

		for (auto& animationObj : animationArray.GetArray())
		{
			framesCount = 0;

			stateId = animationObj["stateId"].GetString();

			if (stateIds.find(stateId) != stateIds.end())
			{
				LoggerLocator::locate()->logFatal(std::string("stateId ").append(stateId).append("is duplicated"));
				return false;
			}

			stateIds.insert({ stateId, statesCount });

			rapidjson::Value& framesArray = animationObj["frames"];

			for (auto& frameObj : framesArray.GetArray())
			{
				states[statesCount].frames[framesCount].duration = frameObj["duration"].GetDouble();
				states[statesCount].frames[framesCount].spriteId = spritesIdMap.at(frameObj["spriteId"].GetString());

				transitionStr = frameObj["next"].GetString();
				nextFrameId = 0;

				if (transitionStr.compare("STAY_HERE") == 0)
				{
					next = Transition::STAY_HERE;
				}
				else if (transitionStr.compare("GO_NEXT") == 0)
				{
					next = Transition::GO_NEXT;
				}
				else if (transitionStr.compare("GO_PREV") == 0)
				{
					next = Transition::GO_PREV;
				}
				else if (transitionStr.compare("GO_TO_FRAME") == 0)
				{
					next = Transition::GO_TO_FRAME;

					// TODO: check boundaries
					if (!readUint(nextFrameId, "nextFrame", frameObj)) return false;
				}
				else if (transitionStr.compare("GO_TO_STATE") == 0)
				{
					next = Transition::GO_TO_STATE;

					// Read argument
					if (!readString(nextStateIdStr, "nextState", frameObj)) return false;

					if (goToStateFrames.find(statesCount) == goToStateFrames.end())
					{
						goToStateFrames.insert({statesCount, std::map<uint32_t, std::string>()});
					}
					goToStateFrames.at(statesCount).insert({ framesCount, nextStateIdStr });
				}
				else
				{
					LoggerLocator::locate()->logFatal(std::string("Wrong transition value at frame: ").append(std::to_string(framesCount)));
					return false;
				}

				states[statesCount].frames[framesCount].next = next;
				states[statesCount].frames[framesCount].transitionArg = nextFrameId;

				framesCount++;
			}

			states[statesCount].framesCount = framesCount;

			statesCount++;
		}

		entityStatesCount = statesCount;

		// Parse GO_TO_STATE transitions
		uint32_t currStateId, nextStateId, frameId;
		for (auto state = goToStateFrames.begin(); state != goToStateFrames.end(); state++)
		{
			currStateId = state->first;

			for (auto frame = state->second.begin(); frame != state->second.end(); frame++)
			{
				frameId = frame->first;
				if (stateIds.find(frame->second) == stateIds.end())
				{
					LoggerLocator::locate()->logFatal(std::string("StateId ").append(frame->second).append(" in frame ").append(std::to_string(frameId)).append(" of state ").append(std::to_string(currStateId)).append(" is not exist"));
					return false;
				}

				nextStateId = stateIds.at(frame->second);
				states[currStateId].frames[frameId].transitionArg = nextStateId;
			}
		}

		return true;
	}
};