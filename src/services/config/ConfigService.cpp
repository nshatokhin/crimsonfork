#include "ConfigService.h"

#include "helpers/StringParse.h"
#include "services/LoggerLocator.h"

#include <fstream>
#include <vector>

ConfigService::ConfigService(std::string filename) :
	m_initialized(false)
{
	rapidjson::Document json;

	std::ifstream inputFile;
	inputFile.open(filename);

	if (!inputFile.is_open())
	{
		LoggerLocator::locate()->logFatal(std::string("File \"").append(filename).append("\" is not exists"));
		return;
	}

	std::string str;

	inputFile.seekg(0, std::ios::end);
	str.reserve(inputFile.tellg());
	inputFile.seekg(0, std::ios::beg);

	str.assign((std::istreambuf_iterator<char>(inputFile)),
		std::istreambuf_iterator<char>());

	json.Parse(str.c_str());

	if (json.HasParseError())
	{
		LoggerLocator::locate()->logFatal("Non valid json");
		return;
	}

	if (!readMenuSprites(json))
	{
		LoggerLocator::locate()->logFatal("Menu Sprites not loaded");
		return;
	}
	if (!readMenuSounds(json))
	{
		LoggerLocator::locate()->logFatal("Menu Sounds not loaded");
		return;
	}
	if (!readMenuMusic(json))
	{
		LoggerLocator::locate()->logFatal("Menu Music not loaded");
		return;
	}
	if (!readMenuTerrains(json))
	{
		LoggerLocator::locate()->logFatal("Menu Terrains not loaded");
		return;
	}
	if (!readSprites(json))
	{
		LoggerLocator::locate()->logFatal("Sprites not loaded");
		return;
	}
	if (!readSounds(json))
	{
		LoggerLocator::locate()->logFatal("Sounds not loaded");
		return;
	}
	if (!readMusic(json))
	{
		LoggerLocator::locate()->logFatal("Music not loaded");
		return;
	}
	if (!readTerrains(json))
	{
		LoggerLocator::locate()->logFatal("Terrains not loaded");
		return;
	}
	if (!readMenuSettings(json))
	{
		LoggerLocator::locate()->logFatal("MenuSettings not loaded");
		return;
	}
	if (!readGameSettings(json))
	{
		LoggerLocator::locate()->logFatal("GameSettings not loaded");
		return;
	}
	if (!readEnemyParams(json))
	{
		LoggerLocator::locate()->logFatal("Enemies params not loaded");
		return;
	}
	if (!readWeapons(json))
	{
		LoggerLocator::locate()->logFatal("Weapons params not loaded");
		return;
	}
	if (!readBonuses(json))
	{
		LoggerLocator::locate()->logFatal("Bonuses params not loaded");
		return;
	}
	if (!readPlayerParams(json))
	{
		LoggerLocator::locate()->logFatal("Player params not loaded");
		return;
	}
	if (!readUIParams(json))
	{
		LoggerLocator::locate()->logFatal("UI params not loaded");
		return;
	}
	if (!readFont(json))
	{
		LoggerLocator::locate()->logFatal("Font params not loaded");
		return;
	}

	m_initialized = true;
}

ConfigService::~ConfigService()
{
	
}

bool ConfigService::isInitialized() const
{
	return m_initialized;
}

SpritesVector ConfigService::sprites() const
{
	return m_sprites;
}

SoundsVector ConfigService::sounds() const
{
	return m_sounds;
}

MusicVector ConfigService::music() const
{
	return m_music;
}

TerrainVector ConfigService::terrains() const
{
	return m_terrains;
}

SpritesVector ConfigService::menuSprites() const
{
	return m_menuSprites;
}

SoundsVector ConfigService::menuSounds() const
{
	return m_menuSounds;
}

MusicVector ConfigService::menuMusic() const
{
	return m_menuMusic;
}

TerrainVector ConfigService::menuTerrains() const
{
	return m_menuTerrains;
}

uint32_t ConfigService::terrainTilesCount() const
{
	return m_terrainTilesCount;
}

uint32_t ConfigService::menuTilesCount() const
{
	return m_menuTilesCount;
}

uint32_t ConfigService::tileWidth() const
{
	return m_tileWidth;
}

uint32_t ConfigService::tileHeight() const
{
	return m_tileHeight;
}

uint32_t ConfigService::minEnemySpawnDistance() const
{
	return m_minEnemySpawnDistance;
}

uint32_t ConfigService::maxEnemySpawnDistance() const
{
	return m_maxEnemySpawnDistance;
}

uint32_t ConfigService::enemiesSpawnSimultaneously() const
{
	return m_enemiesSpawnSimultaneously;
}

double ConfigService::enemiesSpawnCooldownTime() const
{
	return m_enemiesSpawnCooldownTime;
}

uint32_t ConfigService::gridCellWidth() const
{
	return m_gridCellWidth;
}

uint32_t ConfigService::gridCellHeight() const
{
	return m_gridCellHeight;
}

EnemyParams ConfigService::enemyParams() const
{
	return m_enemyParams;
}

uint32_t ConfigService::playerSpeed() const
{
	return m_playerSpeed;
}

uint32_t ConfigService::playerHitmapRadius() const
{
	return m_playerHitmapRadius;
}

uint32_t ConfigService::playerDefaultWeapon() const
{
	return m_playerDefaultWeapon;
}

uint32_t ConfigService::playerStatesCount() const
{
	return m_playerStatesCount;
}

PlayerAnimation * ConfigService::playerAnimationStates()
{
	return m_playerAnimationStates;
}

uint32_t ConfigService::gameBulletIndicatorX() const
{
	return m_gameBulletIndicatorX;
}

uint32_t ConfigService::gameBulletIndicatorY() const
{
	return m_gameBulletIndicatorY;
}

uint32_t ConfigService::gameBulletIndicatorSpriteId() const
{
	return m_gameBulletIndicatorSpriteId;
}

uint32_t ConfigService::menuCursorArrowSpriteId() const
{
	return m_menuCursorArrowSpriteId;
}

uint32_t ConfigService::cursorArrowSpriteId() const
{
	return m_cursorArrowSpriteId;
}

uint32_t ConfigService::cursorAimSpriteId() const
{
	return m_cursorAimSpriteId;
}

uint32_t ConfigService::scoreIndicatorY() const
{
	return m_scoreIndicatorY;
}

uint32_t ConfigService::timeIndicatorX() const
{
	return m_timeIndicatorX;
}

uint32_t ConfigService::timeIndicatorY() const
{
	return m_timeIndicatorY;
}

uint32_t ConfigService::menuMusicTheme() const
{
	return m_menuMusicTheme;
}

uint32_t ConfigService::gameMusic() const
{
	return m_gameMusic;
}

uint32_t ConfigService::gameOverMusic() const
{
	return m_gameOverMusic;
}

uint32_t ConfigService::victoryMusic() const
{
	return m_victoryMusic;
}

double ConfigService::soundsVolume() const
{
	return m_soundsVolume;
}

double ConfigService::musicVolume() const
{
	return m_musicVolume;
}

uint32_t ConfigService::gameOverPopupVictoryBackground() const
{
	return m_gameOverPopupVictoryBackground;
}

uint32_t ConfigService::gameOverPopupFailBackground() const
{
	return m_gameOverPopupFailBackground;
}

uint32_t ConfigService::gameOverPopupExitButtonStates() const
{
	return m_gameOverPopupExitButtonStates;
}

ButtonAnimation * ConfigService::gameOverPopupExitButtonAnimation()
{
	return m_gameOverPopupExitButtonAnimation;
}

int32_t ConfigService::gameOverPopupExitButtonX() const
{
	return m_gameOverPopupExitButtonX;
}

uint32_t ConfigService::gameOverPopupExitButtonY() const
{
	return m_gameOverPopupExitButtonY;
}

uint32_t ConfigService::gameOverPopupExitButtonWidth() const
{
	return m_gameOverPopupExitButtonWidth;
}

uint32_t ConfigService::gameOverPopupExitButtonHeight() const
{
	return m_gameOverPopupExitButtonHeight;
}

int32_t ConfigService::gameOverPopupRestartButtonX() const
{
	return m_gameOverPopupRestartButtonX;
}

uint32_t ConfigService::gameOverPopupRestartButtonY() const
{
	return m_gameOverPopupRestartButtonY;
}

uint32_t ConfigService::gameOverPopupRestartButtonWidth() const
{
	return m_gameOverPopupRestartButtonWidth;
}

uint32_t ConfigService::gameOverPopupRestartButtonHeight() const
{
	return m_gameOverPopupRestartButtonHeight;
}

uint32_t ConfigService::menuBackground() const
{
	return m_menuBackground;
}

uint32_t ConfigService::menuStartButtonStates() const
{
	return m_menuStartButtonStates;
}

ButtonAnimation * ConfigService::menuStartButtonAnimation()
{
	return m_menuStartButtonAnimation;
}

uint32_t ConfigService::menuStartButtonX() const
{
	return m_menuStartButtonX;
}

uint32_t ConfigService::menuStartButtonY() const
{
	return m_menuStartButtonY;
}

uint32_t ConfigService::menuStartButtonWidth() const
{
	return m_menuStartButtonWidth;
}

uint32_t ConfigService::menuStartButtonHeight() const
{
	return m_menuStartButtonHeight;
}

uint32_t ConfigService::menuExitButtonStates() const
{
	return m_menuExitButtonStates;
}

ButtonAnimation * ConfigService::menuExitButtonAnimation()
{
	return m_menuExitButtonAnimation;
}

uint32_t ConfigService::menuExitButtonX() const
{
	return m_menuExitButtonX;
}

uint32_t ConfigService::menuExitButtonY() const
{
	return m_menuExitButtonY;
}

uint32_t ConfigService::menuExitButtonWidth() const
{
	return m_menuExitButtonWidth;
}

uint32_t ConfigService::menuExitButtonHeight() const
{
	return m_menuExitButtonHeight;
}

uint32_t ConfigService::fontTimeDelimiter() const
{
	return m_fontTimeDelimiter;
}

FontVector ConfigService::scoreFont() const
{
	return m_scoreFont;
}

FontVector ConfigService::timeFont() const
{
	return m_timeFont;
}

uint32_t ConfigService::wallSpriteId() const
{
	return m_wallSpriteId;
}

std::vector<WeaponParams> ConfigService::weapons() const
{
	return m_weapons;
}

std::vector<BonusParams> ConfigService::bonuses() const
{
	return m_bonuses;
}

uint32_t ConfigService::gameOverPopupRestartButtonStates() const
{
	return m_gameOverPopupRestartButtonStates;
}

ButtonAnimation * ConfigService::gameOverPopupRestartButtonAnimation()
{
	return m_gameOverPopupRestartButtonAnimation;
}

bool ConfigService::readSprites(rapidjson::Document &json)
{
	return readResources(m_sprites, m_spriteIds, "spriteId", "spritePath", json["sprites"]);
}

bool ConfigService::readSounds(rapidjson::Document & json)
{
	return readResources(m_sounds, m_soundsIds, "soundId", "soundPath", json["sounds"]);
}

bool ConfigService::readMusic(rapidjson::Document & json)
{
	return readResources(m_music, m_musicIds, "musicId", "musicPath", json["music"]);
}

bool ConfigService::readTerrains(rapidjson::Document & json)
{
	rapidjson::Value& terrainObj = json["terrain"];

	if (!(readUint(m_tileWidth, "tileWidth", terrainObj) && readUint(m_tileHeight, "tileHeight", terrainObj)))
	{
		LoggerLocator::locate()->logFatal("Can't read terrain tiles width and height");
		return false;
	}

	rapidjson::Value& terrainsArray = terrainObj["sprites"];

	m_terrains.clear();
	m_terrainTilesCount = 0;

	std::string str;
	for (rapidjson::Value& terrainObj : terrainsArray.GetArray())
	{
		if (!readString(str, "spritePath", terrainObj))
		{
			LoggerLocator::locate()->logFatal("Can't read terrain tile sprite path");
			return false;
		}

		m_terrains.push_back(str);
		m_terrainTilesCount++;
	}

	return true;
}

bool ConfigService::readMenuSprites(rapidjson::Document & json)
{
	return readResources(m_menuSprites, m_menuSpriteIds, "spriteId", "spritePath", json["menuSprites"]);
}

bool ConfigService::readMenuSounds(rapidjson::Document & json)
{
	return true;
}

bool ConfigService::readMenuMusic(rapidjson::Document & json)
{
	return readResources(m_menuMusic, m_menuMusicIds, "musicId", "musicPath", json["menuMusic"]);
}

bool ConfigService::readMenuTerrains(rapidjson::Document & json)
{
	rapidjson::Value& terrainObj = json["menuTerrain"];

	if(!(readUint(m_menuTileWidth, "tileWidth", terrainObj) && readUint(m_menuTileHeight, "tileHeight", terrainObj)))
	{
		LoggerLocator::locate()->logFatal("Can't read menu bg tiles width and height");
		return false;
	}

	rapidjson::Value& terrainsArray = terrainObj["sprites"];

	m_menuTerrains.clear();
	m_menuTilesCount = 0;

	std::string str;
	for (rapidjson::Value& terrainObj : terrainsArray.GetArray())
	{
		if (!readString(str, "spritePath", terrainObj))
		{
			LoggerLocator::locate()->logFatal("Can't read terrain tile sprite path");
			return false;
		}

		m_menuTerrains.push_back(str);
		m_menuTilesCount++;
	}

	return true;
}

bool ConfigService::readGameSettings(rapidjson::Document & json)
{
	rapidjson::Value& gameObj = json["game"];

	return readUint(m_minEnemySpawnDistance, "minEnemySpawnDistance", gameObj) &&
		readUint(m_maxEnemySpawnDistance, "maxEnemySpawnDistance", gameObj) &&
		readUint(m_enemiesSpawnSimultaneously, "enemiesSpawnSimultaneously", gameObj) &&
		readDouble(m_enemiesSpawnCooldownTime, "enemiesSpawnCooldownTime", gameObj) &&
		readUint(m_gridCellWidth, "gridCellWidth", gameObj) &&
		readUint(m_gridCellHeight, "gridCellHeight", gameObj) &&
		readDouble(m_soundsVolume, "soundsVolume", gameObj) &&
		readDouble(m_musicVolume, "musicVolume", gameObj) &&
		readId(m_gameMusic, m_musicIds, "music", gameObj) &&
		readId(m_gameOverMusic, m_musicIds, "gameOverMusic", gameObj) &&
		readId(m_victoryMusic, m_musicIds, "victoryMusic", gameObj) &&
		readId(m_wallSpriteId, m_spriteIds, "walls", gameObj);;
}

bool ConfigService::readMenuSettings(rapidjson::Document & json)
{
	rapidjson::Value& menuObj = json["menu"];

	return readId(m_menuMusicTheme, m_menuMusicIds, "music", menuObj) &&
		readId(m_menuCursorArrowSpriteId, m_menuSpriteIds, "cursorPointer", menuObj) &&
		readMainMenuPopup(json);
}

bool ConfigService::readEnemyParams(rapidjson::Document & json)
{
	return readId(m_enemyParams.deathSound, m_soundsIds, "deathSound", json["enemy"]) &&
		readEntityParams<EnemyAnimation>("enemy", m_enemyParams.maxSpeed, m_enemyParams.hitmapRadius, m_enemyParams.mass, m_enemyParams.maxHP, m_enemyParams.animationStatesCount, m_enemyParams.animationStates, json);
}

bool ConfigService::readPlayerParams(rapidjson::Document & json)
{
	rapidjson::Value& rootObj = json["player"];

	return readId(m_playerDefaultWeapon, m_weaponIds, "defaultWeapon", rootObj) &&
		readEntityParams<PlayerAnimation>("player", m_playerSpeed, m_playerHitmapRadius, m_playerMass, m_playerMaxHP, m_playerStatesCount, m_playerAnimationStates, json);
}

bool ConfigService::readWeaponType(WeaponType & type, const std::string &key, rapidjson::Value & object)
{
	type = WeaponType::CANNON;
	return true;
}

bool ConfigService::readWeaponParams(WeaponParams & params, uint32_t &currId, IdMap& idMap, rapidjson::Value & object)
{
	std::string idStr;
	if (!readString(idStr, "id", object))
	{
		LoggerLocator::locate()->logFatal(std::string("Bonus \"id\" field is not exists or not string"));
		return false;
	}
	
	if (idMap.find(idStr) != idMap.end())
	{
		LoggerLocator::locate()->logFatal(std::string("Bonus ID ").append(idStr).append(" is duplicated"));
		return false;
	}
	
	bool success = readWeaponType(params.type, "type", object) &&
		readUint(params.bulletSpeed, "bulletSpeed", object) &&
		readUint(params.bulletHitmapRadius, "bulletHitmapRadius", object) &&
		readUint(params.ammoCount, "ammoCount", object) &&
		readAnimation<WeaponAnimation>(params.weaponAnimationStates, params.weaponAnimation, m_spriteIds, object["weaponAnimation"]) &&
		readCoordinate2D(params.bulletSpawnPoint, "bulletSpawnPoint", object) &&
		readAnimation<WeaponAnimation>(params.bulletAnimationStates, params.bulletAnimation, m_spriteIds, object["bulletAnimation"]) &&
		readDouble(params.shootCooldownDelay, "shootCooldownDelay", object) &&
		readDouble(params.reloadDelay, "reloadDelay", object) &&
		readId(params.shootSound, m_soundsIds, "shootSound", object) &&
		readId(params.reloadSound, m_soundsIds, "reloadSound", object) &&
		readId(params.reloadCompleteSound, m_soundsIds, "reloadCompleteSound", object) &&
		readId(params.noAmmoSound, m_soundsIds, "noAmmoSound", object) &&
		readBoolean(params.automatic, "automatic", object) &&
		readBoolean(params.targeted, "targeted", object);
		
	if(success)
	{
		params.id = currId;
		idMap.insert({ idStr, currId++ });
		return true;
	}
	
	return false;
}

bool ConfigService::readWeapons(rapidjson::Document & json)
{
	uint32_t currId = 0;
	m_weaponIds.clear();
	m_weapons.clear();
	
	rapidjson::Value weaponsArray;
	if (!readArray(weaponsArray, "weapons", json))
	{
		return false;
	}

	WeaponParams params;
	for (auto& weaponObj : weaponsArray.GetArray())
	{
		if (!readWeaponParams(params, currId, m_weaponIds, weaponObj))
		{
			return false;
		}

		m_weapons.push_back(params);
	}

	return true;
}

bool ConfigService::readBonusParams(BonusParams & params, uint32_t &currId, IdMap& idMap, rapidjson::Value & object)
{
	std::string idStr;
	if (!readString(idStr, "id", object))
	{
		LoggerLocator::locate()->logFatal(std::string("Bonus \"id\" field is not exists or not string"));
		return false;
	}
	
	if (idMap.find(idStr) != idMap.end())
	{
		LoggerLocator::locate()->logFatal(std::string("Bonus ID ").append(idStr).append(" is duplicated"));
		return false;
	}

	bool success = readUint(params.hitmapRadius, "hitmapRadius", object) &&
		readAnimation<BonusAnimation>(params.animationStatesCount, params.animationStates, m_spriteIds, object["animation"]) &&
		readId(params.collectSound, m_soundsIds, "collectSound", object) &&
		readModifier(params.modifier, "modifier", object) &&
		readBoolean(params.instant, "instant", object) &&
		readUint(params.dropChance, "dropChance", object);
		
	if(success)
	{
		params.id = currId;
		idMap.insert({ idStr, currId++ });
		return true;
	}
	
	return false;
}

bool ConfigService::readBonuses(rapidjson::Document & json)
{
	uint32_t currId = 0;
	m_bonusIds.clear();
	m_bonuses.clear();
	
	rapidjson::Value bonusesArray;
	if (!readArray(bonusesArray, "bonuses", json))
	{
		return false;
	}

	BonusParams params;
	for (auto& weaponObj : bonusesArray.GetArray())
	{
		if (!readBonusParams(params, currId, m_bonusIds, weaponObj))
		{
			return false;
		}

		m_bonuses.push_back(params);
	}

	return true;
}

bool ConfigService::readUIParams(rapidjson::Document & json)
{
	rapidjson::Value& rootObj = json["ui"];

	return readUint(m_gameBulletIndicatorX, "gameBulletIndicatorX", rootObj) &&
		readUint(m_gameBulletIndicatorY, "gameBulletIndicatorY", rootObj) &&
		readUint(m_scoreIndicatorY, "scoreIndicatorY", rootObj) &&
		readUint(m_timeIndicatorX, "timeIndicatorX", rootObj) &&
		readUint(m_timeIndicatorY, "timeIndicatorY", rootObj) &&
		readId(m_gameBulletIndicatorSpriteId, m_spriteIds, "gameBulletIndicatorSprite", rootObj) &&
		readId(m_cursorArrowSpriteId, m_spriteIds, "cursorArrowPointer", rootObj) &&
		readId(m_cursorAimSpriteId, m_spriteIds, "cursorAimPointer", rootObj) &&
		readGameOverPopup(json);
}

bool ConfigService::readMainMenuPopup(rapidjson::Document & json)
{
	rapidjson::Value& rootObj = json["menu"];

	return readId(m_menuBackground, m_menuSpriteIds, "background", rootObj) &&
		readButtonParams(m_menuStartButtonX, m_menuStartButtonY, m_menuStartButtonWidth, m_menuStartButtonHeight, m_menuStartButtonStates, m_menuStartButtonAnimation, m_menuSpriteIds, rootObj["buttons"].GetArray()[0]) &&
		readButtonParams(m_menuExitButtonX, m_menuExitButtonY, m_menuExitButtonWidth, m_menuExitButtonHeight, m_menuExitButtonStates, m_menuExitButtonAnimation, m_menuSpriteIds, rootObj["buttons"].GetArray()[1]);
}

bool ConfigService::readGameOverPopup(rapidjson::Document & json)
{
	rapidjson::Value& rootObj = json["gameOverPopup"];

	return readId(m_gameOverPopupVictoryBackground, m_spriteIds, "victoryBackground", rootObj) &&
		readId(m_gameOverPopupFailBackground, m_spriteIds, "failBackground", rootObj) &&
		readButtonParams(m_gameOverPopupRestartButtonX, m_gameOverPopupRestartButtonY, m_gameOverPopupRestartButtonWidth, m_gameOverPopupRestartButtonHeight, m_gameOverPopupRestartButtonStates, m_gameOverPopupRestartButtonAnimation, m_spriteIds, rootObj["buttons"].GetArray()[0]) &&
		readButtonParams(m_gameOverPopupExitButtonX, m_gameOverPopupExitButtonY, m_gameOverPopupExitButtonWidth, m_gameOverPopupExitButtonHeight, m_gameOverPopupExitButtonStates, m_gameOverPopupExitButtonAnimation, m_spriteIds, rootObj["buttons"].GetArray()[1]);

}

bool ConfigService::readFont(rapidjson::Document & json)
{
	rapidjson::Value& rootObj = json["font"];

	bool result = readId(m_fontTimeDelimiter, m_spriteIds, "timeDelimiter", rootObj);

	uint32_t symbolId;
	for (rapidjson::Value& resourceId : rootObj["scoreDigits"].GetArray())
	{
		result &= readId(symbolId, m_spriteIds, resourceId);
		m_scoreFont.push_back(symbolId);
	}

	for (rapidjson::Value&  resourceId : rootObj["timeDigits"].GetArray())
	{
		result &= readId(symbolId, m_spriteIds, resourceId);
		m_timeFont.push_back(symbolId);
	}

	return result;
}

bool ConfigService::readButtonParams(int32_t& x, int32_t &y, uint32_t &width, uint32_t &height, uint32_t & animationStatesCount, ButtonAnimation * animation, std::map<std::string, uint32_t>& spritesIdMap, rapidjson::Value& buttonObj)
{
	return readInt(x, "x", buttonObj) &&
		readInt(y, "y", buttonObj) &&
		readUint(width, "width", buttonObj) &&
		readUint(height, "height", buttonObj) &&
		readAnimation<ButtonAnimation>(animationStatesCount, animation, spritesIdMap, buttonObj["animation"]);
}

bool ConfigService::readResources(std::vector<std::string>& pathesList, std::map<std::string, uint32_t>& idMap, const std::string & tagId, const std::string & tagPath, rapidjson::Value & resourcesArray)
{
	std::string id, path;
	uint32_t currResId = 0;

	pathesList.clear();
	idMap.clear();

	for (rapidjson::Value& resourceObj : resourcesArray.GetArray())
	{
		if (!(readString(id, tagId, resourceObj) && readString(path, tagPath, resourceObj)))
		{
			LoggerLocator::locate()->logFatal(std::string("Can't read resource's ").append(tagId).append(" and ").append(tagPath));
			return false;
		}

		if (idMap.find(id) != idMap.end())
		{
			LoggerLocator::locate()->logFatal(std::string(tagId).append(" is duplicated"));
			return false;
		}

		idMap.insert({ id, currResId++ });
		pathesList.push_back(path);
	}

	return true;
}

bool ConfigService::readId(uint32_t & id, const std::map<std::string, uint32_t>& idMap, const std::string &key, rapidjson::Value & object)
{
	std::string idStr;
	if (!readString(idStr, key, object)) return false;

	if (idMap.find(idStr) == idMap.end())
	{
		LoggerLocator::locate()->logFatal(std::string("Id with key ").append(idStr).append(" is not found in config"));
		return false;
	}

	id = idMap.at(idStr);

	return true;
}

bool ConfigService::readId(uint32_t & id, const std::map<std::string, uint32_t>& idMap, rapidjson::Value & object)
{
	if (!object.IsString())
	{
		LoggerLocator::locate()->logFatal(std::string("readId: given object is not string"));
		return false;
	}

	std::string idStr = object.GetString();

	if (idMap.find(idStr) == idMap.end())
	{
		LoggerLocator::locate()->logFatal(std::string("Id with key ").append(idStr).append(" is not found in config"));
		return false;
	}

	id = idMap.at(idStr);

	return true;
}

bool ConfigService::readBoolean(bool & var, const std::string & key, rapidjson::Value & object)
{
	if (!object.HasMember(key.c_str()))
	{
		LoggerLocator::locate()->logFatal(std::string("Key ").append(key).append(" is not exist in config"));
		return false;
	}

	var = object[key.c_str()].GetBool();

	return true;
}

bool ConfigService::readInt(int32_t & var, const std::string & key, rapidjson::Value & object)
{
	if (!object.HasMember(key.c_str()))
	{
		LoggerLocator::locate()->logFatal(std::string("Key ").append(key).append(" is not exist in config"));
		return false;
	}
	
	var = object[key.c_str()].GetInt();

	return true;
}

bool ConfigService::readUint(uint32_t & var, const std::string & key, rapidjson::Value & object)
{
	if (!object.HasMember(key.c_str()))
	{
		LoggerLocator::locate()->logFatal(std::string("Key ").append(key).append(" is not exist in config"));
		return false;
	}

	var = object[key.c_str()].GetUint();

	return true;
}

bool ConfigService::readDouble(double & var, const std::string & key, rapidjson::Value & object)
{
	if (!object.HasMember(key.c_str()))
	{
		LoggerLocator::locate()->logFatal(std::string("Key ").append(key).append(" is not exist in config"));
		return false;
	}

	var = object[key.c_str()].GetDouble();

	return true;
}

bool ConfigService::readString(std::string & var, const std::string & key, rapidjson::Value & object)
{
	if (!object.HasMember(key.c_str()))
	{
		LoggerLocator::locate()->logFatal(std::string("Key ").append(key).append(" is not exist in config"));
		return false;
	}

	var = object[key.c_str()].GetString();

	return true;
}

bool ConfigService::readArray(rapidjson::Value& array, const std::string & key, rapidjson::Value & object)
{
	if (!object.HasMember(key.c_str()))
	{
		LoggerLocator::locate()->logFatal(std::string("Key ").append(key).append(" is not exist in config"));
		return false;
	}

	array = object[key.c_str()].GetArray();

	return true;
}

bool ConfigService::readCoordinate2D(Coordinate2D & coordinate, const std::string & key, rapidjson::Value & object)
{
	std::string str;
	if (!readString(str, key, object))
	{
		LoggerLocator::locate()->logFatal(std::string("Key ").append(key).append(" is not exists or not string"));
		return false;
	}

	if (!parseCoordinateNotation(coordinate, str))
	{
		LoggerLocator::locate()->logFatal(std::string("String ").append(str).append(" is not coordinate notation (x; y)"));
		return false;
	}

	return true;
}

bool ConfigService::readModifier(Modifier & modifier, const std::string & key, rapidjson::Value & object)
{
	if (!object.HasMember(key.c_str()))
	{
		LoggerLocator::locate()->logFatal(std::string("Key ").append(key).append(" is not exist in config"));
		return false;
	}
	
	rapidjson::Value& rootObj = object[key.c_str()];
	
	std::string paramStr, operStr, argStr;
	if (!readString(paramStr, "parameter", rootObj))
	{
		LoggerLocator::locate()->logFatal(std::string("Modifier's parameter field is not exists or not string"));
		return false;
	}
	
	if (!readString(operStr, "operation", rootObj))
	{
		LoggerLocator::locate()->logFatal(std::string("Modifier's operation field is not exists or not string"));
		return false;
	}

	Modifier::Parameter param;
	Modifier::Operation operation = 0;
	Modifier::Argument argument = 0;
	
	if(paramStr.compare("HEALTH") == 0)
	{
		param = Modifier::Parameter::HEALTH;
		
		int32_t arg;
		Modifier::HealthOperations hop;
		
		if(operStr.compare("SET") == 0)
		{
			hop = Modifier::HealthOperations::SET;
			
			if(!readInt(arg, "argument", rootObj))
			{
				LoggerLocator::locate()->logFatal(std::string("Can't read argument for SET operation of HEALTH param"));
				return false;
			}
			
			argument = static_cast<Modifier::Argument>(arg);
		}
		else if(operStr.compare("INCREASE") == 0)
		{
			hop = Modifier::HealthOperations::INCREASE;
			
			if(!readInt(arg, "argument", rootObj))
			{
				LoggerLocator::locate()->logFatal(std::string("Can't read argument for INCREASE operation of HEALTH param"));
				return false;
			}
			
			argument = static_cast<Modifier::Argument>(arg);
		}
		else if(operStr.compare("DECREASE") == 0)
		{
			hop = Modifier::HealthOperations::DECREASE;
			
			if(!readInt(arg, "argument", rootObj))
			{
				LoggerLocator::locate()->logFatal(std::string("Can't read argument for DECREASE operation of HEALTH param"));
				return false;
			}
			
			argument = static_cast<Modifier::Argument>(arg);
		}
		else if(operStr.compare("MAXIMIZE") == 0)
		{
			hop = Modifier::HealthOperations::MAXIMIZE;
		}
		else if(operStr.compare("MINIMIZE") == 0)
		{
			hop = Modifier::HealthOperations::MINIMIZE;
		}
		else
		{
			LoggerLocator::locate()->logFatal(std::string("Unknown operation ").append(operStr).append(" for HEALTH param"));
			return false;
		}
		
		operation = static_cast<Modifier::Operation>(hop);
	}
	else if(paramStr.compare("WEAPON") == 0)
	{
		param = Modifier::Parameter::WEAPON;
		
		uint32_t weaponId;
		Modifier::WeaponOperations wop = Modifier::WeaponOperations::SET;
		
		if(operStr.compare("SET") == 0)
		{
			wop = Modifier::WeaponOperations::SET;

			if(!readId(weaponId, m_weaponIds, "argument", rootObj))
			{
				LoggerLocator::locate()->logFatal(std::string("Can't read argument for SET operation of WEAPON param"));
				return false;
			}
			
			argument = static_cast<Modifier::Argument>(weaponId);
		}
		else
		{
			LoggerLocator::locate()->logFatal(std::string("Unknown operation ").append(operStr).append(" for WEAPON param"));
			return false;
		}
		
		operation = static_cast<Modifier::Operation>(wop);
	}
	else
	{
		LoggerLocator::locate()->logFatal(std::string("Unknown modifying param ").append(paramStr));
		return false;
	}
	
	modifier = Modifier(param, operation, argument);
	
	return true;
}
