#pragma once

#include <cstdint>

enum class Transition : uint8_t
{
	STAY_HERE = 0,
	GO_NEXT,
	GO_PREV,
	GO_TO_FRAME,
	GO_TO_STATE
};

class Frame
{
public:
	uint32_t spriteId;
	double duration;
	Transition next;
	int32_t transitionArg;
};