#pragma once

#include <cstdint>

#include "entities/Types.h"
#include "primitives/Coordinate2D.h"
#include "primitives/GameObjectHitmap.h"
#include "primitives/Vector2D.h"

class PhysicsComponent
{
protected:
	Coordinate2D m_position;

public:
	static const Vector2D BASIC_ORIENTATION;

	EntityType type;
	uint32_t entityId;

    Vector2D orientation;
    Vector2D speed;
    Vector2D force;
    double mass;
	GameObjectHitmap hitmap;
	bool canBeShifted;
    
public:
    PhysicsComponent()
    {
        
    }

	void init(EntityType entityType, Coordinate2D position, Vector2D speed, uint32_t hitmapRadius, double mass, bool shiftAllowed);

	void setPosition(const Coordinate2D &newPosition);
	Coordinate2D position() const;

	double orientationAngle() const
	{
		return atan2(orientation.y, orientation.x) - atan2(BASIC_ORIENTATION.y, BASIC_ORIENTATION.x);
	}

	double speedAngle() const
	{
		return atan2(speed.y, speed.x) - atan2(BASIC_ORIENTATION.y, BASIC_ORIENTATION.x);
	}
    
/*    void setForce(Vector2D force)
    {
        m_forces = force;
    }
    
    void resetForces()
    {
        m_forces = Vector2D();
    }
    
    void applyForce(Vector2D force)
    {
        m_forces += force;
    }
    
    void update(float dt)
    {
        Vector2D acceleration = m_force / m_mass;
        m_speed += acceleration * dt;
        m_position += m_speed;
    }
*/
};
