#include "PhysicsComponent.h"

const Vector2D PhysicsComponent::BASIC_ORIENTATION = Vector2D(1, 0);

void PhysicsComponent::init(EntityType entityType, Coordinate2D pos, Vector2D spd, uint32_t hitmapRadius, double m, bool shiftAllowed)
{
	type = entityType;
	m_position = pos;
	speed = spd;
	canBeShifted = shiftAllowed;
	mass = m;

	orientation = BASIC_ORIENTATION;

	// TODO: choose correct shape of hitmap
	hitmap.initCircle(pos, hitmapRadius);
}

void PhysicsComponent::setPosition(const Coordinate2D & newPosition)
{
	m_position = newPosition;
	hitmap.center = newPosition;
}

Coordinate2D PhysicsComponent::position() const
{
	return m_position;
}
