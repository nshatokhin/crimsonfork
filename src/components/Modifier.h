#pragma once

#include "services/LoggerLocator.h"

#include <cstdint>

class Modifier
{
public:
	enum class Parameter : uint8_t
	{
		HEALTH = 0,
		WEAPON
	};

	typedef uint8_t Operation;
	typedef int32_t Argument;

	enum class HealthOperations : Operation
	{
		SET = 0,
		INCREASE,
		DECREASE,
		MAXIMIZE,
		MINIMIZE
	};

	enum class WeaponOperations : Operation
	{
		SET = 0
	};

public:
	Modifier(Parameter parameter = Parameter::HEALTH, Operation operation = 0, Argument argument = 0) :
		parameter(parameter), operation(operation), argument(argument) {}

	void Init(Parameter param = Parameter::HEALTH, Operation op = 0, Argument arg = 0)
	{
		parameter = param;
		operation = op;
		argument = arg;

	}

	template<class Entity>
	static void Apply(Entity &entity, const Modifier &modifier)
	{
		if (modifier.parameter == Parameter::HEALTH)
		{
			HealthOperations op = static_cast<HealthOperations>(modifier.operation);

			if (op == HealthOperations::SET)
			{

			}
			else if (op == HealthOperations::INCREASE)
			{

			}
			else if (op == HealthOperations::DECREASE)
			{

			}
			else if (op == HealthOperations::MAXIMIZE)
			{

			}
			else if (op == HealthOperations::MINIMIZE)
			{

			}
			else
			{
				LoggerLocator::locate()->logWarning("Unknown health operation");
			}
		}
		else if (modifier.parameter == Parameter::WEAPON)
		{
			WeaponOperations op = static_cast<WeaponOperations>(modifier.operation);

			if (op == WeaponOperations::SET)
			{
				Weapon::DestroyWeapon(entity.weapon);
				entity.weapon = Weapon::CreateWeapon(modifier.argument);
				entity.weapon.ownerPhysicsComponent = entity.physicsComponent;
			}
			else
			{
				LoggerLocator::locate()->logWarning("Unknown weapon operation");
			}
		}
		else
		{
			LoggerLocator::locate()->logWarning("Unknown modifier operation");
		}
	}

public:
	Parameter parameter;
	Operation operation;
	Argument argument;
};