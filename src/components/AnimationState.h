#pragma once

#include "AnimationFrame.h"

template<uint32_t T>
class State
{
public:
	Frame frames[T];
	uint32_t framesCount;
};
