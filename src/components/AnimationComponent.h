#pragma once

#include <cassert>
#include <cstdint>

#include "AnimationFrame.h"
#include "AnimationState.h"

template <uint32_t States, uint32_t Frames>
class AnimationComponent
{
    
public:
    AnimationComponent() :
        m_playingTime(0),
        m_statesCount(0),
        m_currentStateIndex(0),
        m_currentFrameIndex(0),
        m_currentSprite(0)
    {
        
    }
    
    void init(uint32_t statesCount, State<Frames> states[])
    {
        assert(statesCount <= States);
        
        m_statesCount = statesCount;
        
        for(uint32_t i = 0; i < m_statesCount;i++)
        {
            assert(states[i].framesCount <= Frames);
            
            for(uint32_t j = 0; j < states[i].framesCount;j++)
            {
                m_states[i].frames[j] = states[i].frames[j];
            }

			m_states[i].framesCount = states[i].framesCount;
        }

		m_currentStateIndex = 0;
		m_currentFrameIndex = 0;
		m_currentSprite = m_states[m_currentStateIndex].frames[m_currentFrameIndex].spriteId;
    }
    
public:
    State<Frames> m_states[States];
    double m_playingTime;
    
    uint32_t m_statesCount;
    uint32_t m_currentStateIndex;
    uint32_t m_currentFrameIndex;
    uint32_t m_currentSprite;
    
};
